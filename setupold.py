import os
from setuptools import setup, Extension


with open("README.md", "r") as fh:
    long_description = fh.read()


rwfield_path = os.path.join("GaussPulse", "Fields", "RWFields")
Extension('rwfield', [os.path.join(rwfield_path, integral.c), os.path.join(rwfield_path, ), os.path.join(rwfield_path, ), os.path.join(rwfield_path, ))


setup(
    name="PyRadPulse",  # Replace with your own username
    version="0.0.1",
    author="Gergely Nagy",
    author_email="nagygeri11@gmail.com",
    description="Simulation of tightly focused radially polarized ultrashort light pulses",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
)
