#!/bin/bash

# check if conda exists on system
if hash conda 2>/dev/null; then
    USE_CONDA=true
elif hash python 2>/dev/null; then
    USE_PYENV=true
else
    echo "Error: no Python enviroment found"
fi

# check if GSL is installed (hope it works)
if [[ -z $(ldconfig -p | grep libgsl) ]]; then
    printf "\n ERROR: Can't find GSL library; please install it with \n\n\tsudo apt-get install libgsl-dev\n\n"
    exit 79
fi

#complie c codes
wd=$(pwd)
cd "${wd}/GaussPulse/Fields/RWFields"
if [[ ! -d "obj" ]]; then
    mkdir obj
fi
make
cd "${wd}"

#~~ Install using Conda
if [[ $USE_CONDA == true ]]; then
    echo "Using conda"
    # setup to conda activate (as per https://stackoverflow.com/a/58081608/5099168)
    eval "$(command conda 'shell.bash' 'hook' 2>/dev/null)"
    # create new conda environment
    read -p "Create new Python enviroment? (Y/n) (refusing will abort in this early version)" -i y -n 1 -r
    echo # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        conda env create --name elfield --file=conda_env.yml
        conda activate elfield
        echo "new conda environment created at $CONDA_PREFIX"
    else
        echo "Aborting..."
        exit
    fi
    # install Python dependencies
    # conda install --yes --file requirements-conda.txt
fi
exit

    #~~ Install using pip (untested)
elif [[ $USE_PYENV == true ]]; then
    echo "Using pip (untested!)"
    pip install -r requirements-pip.txt
fi

if [[ $USE_CONDA == true ]]; then
    conda develop .
    echo "Installation done. Please run 'conda activate elfield', and run ./test.py to check the installation!"
elif [[ $USE_PYENV == true ]]; then
    pip install -e .
    echo "Installation done. Please run ./test.py to check the installation!"

exit 0
