[TODO]

PyRadPulse is both a module and a standalone software for simulating ultrashort tightly and lightly focused radially polarized laser pulses.

**Requirements**:

* Python >= 3.8.3
* GSL library
* Conda is recommended to create isolated environment (installing via pip is not tested yet).


**Setup**

Linux: run ./install.sh

Other OS: not automated/tested yet; steps should be:
 * change to GaussPulse/Fields/RWFields
 * run `make`
 * change back to project directory
 * Conda: `conda env create --name pyradpulse --file conda_env.yml && conda activate pyradpulse && conda develop . `
 * pip (untested): `pip install -r requirements-pip.txt && pip install -e .`

 The project will be installed in the project folder ("develop mode").


**Usage**

Some examples can be found in the scripts folder. [TODO]