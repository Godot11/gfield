from GaussPulse import InputParse, vis, Comp, Norm
from GaussPulse.FieldVector import FieldFromSaved
import numpy as np
import os

field_folder = "example_fields"
fields = ["example_levy.json", "example_pu.json", "example_vik.json"]

failed = ""


def fail(label, what_failed, exception):
    global failed
    failstr = label + " " + what_failed + "\n"
    failed += failstr


def test(field_config_file):
    field_config = InputParse.ConfigParser(field_config_file)
    field = field_config.field
    savepath = field_config.savepath
    label = field_config.label

    print(f"Processing field: {label}")
    print(field.prettyprint(colored=True))
    print("Computing field")
    field.calculate_field(multicore=field_config.multicore)

    print("calculation successful")

    field = field.set_energy(field_config.energy)
    field.save(savepath)

    print("testing save/load")
    loaded = FieldFromSaved(savepath)
    print(f"timepoints equal with saved: {np.all(loaded.t == field.t)}")
    print(f"mesh equals with saved: {loaded.mesh == field.mesh}")
    loaded.unload_field()

    print("testing plots")
    for comp in field.nonzero_comps:
        print(f"Processing {comp.name} component:")
        print("plotting spectral and temporal pulse shape")
        # vis.pulse_shape(
        #     field_config.field,
        #     comp,
        #     r=0.5,
        #     phi=0,  # todo make this optional
        #     z=0.0,
        #     save=True,
        #     savepath=os.path.join(savepath, f"shape_{comp.name}.png"),
        # )
        print("Generating 2D animation along optical axis")
        vis.optax_anim(
            field,
            comp,
            Norm.REAL,
            save=True,
            savepath=os.path.join(savepath, f"optaxanim_{comp.name}.mp4"),
        )
        print("Generating 1D animation along optical axis")
        vis.optax_line_anim(
            field,
            0.5,
            comp,
            Norm.REAL,
            save=True,
            savepath=os.path.join(savepath, f"optaxlineanim_{comp.name}.mp4"),
        )
        print("plotting field in a line at t=0 along optical axis")
        vis.optax_line(
            field,
            0.5,
            0,
            comp,
            Norm.REAL,
            save=True,
            savepath=os.path.join(savepath, f"optaxline_{comp.name}.png"),
        )
        print("plotting field in 2D along optical axis at t=0")
        vis.optax_frame(
            field,
            0,
            comp,
            Norm.REAL,
            save=True,
            savepath=os.path.join(savepath, f"optaxframe_{comp.name}.png"),
        )
        print("plotting temporal evolution at a point")
        vis.point_time(
            field,
            0.5,
            0.0,
            comp,
            Norm.REAL,
            save=True,
            savepath=os.path.join(savepath, f"pointtime_{comp.name}.png"),
        )

        print("Generating animation in 2D adjacent to optical axis at z=0")
        vis.adj_anim(
            field,
            0,
            comp,
            Norm.ABS,
            save=True,
            savepath=os.path.join(savepath, f"adjanim_{comp.name}.mp4"),
        )
        print("plotting field in 2D adjacent to optical axis at z=0 ")
        vis.adj_frame(
            field,
            0,
            0,
            comp,
            Norm.ABS,
            save=True,
            savepath=os.path.join(savepath, f"adjframe_{comp.name}.png"),
        )
        print("plotting field in 2D adjacent to optical axis at z=0 ")
        vis.adj_line(
            field,
            0,
            0,
            comp,
            Norm.ABS,
            save=True,
            savepath=os.path.join(savepath, f"adjline_{comp.name}.png"),
        )

    vis.total_energy(
        field, save=True, savepath=os.path.join(savepath, f"total_energy_evolution.png")
    )
    vis.centroid_speed(
        field, save=True, savepath=os.path.join(savepath, f"centroid_speed.png")
    )


if __name__ == "__main__":
    for field in fields:
        fpath = os.path.join(field_folder, field)
        test(fpath)
