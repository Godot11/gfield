#!/opt/anaconda3/bin/python
# -*- coding: utf-8 -*-

import multiprocessing as multiproc
import warnings
from copy import copy
from dataclasses import dataclass
from typing import Any, List, Literal, Tuple, Type, Union

import numpy as np
import pyfftw
from matplotlib import pyplot as plt
from nptyping import NDArray
from progressbar import progressbar as prbar

from scipy import constants as const
from scipy import fft
from scipy.special import gamma


# Speed of light in nm/fs
from . import (
    C_LIGHT,
    OMEGA_0,
    Vectorized,  # fft,
    ArrayLike,
    Comp,
    MonoField,
    meshfield,
    utils,
)

pyfftw.config.NUM_THREADS = multiproc.cpu_count()
pyfftw.config.PLANNER_EFFORT = "FFTW_ESTIMATE"
# pyfftw.config.PLANNER_EFFORT = 'FFTW_MEASURE'

# integer initialized as 0, can be accessed by multiple processes)
multiproc_task_counter = multiproc.Value("i", 0)  # multiproc.Value("i", 0)


@dataclass
class FFTParameters:
    dt: float
    tmin: float
    tmax: float
    nframes: int
    t_length: float
    t_fft_window: float
    d_omega: float
    eval_omega_min: float
    eval_omega_max: float
    eval_omega_range: float
    omega_fft_window: float
    n_omega: int
    n_eval_omegas: int
    n_noneval_omegas: int

    def __init__(
        self,
        eval_omega_min: float,
        eval_omega_max: float,
        dt: float,
        maximal_domega: float,
        tmin: float,
        tmax_aim: float,
        antialias_coeff: float = 1.5,
    ) -> None:
        """
        Class to generate and store the time- and frequency-domain iFFT parameters.

        Args:
            eval_omega_min (float): lowef bound of evaluated omegas
            eval_omega_max (float): upper bound of evaluated omegas
            dt (float): timestep; this is considered holy
            maximal_domega (float): Upper bound of domega; actual domega will be
                at most this big.
            tmin (float): Initial time of the simulation
            tmax_aim (float): End time of the simulation. dt takes precedence;
                the last simulation frame will be at tmax = tmin + N*dt, where N
                is the highest integer so that tmax is the closest to tmax_aim.
            antialias_coeff (float, optional): Controls the minimal time window
                (and thus the frequency resolution). The time window will be at
                least T_FFT > (tmax_aim - tmin) * antialias_coeff. Defaults to 1.5.
        """
        self.eval_omega_min = eval_omega_min
        self.eval_omega_max = eval_omega_max
        self.eval_omega_range = eval_omega_max - eval_omega_min
        self.dt = dt
        self.tmin = tmin
        self.tmax = tmax_aim

        # partition the t range and set tmax so that dt is obeyed
        t_length = tmax_aim - tmin
        self.nframes = (
            round(t_length / dt) + 1
        )  # add 1 because the endpoint is included
        self.t = self.tmin + np.array([i * self.dt for i in range(self.nframes)])
        self.tmax = self.t[-1]
        self.t_length = tmax_aim - tmin
        self.times = self.t  # todo

        # set the FFT windows on freq and time domain
        self.omega_fft_window = 2 * const.pi / dt
        self.t_fft_window = 2 * const.pi / maximal_domega

        # avoid aliasing in the time domain by increasing the time window if needed
        if self.t_fft_window < antialias_coeff * self.t_length:
            print("t_fft_window increased")
            self.t_fft_window = antialias_coeff * self.t_length

        # scale domega so that it exactly partitions the range [0, omega_fft_window]
        # with a number of points with radix {2,3,4,5} (to optimize FFT) that is
        # greater than nframes.
        d_omega = 2 * const.pi / self.t_fft_window
        nomega = fft.next_fast_len(int(self.omega_fft_window // d_omega) + 1)
        self.n_omega = nomega if nomega >= self.nframes else self.nframes
        self.d_omega = self.omega_fft_window / self.n_omega
        assert self.d_omega <= maximal_domega

        # find omega and t points for FFT
        ommin = self.eval_omega_min
        ommax = ommin + self.omega_fft_window
        if ommax < self.eval_omega_max:
            rec_dt = 2 * const.pi / self.eval_omega_range
            warnings.warn(
                f"Simulation timestep is too big ({self.dt}), and frequencies considered"
                f"meaningful are turncated. Recommended minimal time step: dt < {rec_dt}"
            )
        self.fft_omegas: NDArray = ommin + np.array(
            [i * self.d_omega for i in range(self.n_omega)]
        )
        self.fft_times: NDArray = np.array([i * dt for i in range(self.n_omega)])

        # find evaluated omegas
        nev_om_index_lower, nev_om_index_upper = np.searchsorted(
            self.fft_omegas, (self.eval_omega_min, self.eval_omega_max)
        )
        self.eval_omegas: NDArray = np.split(
            self.fft_omegas, [nev_om_index_lower, nev_om_index_upper]
        )[1]
        self.n_eval_omegas = len(self.eval_omegas)
        self.n_noneval_omegas = len(self.fft_omegas) - self.n_eval_omegas
        self.eval_omega_min = self.eval_omegas[0]
        self.eval_omega_max = self.eval_omegas[-1]

        # update parameters with actual values


def gaussian_envelope(omega: Vectorized, t_pulse: float) -> Vectorized:
    """
    Gaussian spectral shape function, as used in Pu's article, expr. (3): the
    Fourier transform of a Gaussian with FWHM = params.t_pulse.
    """
    omega0 = 2 * np.pi * C_LIGHT
    ag = np.sqrt(2 * np.log(2))  # 1.1774100225154747  # == sqrt(2 * ln(2))
    # multiplier = np.sqrt(2) * const.pi * params.t_pulse / ag  #! wrong? (in a constant anyway)
    multiplier = t_pulse / (np.sqrt(2) * ag)
    exponent = -1 * t_pulse ** 2 * (omega - omega0) ** 2 / (4 * ag ** 2)
    result = multiplier * np.exp(exponent)
    return result


def poisson_envelope(omega: Vectorized, s: float) -> Vectorized:
    omega0 = 2 * np.pi * C_LIGHT
    # s = params.s
    a = 2 * const.pi * (s / omega0) ** (s + 1)
    b = np.power(omega, s)
    bb = np.exp(-s * omega / omega0)
    c = 1.0 / gamma(s + 1)
    d = np.heaviside(omega, 0)
    return a * b * c * d * bb


def poisson_envelope_2(omega: Vectorized, s: float) -> Vectorized:

    omega0 = 2 * np.pi * C_LIGHT
    # s = params.s
    m = 2 * const.pi * (s / omega0)
    o = omega / omega0
    e = (np.log(s * o) - o) * s
    ee = np.exp(e)
    g = gamma(s + 1)
    h = np.heaviside(omega, 0)
    return m * ee / g * h


def envelope(
    omega: Vectorized, envl_t_param: float, envelope_type: Literal["poisson", "gauss"]
) -> Vectorized:
    # return np.ones_like(omega)
    if envelope_type == "poisson":
        return poisson_envelope_2(np.abs(omega), envl_t_param)
    elif envelope_type == "gauss":
        return gaussian_envelope(omega, envl_t_param)
    else:
        raise NotImplementedError(
            f"only envelope types 'gauss' and 'poisson' are inplemented (given: {envelope_type})"
        )


def envelope_magnitude_range(
    envelope_type: Literal["poisson", "gauss"],
    t_pulse: float,
    ln_ratio: float,
    omega0: float,
) -> Tuple[float, float]:
    """
    Given the time-envelope function A(omega), return the D_omega where
    A(omega0 +- D_omega) = A(omega0) * 10^(-ln_ratio)

    Arguments:
        t_pulse {float} -- Temporal duration of the pulse.
        ln_ratio {float} -- decreasement order of the envelope function
    """
    ag = np.sqrt(2 * np.log(2))
    om_halfwidth = 2 * ag / t_pulse * np.sqrt(ln_ratio * np.log(10))
    if envelope_type == "poisson":
        # omega == 0 may have problems due to zero divisions  TODO: manage these properly
        ommin = 1e-20 * om_halfwidth
        ommax = omega0 + 1.5 * om_halfwidth  # just a guess
    elif envelope_type == "gauss":
        ommin = omega0 - om_halfwidth
        ommax = omega0 + om_halfwidth
    else:
        raise NotImplementedError(
            f"only envelope types 'gauss' and 'poisson' are inplemented (given: {envelope_type})"
        )
    return ommin, ommax


def statcomp(
    formula: MonoField.SpectralFormula,
    params: Any,
    component: Comp,
    r: NDArray,
    z: NDArray,
    omega: float,
) -> NDArray:
    """
    Calculate the quasi-statical (monochromatic) field at the given point in space.
    Vectorized in :param:`omega`.
    """
    return formula.component(component, r, 0, z, omega, params)


# todo remove tnull shift from here
def shaped_component(
    formula: MonoField.SpectralFormula,
    component: Comp,
    r: NDArray,
    phi: float,
    z: NDArray,
    omega: float,
    envl_t_param: float,
    params: Any,
    envelope_type: Literal["poisson", "gauss"],
    tnull: float,
    phase_derivates: List[float] = [],
) -> NDArray:
    """
    The spectral component, multiplied with:
        * the Gaussian shape function described in :meth:`shapefunc`,
        * a phase delay of exp(-i*omega*params.tnull) corresponding to the time shift,
        * and the dispersive phase calculated from the phase derivates given in
            the supplied params.

    Vectorized in :param:`omega`.
    """
    dispersive_phase = 0.0
    for i, coeff in enumerate(phase_derivates):
        dispersive_phase += coeff * (omega - OMEGA_0) ** i
    formula_res = formula.component(component, r, phi, z, omega, params)
    # nm = np.amax(np.nan_to_num(np.abs(formula_res)))
    # if nm == 0:
    #     nm = 1.0
    # print(nm)
    envl = envelope(omega, envl_t_param, envelope_type)
    tshift = np.exp(-1j * omega * tnull)
    dispersion = np.exp(1j * dispersive_phase)
    res = formula_res * envl * tshift * dispersion
    return res


def ifft_as_params(
    a: NDArray, fft_params: FFTParameters, axis: int = -1, overwrite_x: bool = False
):

    a_ifft = fft.ifft(a, n=fft_params.n_omega, axis=axis, overwrite_x=overwrite_x) / (
        2 * const.pi * fft_params.dt
    )
    # cut the values to the time range asked for, along the given axis
    sl = [slice(None)] * a_ifft.ndim
    sl[axis] = slice(fft_params.nframes)
    a_t = a_ifft[tuple(sl)]

    # compensate for omega shift (omegas start at omegamin)
    shifter = np.exp(
        1j * fft_params.eval_omega_min * fft_params.fft_times[: fft_params.nframes]
    )
    return utils.mult_along_axis(a_t, shifter, axis)


def spectrum_optaxplane(
    formula: MonoField.SpectralFormula,
    component: Comp,
    mesh: meshfield.RectMesh,
    envl_t_param: float,
    params: Any,
    fftparams: FFTParameters,
    envelope_type: Literal["poisson", "gauss"],
    phase_derivates: List[float] = [],
    multicore: bool = True,
    show_progress: bool = True,
    phi=0,
):
    "Calculate the spectrum for each point in the plane, and return in a 3d array as [omega, r, z]."
    # setup
    n_eval_omegas = fftparams.n_eval_omegas
    eval_omegas = fftparams.eval_omegas

    shaped_comp_args = {
        "formula": formula,
        "component": component,
        "r": mesh.rmesh,
        "phi": phi,
        "z": mesh.zmesh,
        # "omega": omega -- this will be set in _spectrum_optaxplane_core
        "envl_t_param": envl_t_param,
        "params": params,
        "envelope_type": envelope_type,
        "tnull": fftparams.tmin,
        "phase_derivates": phase_derivates,
    }

    multiproc_task_counter.value = 0
    args = [
        (shaped_comp_args, omega, show_progress, n_eval_omegas) for omega in eval_omegas
    ]
    if multicore:
        process_num = multiproc.cpu_count()
        with multiproc.Pool(process_num) as pool:
            spectra = np.array([*pool.map(_spectrum_optaxplane_core, args)])

    else:
        spectra = np.array([*map(_spectrum_optaxplane_core, args)])

    multiproc_task_counter.value = 0
    return spectra


def _spectrum_optaxplane_core(args: Tuple) -> NDArray:
    """
    Multithreadable core of _spectrum_optaxplane. Calculate the monochromatic
    component for a given omega.
    """
    (shaped_comp_args, omega, show_progress, omeganum) = args
    shaped_comp_args["omega"] = omega
    spectrum = np.conjugate(
        shaped_component(**shaped_comp_args)
    )  #!!!!!! Why is conjugation neccessary??

    if show_progress:
        multiproc_task_counter.value += 1
        i = multiproc_task_counter.value
        print(f"Calculated: {i}\tout of {omeganum}\t\t\t", end="\r")

    return spectrum


def tfield_optaxplane(
    formula: MonoField.SpectralFormula,
    component: Comp,
    mesh: meshfield.RectMesh,
    envl_t_param: float,
    params: Any,
    fftparams: FFTParameters,
    envelope_type: Literal["poisson", "gauss"],
    phase_derivates: List[float] = [],
    multicore: bool = True,
    show_progress: bool = True,
    phi=0,
) -> NDArray:
    """ #todo: update this!
    Calculate the field component's time-dependent value in the plane of the
    optical axis, on a mesh bounded by (0, params.rmax) on the r axis,
    and (params.zmin, params.zmax) on the z axis, with params.rpoints and
    params.zpoints ticks on them. For each point on the mesh, the temporal shape
    of the field is computed by :meth:`field.tdepcomp`. The temporal shape of the
    field on the mesh is returned as a 3D array, with axes [time, r, z].

    Arguments:
        phi {float} -- angle of the plane the field is calculated on. (Nothing		#TODO remove phi
                depends on it, so can be arbitrary. Only implemented for if
                we'll work with some non-symmetric field later.)
        params {:class:`field.FieldParameters`} -- :class:`field.FieldParameters`
            object describing the propertities of the field.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        multicore {bool} -- whether to use multicore computing. If set, the
                computation will be threaded to (cpu number - 1) threads. (default: {False})
        show_progress {bool} -- Whether to dislpay the progress. (default: {True})
        verbose {bool} -- Whether to print additional info about the complexity
                and warn if the frame number had to be decreased. (default: {True})
    """

    spectrum = spectrum_optaxplane(
        formula,
        component,
        mesh,
        envl_t_param,
        params,
        fftparams,
        envelope_type,
        phase_derivates,
        multicore,
        show_progress,
        phi,
    )
    fieldvals = ifft_as_params(spectrum, fftparams, axis=0, overwrite_x=True)
    return fieldvals


def tfield_optaxplane_from_tformula(
    formula: MonoField.TimeFormula,
    component: Comp,
    params: Any,
    mesh: meshfield.RectMesh,
    times: ArrayLike,
    multicore: bool = True,
    show_progress: bool = True,
    phi=0,
):
    """ #!
   Twin sister of :meth:`tfield_optaxplane` that works with time-domain functions
   and thus omits the iFFT. The temporal shape of the field on the mesh is returned
   as a 3D array, with axes [time, r, z].

    Arguments:
        phi {float} -- angle of the plane the field is calculated on. (Nothing		#TODO remove phi
                depends on it, so can be arbitrary. Only implemented for if
                we'll work with some non-symmetric field later.)
        params {:class:`field.FieldParameters`} -- :class:`field.FieldParameters`
            object describing the propertities of the field.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        multicore {bool} -- whether to use multicore computing. If set, the
                computation will be threaded to (cpu number - 1) threads. (default: {False})
        show_progress {bool} -- Whether to dislpay the progress. (default: {True})
    """

    # setup
    nr = mesh.nr
    nz = mesh.nz
    nframes = len(times)

    rticks = mesh.rticks
    zticks = mesh.zticks
    # frametimes = params.frametimes
    r, z = np.meshgrid(rticks, zticks, indexing="ij")
    # rr, zz omom= np.meshgrid(rticks, zticks, omega)
    with warnings.catch_warnings():
        # to hell with warnings for omega=0                                         #TODO manage this properly
        warnings.filterwarnings("ignore", "division by zero")
        warnings.filterwarnings("ignore", "invalid number")

        if multicore:
            # Multiprocessing: multi-threaded loop
            process_num = multiproc.cpu_count() - 1
            # map the necccessary arguments on the helper function
            args = [
                (formula, component, r, phi, z, t, params, show_progress, nframes)
                for t in times
            ]
            multiproc_task_counter.value = 0
            with multiproc.Pool(process_num) as pool:
                result = np.array([*pool.map(_t_calculator, args)])
            # reset task counter
            multiproc_task_counter.value = 0

        else:
            # No multiprocessing: single threaded loop

            if show_progress:
                # use progressbar if asked, else don't
                iterator = prbar(
                    enumerate(times), max_value=len(times), redirect_stdout=True
                )
            else:
                iterator = enumerate(times)

            result = np.empty((nframes, nr, nz), dtype="complex64")
            for i, t in iterator:
                result[i] = formula.component(component, r, phi, z, t, params)

    return result


def _t_calculator(arglist: Tuple) -> NDArray:
    """
    Calculate the field along a given line of constant r. Auxiliary function for
    :meth:`tfield_optaxplane` to be multi-threaded.
    """
    formula, component, r, phi, z, t, params, dispprogress, tnum = arglist
    fieldvals = formula.component(component, r, phi, z, t, params)

    if dispprogress:
        multiproc_task_counter.value += 1
        i = multiproc_task_counter.value
        print(f"Calculated: {i} out of {tnum}\t\t\t", end="\r")

    return fieldvals
