from __future__ import annotations
from enum import Enum
from scipy import constants as const
from typing import *  # Any, Callable, List, Literal, Type, TypeVar, Union, Sequence
from nptyping import NDArray

# todo: make these flags modifieable after import
NORMALIZED = True
USE_PYFFT = True


class Comp(Enum):
    E_r, E_phi, E_z, B_r, B_phi, B_z = "e_r", "e_phi", "e_z", "b_r", "b_phi", "b_z"


class Norm(Enum):
    REAL, IMAG, AMPL, ABS, PHASE, ANGLE = 1, 2, 3, 3, 4, 4

    @classmethod
    def strdict(cls):
        return {
            "re": cls.REAL,
            "real": cls.REAL,
            "im": cls.IMAG,
            "imag": cls.IMAG,
            "imaginary": cls.IMAG,
            "abs": cls.ABS,
            "ampl": cls.AMPL,
            "amplitude": cls.AMPL,
            "magnitude": cls.AMPL,
            "ang": cls.ANGLE,
            "angle": cls.ANGLE,
            "ph": cls.PHASE,
            "phase": cls.PHASE,
        }

    @staticmethod
    def fromstr(s):
        return Norm.strdict()[s]


# typing stuff
NumberType = Union[int, float, complex]
ArrayLike = Union[Sequence, NDArray]
Vectorized = TypeVar("Vectorized", float, complex, NDArray)
# ComponentStr = Literal["e_r", "e_phi", "e_z", "b_r", "b_phi", "b_z"]
NormStr = Literal["re", "im", "ampl", "abs", "phase", "angle"]

# function with signature func(r, phi, z, omega_or_t, params)
FieldCompFunction = Callable[[NDArray, float, NDArray, float, Any], NDArray]

if NORMALIZED:
    C_LIGHT: float = 1.0  # c*    so speed normalization factor = c / C_LIGHT
    LAMBDA_0: float = 1.0  # legth normalization factor = lambda0
    OMEGA_0: float = 2 * 3.141592653589793 * C_LIGHT / LAMBDA_0
    EPSILON_0: float = 1
    # C_LIGHT = 1 / (2 * const.pi)
else:
    LENGTH_UNIT = const.micro
    TIME_UNIT = const.femto
    C_LIGHT = const.c * TIME_UNIT / LENGTH_UNIT
    EPSILON_0 = const.epsilon_0 * LENGTH_UNIT ** 3 / TIME_UNIT ** 4

# if USE_PYFFT:
#     from pyfftw.interfaces import scipy_fftpack  # TODO: benchmark this vs scipy

#     fft = scipy_fftpack
# else:
#     from scipy import fftpack

#     fft = fftpack
fft = None
