#!/opt/anaconda3/bin/python
# -*- coding: utf-8 -*-

"""Process the data computed by tempfield.py: read it, display it, and/or
calculate the total energy of the pulse"""

import os
from typing import Any, List, Optional, Union, cast

import matplotlib as mpl
from matplotlib.pyplot import savefig
import numpy as np
from matplotlib import animation as animation
from matplotlib import pyplot as plt
from numpy import linalg
from progressbar import progressbar as prbar
from scipy import constants as const
from scipy import integrate as intg
from .colormaps import *

from . import (
    NormStr,
    NDArray,
    Norm,
    Comp,
    C_LIGHT,
    OMEGA_0,
    FieldFFT,
    FieldVector,
    MonoField,
    Parameters,
    meshfield,
    physutils,
    utils,
)
from .Fields import LevyRadpolField as levycomps
from .Fields import RWFields
from .meshfield import RectMesh

# set up pyplot stuff

FIGSIZE = (10, 6.25)

mpl.rcParams["figure.figsize"] = FIGSIZE


# ~~ Helpers
################################################################################


def get_norm(
    scale: str,
    min: float,
    max: float,
    logrange: float = 10,
    symlog_linscale: float = 0.03,
) -> Any:
    "Get the appropiate type of norm"
    if scale == "lin":
        norm = mpl.colors.Normalize(vmin=min, vmax=max)

    elif scale == "log":
        norm = mpl.colors.LogNorm(vmin=(max * 10 ** (-logrange)), vmax=max, base=10)

    elif scale == "symlog":
        norm = mpl.colors.SymLogNorm(
            vmin=min,
            vmax=max,
            linthresh=(max * 10 ** (-logrange)),
            linscale=symlog_linscale,
            base=10,
        )
    else:
        raise ValueError("scale should be 'lin', 'log' or 'symlog'")

    return norm


def mirror_frame(frame: NDArray, m: int = 0) -> NDArray:
    "Mirror the frame around the r=0 line"
    reflection = np.flip(frame, axis=0)
    mirrored = np.concatenate((reflection[:-1, :], frame), axis=0)
    mirrored = mirrored * np.exp(1j * np.pi * m)
    return mirrored


def mirror_rticks(rticks: NDArray) -> NDArray:
    "get the r ticks of the mirrored frame"
    mirror_ticks = -1 * np.flip(rticks)
    mirrored_axcoords = np.concatenate((mirror_ticks[:-1], rticks), axis=0)
    return mirrored_axcoords


def spin_zline(
    frame: NDArray, z: float, thetanum: int, m: int = 0, interpolate=False, zticks=None
) -> NDArray:
    '"Spin" the (phi, z)=constant line around the optical axis'  # BUG: zticks is NOT optional!
    if z < zticks[0] or z > zticks[-1]:
        raise ValueError("z coordinate out of bounds")
    z_index = np.searchsorted(zticks, z)
    z_line = frame[:, z_index]
    if interpolate:
        assert zticks is not None
        dz = zticks[z_index + 1] - zticks[z_index]
        dframe = frame[:, z_index + 1] - frame[:, z_index]
        diff = dframe / dz
        z_line += diff * (z - zticks[z_index])
    spinned = np.tile(np.transpose(z_line), (thetanum, 1))
    thetas = np.tile(
        np.linspace(0, 2 * np.pi, thetanum), (frame.shape[0], 1)
    ).transpose()
    spinned *= np.exp(1j * m * thetas)
    return spinned


def spin_rticks(rticks: NDArray, thetanum: int) -> NDArray:
    "get the x, y coordinates for the spinned z-line"
    angl = np.linspace(0, 2 * const.pi, thetanum)
    r, thetas = np.meshgrid(rticks, angl)
    x = r * np.cos(thetas)
    y = r * np.sin(thetas)
    return (x, y)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ~~ AT-CALCULATION PLOTTERS ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


# ~~ Pre-calculation info
################################################################################


def pulse_shape(
    fieldvec: FieldVector.FieldFromSpectralFormula,
    component: Comp,
    r: float,
    phi: float,
    z: float,
    mesh: Optional[RectMesh] = None,
    multicore=True,
    title=None,
    save: bool = False,
    savepath: Optional[str] = None,
) -> None:
    """
    Plot the field's amplitude and phase on the omega and the time domain at a
    given point
    """

    params = fieldvec.params
    omegas = fieldvec.fft_params.eval_omegas
    formula = cast(MonoField.SpectralFormula, fieldvec.formula)
    gauss = np.zeros_like(omegas, dtype="complex")
    freqvals = np.zeros_like(omegas, dtype="complex")

    if mesh is None:
        mesh = fieldvec.mesh

    ri = mesh.get_r_index(r)
    zi = mesh.get_z_index(z)

    # for ion in

    for i, omega in enumerate(omegas):
        gauss[i] = FieldFFT.envelope(
            omega, fieldvec.envl_t_param, fieldvec.envelope_type
        )

    freqs = FieldFFT.spectrum_optaxplane(
        formula,
        component,
        mesh,
        fieldvec.envl_t_param,
        params,
        fieldvec.fft_params,
        fieldvec.envelope_type,
        fieldvec.phase_derivates,
        multicore=multicore,
        show_progress=False,
        phi=phi,
    )
    freqvals = freqs[:, ri, zi]

    field = FieldFFT.tfield_optaxplane(
        formula=fieldvec.formula,
        component=component,
        mesh=mesh,
        envl_t_param=fieldvec.envl_t_param,
        params=fieldvec.params,
        fftparams=fieldvec.fft_params,
        envelope_type=fieldvec.envelope_type,
        phase_derivates=fieldvec.phase_derivates,
        multicore=multicore,
        show_progress=False,
    )
    fieldvals = field[:, ri, zi]

    if hasattr(fieldvec.params, "m"):
        freqvals *= np.exp(1j * phi)
        fieldvals *= np.exp(1j * phi)

    # iz0 = np.searchsorted(mesh.zticks, 0)

    # norm = Norm.ABS
    # freqs = get_norm(freqs)
    # plt.pcolormesh(mesh.zticks, mesh.rticks, freqs.real[10], shading="nearest")
    # plt.colorbar()
    # plt.show()
    # # plt.show()
    # # plt.pcolormesh(mesh.zticks, mesh.rticks, freqs.real[10], shading="nearest")
    # # plt.colorbar()
    # # plt.show()
    # print(freqvals[20])
    # plt.pcolormesh(mesh.zticks, mesh.rticks, freqs.real[20], shading="nearest")
    # plt.colorbar()
    # plt.show()
    # plt.pcolormesh(mesh.zticks, mesh.rticks, freqs.real[40], shading="nearest")
    # plt.colorbar()
    # plt.show()
    # plt.pcolormesh(rs, zs, fieldvals[int(len(fieldvals) // 2), :])
    # plt.show()

    freq_ampl = np.abs(freqvals)
    freq_phase = np.unwrap(np.angle(freqvals), discont=const.pi)
    time_ampl = np.abs(fieldvals)
    time_phase = np.unwrap(np.angle(fieldvals), discont=const.pi)

    normomegas = omegas / (OMEGA_0)  # / fieldparams.omega0
    times = fieldvec.t

    plot_settings = {
        "marker": ".",
        "markersize": 1.5,
        "linestyle": "-",
        "linewidth": 0.5,
    }

    fig = plt.figure()
    supt = (
        title if title is not None else rf"Pulse shape at $r={r}, \varphi={phi}, z={z}$"
    )
    fig.suptitle(supt)
    ax_freq_amp = fig.add_subplot(221)
    ax_freq_phs = fig.add_subplot(223, sharex=ax_freq_amp)
    ax_time_amp = fig.add_subplot(222)
    ax_time_phs = fig.add_subplot(224, sharex=ax_time_amp)

    # /np.amax(freq_ampl)
    ax_freq_amp.plot(
        normomegas, freq_ampl, color="green", label="$|E(\omega)|$", **plot_settings
    )
    ax_freq_amp.plot(
        normomegas,
        np.abs(gauss) / np.amax(np.abs(gauss)) * np.amax(freq_ampl),
        color="blue",
        label="envelope",
        alpha=0.5,
        **plot_settings,
    )
    ax_freq_phs.plot(normomegas, freq_phase, color="green", **plot_settings)
    ax_time_amp.plot(
        times, fieldvals.real, color="green", label="$Re\{E(t)\}$", **plot_settings
    )
    ax_time_amp.plot(
        times, time_ampl, color="blue", label="$|E(t)|$", **plot_settings, alpha=0.7
    )
    ax_time_phs.plot(times, time_phase, color="red", **plot_settings)

    ax_freq_amp.set_xlabel(r"$\nu$ ($\nu_0$ units)")
    ax_freq_phs.set_xlabel(r"$\nu$ ($\nu_0$ units)")
    ax_freq_amp.set_ylabel(r"$|E(\nu)|$")
    ax_freq_phs.set_ylabel(r"$\phi(\nu) / \pi)$")

    ax_time_amp.set_xlabel(r"$t$ ($ns$)")
    ax_time_phs.set_xlabel(r"$t$ ($ns$)")
    ax_time_amp.set_ylabel(r"$|E(t)|$")
    ax_time_phs.set_ylabel(r"$\phi(t) / \pi$")

    ax_freq_amp.set_title("Spectral amplitude")
    ax_freq_phs.set_title("Spectral phase")
    ax_time_amp.set_title("Amplitude")
    ax_time_phs.set_title("Phase")

    # plot sine wave in time domain
    period = 2 * const.pi / OMEGA_0
    n = int((times[-1] - times[0]) // (period / 20))
    sinx = np.linspace(times[0], times[-1], n)
    siny = np.sin(OMEGA_0 * sinx) * np.amax(fieldvals.real)
    ax_time_amp.plot(
        sinx,
        siny,
        color="gray",
        linewidth=0.5,
        marker="",
        alpha=0.3,
        label="$\omega_0$ osc.",
    )

    ax_freq_amp.legend()
    ax_time_amp.legend()

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


# TODO: fix this
def plot_pu_integrand(
    comp: Comp,
    r: float,
    z: float,
    omega: float,
    pms: Parameters.RwParameters,
    n: int = 1000,
) -> None:
    """
    Plot the Pu expression's integrand at a given point.
    """
    thetamax = pms.alpha
    thetavals = np.linspace(0, thetamax, n)
    integrand = np.empty(n, dtype="complex64")
    for i, th in enumerate(thetavals):
        integrand[i] = RWFields.integrand(th, r, 0, z, omega, pms, comp)
        # integrand[i] = PuRadpolField.pu_integrand(th, r, 0, z, omega, pms, comp_name)

    plt.plot(thetavals, integrand.real, "b-", label="real")
    plt.plot(thetavals, integrand.imag, "r-", label="imag")
    plt.legend()
    plt.xlabel("theta (°)")
    plt.ylabel("integrand")
    plt.title("Pu integrand\nr={:.1f}um  z={:.1f}um  ω={:.1f} 1/fs".format(r, z, omega))

    plt.show()


# ~~ Monochromatic components
################################################################################

# TODO: should use field.get_monochromatic_comp

# TODO fix
def monochr_adj(
    field: FieldVector.FieldFromSpectralFormula,
    comp: Comp,
    z: float,
    omega: float,
    fieldparams,
    mesh,
    qty="ampl",
    scale="lin",
    logrange=10,
    cmap="gnuplot",
    draw_e_radii=True,
    savepath=None,
):
    """
    Plot and show the intensity of the monocromatic component with the given
    frequency in a plane adjacent to the optical axis."
    """

    xvals = yvals = mirror_rticks(mesh.rticks)

    # get the field value for all points
    e = np.empty((len(xvals), len(yvals)), dtype="complex")
    for i, x in prbar(enumerate(xvals), max_value=len(xvals), redirect_stdout=True):
        for j, y in enumerate(yvals):
            r, phi, z = physutils.cart2cylind(x, y, z)
            e[i, j] = field.formula.component(comp, r, phi, z, omega, fieldparams)

    # plot the quantity with the correct scale and value:
    ampl = physutils.get_qty(e, qty)
    norm = get_norm(
        scale, ampl.min(), ampl.max(), logrange=logrange, symlog_linscale=0.03
    )
    plt.pcolormesh(xvals, yvals, ampl, cmap=cmap, norm=norm)

    # draw the points where the intensity in the z direction drops
    #   below 1/e times the maximal intensity on the plane
    if draw_e_radii:
        # find 1/e line of the squared amplitude adjacent to the optical axis
        intensity = np.abs(e) ** 2
        lim = np.amax(intensity) / np.e ** 2
        bounds = []
        for i, xline in enumerate(intensity[1:-1, 1:-1], start=1):
            for j, yline in enumerate(xline, start=1):
                if intensity[i, j] < lim and (
                    intensity[i, j + 1] < lim
                    and intensity[i, j - 1] > lim
                    or intensity[i, j - 1] < lim
                    and intensity[i, j + 1] > lim
                    or intensity[i + 1, j] < lim
                    and intensity[i - 1, j] > lim
                    or intensity[i - 1, j] < lim
                    and intensity[i + 1, j] > lim
                ):
                    bounds.append([xvals[i], yvals[j]])
        abounds = np.array(bounds)
        if len(abounds) > 0:
            plt.plot(
                abounds[:, 0],
                abounds[:, 1],
                color="white",
                marker=".",
                linestyle="",
                markersize=0.75,
                label=r"1/e bound",
            )

    # decorating
    leftdown = -mesh.rmax  #!!!!
    plt.text(
        0.8 * leftdown,
        -1 * leftdown,
        "z={:.3}μm".format(z / const.micro),
        ha="center",
        va="center",
        fontdict={"color": "white", "size": 14},
    )
    plt.colorbar()
    plt.xlabel(r"$x (\mu m)$")
    plt.ylabel(r"$y (\mu m)$")
    plt.xlim(-mesh.rmax, mesh.rmax)
    plt.ylim(-mesh.rmax, mesh.rmax)
    if draw_e_radii:
        plt.legend()
    plt.gcf().set_size_inches(20, 11.25)  # 1920x1080 with dpi=96

    if savepath is not None:
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.gcf().savefig(savepath, metadata={"artist": "Gergely"})
    else:
        plt.show()


def monochr_optax(
    fieldvec: FieldVector.FieldFromSpectralFormula,
    component: Comp,
    omega: Optional[float] = None,
    qty: Norm = Norm.AMPL,
    scale: str = "lin",
    logrange: float = 10,
    cmap: str = "gnuplot",
    draw_e_radii=True,
    draw_thetaline=True,
    savepath: Optional[str] = None,
) -> None:
    """
    Plot and show the intensity of the monocromatic component with the given
    frequency at the plane of the optical axis."
    """
    fieldparams = fieldvec.params
    mesh = fieldvec.mesh

    try:
        m = fieldparams.m
    except:
        m = 0

    rvals = mirror_rticks(mesh.rticks)
    zvals = mesh.zticks

    # get the field value for all points
    e = fieldvec.get_monochromatic_comp(component, omega)
    # e = np.empty((len(rvals), len(zvals)), dtype="complex")
    # for i, r in prbar(enumerate(rvals), max_value=len(rvals), redirect_stdout=True):
    #     for j, z in enumerate(zvals):
    #         e[i, j] = fieldvec.statcomp(np.abs(r), phi, z, omega, fieldparams)
    e = mirror_frame(e, m)
    # plot the quantity with the correct scale and value:
    ampl = physutils.get_qty(e, qty)
    norm = get_norm(
        scale, ampl.min(), ampl.max(), logrange=logrange, symlog_linscale=0.03
    )
    plt.pcolormesh(zvals, rvals, ampl, cmap=cmap, norm=norm)

    # draw lines in theta angle
    if draw_thetaline:
        divangle = fieldparams.theta
        thetaline = zvals * np.tan(divangle)
        plt.plot(zvals, thetaline, "r", linewidth=0.75, label=r"$\theta$ line")
        plt.plot(zvals, -thetaline, "r", linewidth=0.75)

    # draw the points where the intensity in the z direction drops
    #   below 1/e times the max at that z coordinate
    if draw_e_radii:
        # find 1/e line of the squared amplitude adjacent to the optical axis
        intensity = np.abs(e) ** 2
        bounds = []
        for z_i, rline_intensity in enumerate(intensity.transpose()):
            lim = np.amax(rline_intensity) / np.e ** 2
            for i in range(len(rline_intensity) - 1):
                if (
                    rline_intensity[i] < lim <= rline_intensity[i + 1]
                    or rline_intensity[i] > lim >= rline_intensity[i + 1]
                ):
                    bounds.append([zvals[z_i], rvals[i]])
        abounds = np.array(bounds)
        if len(bounds) > 0:
            plt.plot(
                abounds[:, 0],
                abounds[:, 1],
                color="white",
                marker=".",
                linestyle="",
                markersize=0.75,
                label=r"1/e bound",
            )

    # decorating
    plt.colorbar()
    plt.xlabel(r"$z (\mu m)$")
    plt.ylabel(r"$r (\mu m)$")
    plt.xlim(mesh.zmin, mesh.zmax)
    plt.ylim(-mesh.rmax, mesh.rmax)
    if draw_e_radii or draw_thetaline:
        plt.legend()

    if savepath is not None:
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.gcf.savefig(savepath, metadata={"artist": "Gergely"})
    else:
        plt.show()


# TODO update
def monochr_adj_line(
    z,
    omega,
    fieldparams,
    mesh,
    qty="re",
    title="",
    plot_gaussian=False,
    plot_fwhm=False,
):
    """
    Plot and show the intensity of the monocromatic component with the given
    frequency at the given z coordinate."
    """

    xvals = mirror_rticks(mesh.rticks)
    vals = np.empty(len(xvals), dtype="complex64")
    for i, xval in enumerate(xvals):
        vals[i] = field.statcomp(np.abs(xval), 0, z, omega, fieldparams)

    plotvals = physutils.get_qty(vals, qty)
    plt.plot(xvals, plotvals, "r-")

    if plot_gaussian:
        gaussvals = np.abs(
            levycomps.gaussian_beam(np.abs(xvals), 0, z, omega, fieldparams.theta)
        )
        plt.plot(xvals, gaussvals, "b-", alpha=0.5)
    if plot_fwhm:
        xmin, xmax = -mesh.rmax, mesh.rmax
        plt.hlines(
            np.amax(np.abs(vals)) * 2.7182818 ** (-2),
            xmin=xmin,
            xmax=xmax,
            colors="r",
            linestyles="--",
        )
    if plot_gaussian:
        plt.hlines(
            np.amax(gaussvals) * 2.7182818 ** (-2),
            xmin=xmin,
            xmax=xmax,
            colors="b",
            linestyles="--",
            alpha=0.5,
        )

    plt.show()


def total_energy(
    fieldvec: FieldVector.FieldVector,
    time_avg=True,
    exclude_optax=True,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """ Plot the total and per-component electromagnetic energy on the field vs
    time (as the integral of the energy density)

    Arguments:
        elm_field {:class:`saved_field_vector`} -- the field which's energy
                is calculated
        step {int} -- step along the time point indices (for saving memory
                in case of large meshes)
        savepath {path} -- path where the result should be saved. If None, it's displayed instead.
    """

    # setup
    step = 1
    frametimes = fieldvec.frametimes
    nframes = fieldvec.nframes
    rpoints = np.abs(fieldvec.mesh.rticks)
    zpoints = fieldvec.mesh.zticks
    r, z = np.meshgrid(rpoints, zpoints)

    # get energy values
    e_total = np.zeros(nframes // step, dtype=float)
    for i in range(len(e_total)):
        e_total[i] = fieldvec.get_frame_energy(i, time_avg, exclude_optax)

    # construct plot
    plottimes = frametimes[::step]
    plot_settings = {
        "marker": ".",
        "markersize": 1.5,
        "linestyle": "-",
        "linewidth": 0.5,
    }

    fig, ax = plt.subplots()
    ax.plot(plottimes, e_total, color="black", label=r"Total", **plot_settings)

    # decorate
    ax.set_title("total electromagnetic energy on the mesh")
    ax.set_xlabel("t (fs)")

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


# TODO update
def component_energy(
    fieldvec: FieldVector.FieldVector,
    step=1,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """ Plot the total and per-component electromagnetic energy on the field vs
    time (as the integral of the energy density)

    Arguments:
        elm_field {:class:`saved_field_vector`} -- the field which's energy
                is calculated
        step {int} -- step along the time point indices (for saving memory
                in case of large meshes)
        savepath {path} -- path where the result should be saved. If None, it's displayed instead.
    """

    # setup
    # fieldparams = elm_field.fieldparams
    # mesh = elm_field.mesh
    frametimes = fieldvec.frametimes
    nframes = fieldvec.nframes
    rpoints = np.abs(fieldvec.mesh.rticks)
    zpoints = fieldvec.mesh.zticks
    r, z = np.meshgrid(rpoints, zpoints)

    # get energy values
    en_er = np.zeros(nframes // step, dtype=float)
    en_ez = np.zeros(nframes // step, dtype=float)
    en_b = np.zeros(nframes // step, dtype=float)
    e_total = np.zeros(nframes // step, dtype=float)
    for i in prbar(range(0, nframes, step), max_value=nframes // step):
        en_er[i] = fieldvec.get_component_frame_energy(Comp.E_r, i)
        en_ez[i] = fieldvec.get_component_frame_energy(Comp.E_z, i)
        en_b[i] = fieldvec.get_component_frame_energy(Comp.E_phi, i)
        e_total[i] = en_er[i] + en_ez[i] + en_b[i]

    # construct plot
    plottimes = frametimes[::step]
    plot_settings = {
        "marker": ".",
        "markersize": 1.5,
        "linestyle": "-",
        "linewidth": 0.5,
    }

    fig, ((totax, erax), (ezax, bax)) = plt.subplots(2, 2)
    erax.plot(plottimes, en_er, color="red", label=r"$E_r$", **plot_settings)
    ezax.plot(plottimes, en_ez, color="green", label=r"$E_z$", **plot_settings)
    bax.plot(plottimes, en_b, color="blue", label=r"$B_{\varphi}$", **plot_settings)
    totax.plot(plottimes, e_total, color="black", label=r"Total", **plot_settings)

    # decorate
    totax.set_title("total electromagnetic energy on the mesh")
    erax.set_title(r"total energy of the $E_r$ component")
    ezax.set_title(r"total energy of the $E_z$ component")
    bax.set_title(r"total energy of the $B_{\varphi}$ component")
    totax.set_xlabel("t (fs)")
    erax.set_xlabel("t (fs)")

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


def centroid_speed(
    fieldvec: FieldVector.FieldVector,
    save: bool = False,
    savepath: Optional[str] = None,
) -> None:
    """ Plot the total and per-component electromagnetic energy on the field vs
    time (as the integral of the energy density)

    Arguments:
        elm_field {:class:`saved_field_vector`} -- the field which's energy
                is calculated
        step {int} -- step along the time point indices (for saving memory
                in case of large meshes)
        savepath {path} -- path where the result should be saved. If None, it's displayed instead.
    """

    # setup
    frametimes = fieldvec.frametimes
    nframes = fieldvec.nframes

    # get centroid coordinates
    z_centr = np.zeros(nframes, dtype=float)
    for i in prbar(range(0, len(fieldvec)), max_value=len(fieldvec)):
        z_centr[i] = fieldvec.get_centroid_zcoord(i)

    centr_velocity = np.gradient(z_centr, frametimes) / C_LIGHT

    # construct plot
    plot_settings = {
        "marker": ".",
        "markersize": 1.5,
        "linestyle": "-",
        "linewidth": 0.5,
    }

    fig, ax = plt.subplots()
    ax.plot(frametimes, centr_velocity, color="red", **plot_settings)

    # decorate
    ax.set_xlabel("t (\lambda_0 / c)")
    ax.set_ylabel(r"$v_{centr}$ (c)")

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


# ~~ Four-plot plotters
################################################################################

# TODO: update
def phase_ampl_frame(
    fieldvec: FieldVector.FieldVector,
    frame: int,
    component: Comp,
    scale="lin",
    ampl_cmap="gnuplot",
    phase_cmap="jet",
    title="",
    savepath=None,
    invisible=False,
    fps=20,
    logrange=10,
    rlims=None,
    zlims=None,
    omegalims=None,
):
    """Plot the given frame of the component on four plots:
        * one for the real part of the field
        * one for the phase of the field
        * one for the magnitude of the field Fourier-transformed along
                the z axis (as z = c*t)
        * and one for the phase of that Fourier transform.

    Code is extracted from  :meth:`plot_field_phase_ampl`.
    """
    mesh = fieldvec.mesh

    try:
        m = fieldvec.m
    except:
        m = 0

    # frame on which the pulse is at the focal point
    fr = fieldvec.get_comoving_component_frame(component, frame)
    # (note that here we also get the FFT omegas used for the FFT plot)
    z_omegas, fft_fr = meshfield.get_z_fourier_frame(fr, mesh)

    fr = mirror_frame(fr, m)
    fft_fr = mirror_frame(fft_fr, m)
    r = mirror_rticks(mesh.rticks)
    z = mesh.zticks
    om = z_omegas / OMEGA_0  # z omegas normed by the central angular frequency

    zre = np.real(fr)
    zph = np.angle(fr) / (const.pi)
    fft = np.abs(fft_fr)
    fph = np.angle(fft_fr) / (const.pi)

    fieldmin = np.amin(zre)
    fieldmax = np.amax(zre)
    fftmin = np.amin(fft)
    fftmax = np.amax(fft)

    if abs(fieldmin) < abs(fieldmax):
        fieldmin = -fieldmax
    else:
        fieldmax = -fieldmin

    # * choose the normalization type for the colormap according to `scale`
    phnorm = get_norm("lin", -1, 1)  # phase plot is always linear
    zrenorm = get_norm(
        scale, fieldmin, fieldmax, logrange=logrange, symlog_linscale=0.03
    )
    fftnorm = get_norm(scale, fftmin, fftmax, logrange=logrange, symlog_linscale=0.03)

    # * create the plot
    fig, ((zreax, zphax), (fftax, fphax)) = plt.subplots(
        nrows=2, ncols=2, sharex=True, sharey="row", constrained_layout=True
    )
    zre_plot = zreax.pcolormesh(r, z, zre.transpose(), cmap=ampl_cmap, norm=zrenorm)
    zph_plot = zphax.pcolormesh(r, z, zph.transpose(), cmap=phase_cmap, norm=phnorm)
    fft_plot = fftax.pcolormesh(r, om, fft.transpose(), cmap=ampl_cmap, norm=fftnorm)
    fph_plot = fphax.pcolormesh(r, om, fph.transpose(), cmap=phase_cmap, norm=phnorm)

    zrecbar = fig.colorbar(zre_plot, ax=zreax, extend="max")
    zphcbar = fig.colorbar(zph_plot, ax=zphax, extend="max")
    fftcbar = fig.colorbar(fft_plot, ax=fftax, extend="max")
    fphcbar = fig.colorbar(fph_plot, ax=fphax, extend="max")

    # txt = zphax.text(0.1, 0.01, 't={:.3f}fs'.format((frametimes[frame])),
    #          ha='center', va='center', transform=fig.transFigure, fontdict={'color':'black', 'size':14})

    # * decorate
    fig.suptitle(title)
    zreax.set_xlabel(r"$r (μ m)$")
    zphax.set_xlabel(r"$r (μ m)$")
    fftax.set_xlabel(r"$r (μ m)$")
    fphax.set_xlabel(r"$r (μ m)$")

    zreax.set_ylabel(r"$z (μ m)$")
    zphax.set_ylabel(r"$z (μ m)$")
    fftax.set_ylabel(r"$ω (\omega0 units)$")
    fphax.set_ylabel(r"$ω (\omega0 units)$")

    compstr = {
        Comp.E_r: "$E_r$",
        Comp.E_phi: "$E_{phi}$",
        Comp.E_z: "$E_z$",
        Comp.B_r: "$B_r$",
        Comp.B_phi: "$B_{phi}$",
        Comp.B_z: "$B_z$",
    }
    zreax.set_title("Re(" + compstr[component] + ")")
    zphax.set_title("Phase of " + compstr[component])
    fftax.set_title("Abs(fft of " + compstr[component] + ")")
    fphax.set_title("Phase of fft of " + compstr[component])

    fig.set_size_inches(20, 11.25)
    zrecbar.set_label("Re(" + compstr[component] + ") (arb. units)")
    zphcbar.set_label("Phase(" + compstr[component] + r") / $\pi$")
    fftcbar.set_label("Abs(fft of " + compstr[component] + ") (arb. units)")
    fphcbar.set_label("Phase(fft of " + compstr[component] + r") / $\pi$")

    if rlims is not None:
        zreax.set_xlims(*rlims)  # x axis is shared among all subplots
    if zlims is not None:
        zreax.set_ylims(*zlims)
    if omegalims is not None:
        fftax.set_ylim(*omegalims)

    if savepath is not None:
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        fig.savefig(savepath, metadata={"artist": "Gergely"})

    if not invisible:
        plt.show()

    plt.close(fig)


# TODO: update
def phase_ampl_anim(
    fieldvec: FieldVector.FieldVector,
    component: Comp,
    scale="lin",
    ampl_cmap="gnuplot",
    phase_cmap="jet",
    title="",
    savepath=None,
    invisible=False,
    fps=20,
    logrange=10,
    rlims=None,
    zlims=None,
    omegalims=None,
    clip_phase_threshold=None,
):
    """Plot the given component of the field on four syncronised animated plots:
        * one for the real part of the field
        * one for the phase of the field
        * one for the magnitude of the field Fourier-transformed along the z axis (as z = c*t)
        * and one for the phase of that Fourier transform.

    Code is based on :meth:`meshfield.plot_phiplane_tdep_field`.
    """

    mesh = fieldvec.mesh
    frametimes = fieldvec.frametimes

    try:
        m = fieldvec.m
    except:
        m = 0

    focal_frame_index = fieldvec.get_t_index(0.0)
    # frame on which the pulse is at the focal point
    focal_frame = fieldvec.get_component_frame(component, focal_frame_index)
    # note that here we also get the FFT omegas used for the FFT plot
    z_omegas, focal_fft_frame = meshfield.get_z_fourier_frame(focal_frame, mesh)

    # * get the min and max values and set the scale
    fieldmin = np.amin(np.real(focal_frame))
    fieldmax = np.amax(np.real(focal_frame))
    fftmin = np.amin(np.abs(focal_fft_frame))
    fftmax = np.amax(np.abs(focal_fft_frame))
    if abs(fieldmin) < abs(fieldmax):
        fieldmin = -fieldmax
    else:
        fieldmax = -fieldmin

    phnorm = get_norm("lin", 0, 1)  # phase plot is always linear
    zrenorm = get_norm(
        scale, fieldmin, fieldmax, logrange=logrange, symlog_linscale=0.03
    )
    fftnorm = get_norm(scale, fftmin, fftmax, logrange=logrange, symlog_linscale=0.03)

    # * calculate  coordinates and get initial values from the first frame (to setup the plot)
    r = mirror_rticks(mesh.rticks)
    z = mesh.zticks
    om = z_omegas
    frame0 = fieldvec.get_component_frame(component, 0)
    frame0 = mirror_frame(frame0, m)
    zre0 = np.real(frame0)
    zph0 = np.angle(frame0) / (2 * const.pi) + 0.5
    fft0 = np.abs(frame0)
    fph0 = np.angle(frame0) / (2 * const.pi) + 0.5

    # * calculate coordinates
    # z omegas normed by the central angular frequency

    # * construth the figure using the first frame:
    fig, axes = plt.subplots(
        nrows=2, ncols=2, sharex=True, sharey="row"
    )  # , constrained_layout=True)
    (zreax, zphax), (fftax, fphax) = axes
    zre_plot = zreax.pcolormesh(r, z, zre0.transpose(), cmap=ampl_cmap, norm=zrenorm)
    zph_plot = zphax.pcolormesh(r, z, zph0.transpose(), cmap=phase_cmap, norm=phnorm)
    fft_plot = fftax.pcolormesh(r, om, fft0.transpose(), cmap=ampl_cmap, norm=fftnorm)
    fph_plot = fphax.pcolormesh(r, om, fph0.transpose(), cmap=phase_cmap, norm=phnorm)
    txt = zphax.text(
        0.1,
        0.01,
        "",
        ha="center",
        va="center",
        transform=fig.transFigure,
        fontdict={"color": "black", "size": 14},
    )
    zrecbar = fig.colorbar(zre_plot, ax=zreax, extend="max")
    zphcbar = fig.colorbar(zph_plot, ax=zphax, extend="max")
    fftcbar = fig.colorbar(fft_plot, ax=fftax, extend="max")
    fphcbar = fig.colorbar(fph_plot, ax=fphax, extend="max")

    # * set plot limits
    if rlims is not None:
        zreax.set_xlims(*rlims)
    if zlims is not None:
        zreax.set_ylims(*zlims)
    if omegalims is not None:
        fftax.set_ylim(*omegalims)

    # * decorate
    fig.suptitle(title)
    zreax.set_xlabel(r"$r (\mu m)$")
    zphax.set_xlabel(r"$r (\mu m)$")
    fftax.set_xlabel(r"$r (\mu m)$")
    fphax.set_xlabel(r"$r (\mu m)$")

    zreax.set_ylabel(r"$z (\mu m)$")
    zphax.set_ylabel(r"$z (\mu m)$")
    fftax.set_ylabel(r"$\omega (\omega0 units)$")
    fphax.set_ylabel(r"$\omega (\omega0 units)$")

    zreax.set_title("Real part of " + component.value)
    zphax.set_title("Phase of " + component.value)
    fftax.set_title("Amplitude of FT(" + component.value + ")")
    fphax.set_title("Phase of FT(" + component.value + ")")

    # zrecbar.set_label("Re(" + component +")")
    # zphcbar.set_label("Phase of " + component + ")")
    # fftcbar.set_label("Abs(fft of " + component + ")")
    # fphcbar.set_label("Phase of " + component + ")")

    # * initialize the animation:
    def anim_init():
        zre_plot.set_array([])
        zph_plot.set_array([])
        fft_plot.set_array([])
        fph_plot.set_array([])
        txt.set_text("")
        return txt, zre_plot, zph_plot, fft_plot, fph_plot

    # * update the plot to the ith frame:
    def animate(i):
        print("Processing frame {} out of {}".format(i, len(frametimes)), end="\r")
        frame = fieldvec.get_comoving_component_frame(component, i)
        fft_frame = meshfield.get_z_fourier_frame(frame, mesh)[1]
        frame = mirror_frame(frame, m)
        fft_frame = mirror_frame(fft_frame, m)
        zre = np.real(frame)
        zph = np.angle(frame) / (2 * const.pi) + 0.5
        fft = np.abs(fft_frame)
        fph = np.angle(fft_frame) / (2 * const.pi) + 0.5
        if clip_phase_threshold is not None:
            zamp = np.abs(zre)
            zmax = np.amax(zamp)
            zmask = zamp < zmax * 10 ** clip_phase_threshold
            zph = np.ma.array(zph, mask=zmask)
            famp = np.abs(fft)
            fmax = np.amax(famp)
            fmask = famp < fmax * 10 ** clip_phase_threshold
            fph = np.ma.array(fph, mask=fmask)
        # zph[]
        zre_plot.set_array(zre[:-1, :-1].transpose().flatten())
        zph_plot.set_array(zph[:-1, :-1].transpose().flatten())
        fft_plot.set_array(
            fft[:-1, :-1].transpose().flatten()
        )  # phase plot is always linear
        fph_plot.set_array(fph[:-1, :-1].transpose().flatten())
        txt.set_text("t={:.3f}fs".format(frametimes[i]))
        return txt, zre_plot, zph_plot, fft_plot, fph_plot

    # * construct and save/display the animation
    animated_fig = animation.FuncAnimation(
        fig,
        animate,
        init_func=anim_init,
        frames=len(frametimes),
        interval=1000 / fps,
        repeat=True,
        save_count=10,
        blit=True,
    )

    if savepath is not None:
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        animated_fig.save(savepath, dpi=96, metadata={"artist": "Gergely"})

    else:
        plt.show()

    plt.close(fig)


def optax_anim(
    fieldvec: FieldVector.FieldVector,
    component: Comp,
    qty: Norm,
    scale="lin",
    cmap="gnuplot",
    title="",
    fps=20,
    logrange=10,
    rlims=None,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot the result of :class:`tfield_optaxplane` on an animated colormap,
    symmetrical around r=0.

    Arguments:
        z {float} z coordinate of the plane the field is calculated on.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        fieldvals {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        qty {str} -- indicates which quantity to plot:
                * 're': real part of the field
                * 'im': imaginary part of the field
                * 'ampl': amplitude of the field
                * 'phase': phase of the field
                (default: {'re'})
        cmap {str} -- Matplotlib color map used for the plot.  (default: {'jet'})
        scale {str} -- the kind of scale used for the color map:
                * 'lin': linear
                * 'log': logarithmic from zero (negative values are not plotted)
                * 'symlog': two-way logarithmic symmetrical to 0
                (default: {'lin'})
        title {str} -- Title of the resulting animated plot. (default: {''})
        savepath {path} -- path for the saved animation. If None, the animation
                won't be saved. (default: {None})
        invisible {bool} -- Whether to display the animation or not. (default: {False})
        logscal_range {float}: magnitude range of the logarithmic scale (if used)
    """
    rticks = fieldvec.mesh.rticks
    r = mirror_rticks(rticks)
    z = fieldvec.mesh.zticks

    try:
        m = fieldvec.m
    except:
        m = 0

    plot_times = fieldvec.t

    t_maxint = 0

    if t_maxint < fieldvec.t[0]:
        t_maxint = fieldvec.t[0]
    elif t_maxint > fieldvec.t[-1]:
        t_maxint = fieldvec.t[-1]
    ref_frame_index = fieldvec.get_t_index(t_maxint)
    # frame on which the pulse is at the focal point
    ref_frame = fieldvec.get_component_frame(component, ref_frame_index)

    ref_frame = mirror_frame(ref_frame, m)

    # ~~ set up normalization
    fieldmin = np.amin(physutils.get_qty(ref_frame, qty))
    fieldmax = np.amax(physutils.get_qty(ref_frame, qty))
    if qty in {Norm.REAL, Norm.IMAG}:
        if abs(fieldmin) < abs(fieldmax):
            fieldmin = -fieldmax
        else:
            fieldmax = -fieldmin
    norm = get_norm(scale, fieldmin, fieldmax, logrange=logrange, symlog_linscale=0.03)

    # ~~ set up the figure and the elements using the (arbitrarily choosen) focal frame
    fig, ax = plt.subplots()
    # get the re/im/ampl/phase part asked for
    frame0 = physutils.get_qty(ref_frame, qty)
    pc = ax.pcolormesh(z, r, frame0, cmap=cmap, norm=norm, shading="nearest")
    txt = ax.text(
        0.1,
        0.1,
        "",
        ha="center",
        va="center",
        transform=ax.transAxes,
        fontdict={"color": "black", "size": 14},
    )
    cbar = fig.colorbar(pc, ax=ax, extend="max")

    fig.suptitle(title)
    ax.set_xlabel(r"$z (μ m)$")
    ax.set_ylabel(r"$r (μ m)$")
    cbar.set_label("Re(fieldvals)")

    # ~~ initialize the animation:
    def anim_init():
        pc.set_array([])
        txt.set_text("")
        return txt, pc

    # ~~ update the plot to the i-th frame:
    def animate(i):
        print(
            "Processing frame {} out of {}      ".format(i, fieldvec.nframes), end="\r"
        )
        frame = fieldvec.get_component_frame(component, i)  #!!!
        frame = physutils.get_qty(mirror_frame(frame, m), qty)
        values = frame  # [:, :-1]
        pc.set_array(values.flatten())
        txt.set_text("t={:.4f}fs".format(plot_times[i]))
        return pc, txt

    im_ani = animation.FuncAnimation(
        fig,
        animate,
        init_func=anim_init,
        frames=fieldvec.nframes,
        interval=1000 / fps,
        repeat=True,
        save_count=10,
        blit=True,
    )
    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        im_ani.save(savepath)
    else:
        plt.show()
    plt.close(fig)


def optax_frame(
    fieldvec: FieldVector.FieldVector,
    t: float,
    component: Comp,
    qty: Norm,
    scale="lin",
    cmap="gnuplot",
    title="",
    invisible=False,
    logrange=10,
    rlims=None,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot a frame from the the result of :class:`tfield_optaxplane`,  #TODO update docstring
    symmetrical around r=0.

    Arguments:
        z {float} z coordinate of the plane the field is calculated on.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        fieldvals {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        qty {str} -- indicates which quantity to plot:
                * 're': real part of the field
                * 'im': imaginary part of the field
                * 'ampl': amplitude of the field
                * 'phase': phase of the field
                (default: {'re'})
        cmap {str} -- Matplotlib color map used for the plot.  (default: {'jet'})
        scale {str} -- the kind of scale used for the color map:
                * 'lin': linear
                * 'log': logarithmic from zero (negative values are not plotted)
                * 'symlog': two-way logarithmic symmetrical to 0
                (default: {'lin'})
        title {str} -- Title of the resulting animated plot. (default: {''})
        savepath {path} -- path for the saved animation. If None, the animation
                won't be saved. (default: {None})
        invisible {bool} -- Whether to display the animation or not. (default: {False})
        logscal_range {float}: magnitude range of the logarithmic scale (if used)
    """
    rticks = fieldvec.mesh.rticks
    r = mirror_rticks(rticks)
    z = fieldvec.mesh.zticks
    i = fieldvec.get_t_index(t)

    try:
        m = fieldvec.m
    except:
        m = 0

    frame = fieldvec.get_component_frame(component, i)
    frame = mirror_frame(frame, m)

    plot_x = z
    plot_y = r

    # ~~ set up normalization
    fieldmin = np.amin(physutils.get_qty(frame, qty))
    fieldmax = np.amax(physutils.get_qty(frame, qty))
    if qty in {"re", "im"}:
        if abs(fieldmin) < abs(fieldmax):
            fieldmin = -fieldmax
        else:
            fieldmax = -fieldmin
    norm = get_norm(scale, fieldmin, fieldmax, logrange=logrange, symlog_linscale=0.03)

    # ~~ set up the figure and the elements
    fig, ax = plt.subplots()
    field = physutils.get_qty(frame, qty)
    pc = ax.pcolormesh(plot_x, plot_y, field, cmap=cmap, norm=norm)
    cbar = fig.colorbar(pc, ax=ax, extend="max")

    fig.suptitle(title)
    ax.set_xlabel(r"$r (\lambda_0)$")
    ax.set_ylabel(r"$z (\lambda_0)$")
    # ax.set_xlabel(r"$r (μ m)$")
    # ax.set_ylabel(r"$z (μ m)$")
    cbar.set_label("Re(fieldvals)")

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


def optax_line_anim(
    fieldvec: FieldVector.FieldVector,
    r: float,
    component: Comp,
    qty: Norm,
    title="",
    invisible=False,
    fps=20,
    logrange=10,
    rlims=None,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot the field in a line parallel to the optical axis on an animated plot

    Arguments:
        z {float} z coordinate of the plane the field is calculated on.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        fieldvals {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        qty {str} -- indicates which quantity to plot:
                * 're': real part of the field
                * 'im': imaginary part of the field
                * 'ampl': amplitude of the field
                * 'phase': phase of the field
                (default: {'re'})
        cmap {str} -- Matplotlib color map used for the plot.  (default: {'jet'})
        scale {str} -- the kind of scale used for the color map:
                * 'lin': linear
                * 'log': logarithmic from zero (negative values are not plotted)
                * 'symlog': two-way logarithmic symmetrical to 0
                (default: {'lin'})
        title {str} -- Title of the resulting animated plot. (default: {''})
        savepath {path} -- path for the saved animation. If None, the animation
                won't be saved. (default: {None})
        invisible {bool} -- Whether to display the animation or not. (default: {False})
        logscal_range {float}: magnitude range of the logarithmic scale (if used)
    """
    try:
        m = fieldvec.m
    except:
        m = 0

    # get the index corresponding to the r value
    r = np.round(r, decimals=10)
    r_index = np.searchsorted(fieldvec.mesh.rticks, r, side="right")
    if not (0 <= r_index < fieldvec.mesh.nr):
        raise ValueError("radius out of bounds")

    z = fieldvec.mesh.zticks

    plot_times = fieldvec.frametimes

    t_maxint = 0
    if t_maxint < fieldvec.frametimes[0]:
        t_maxint = fieldvec.frametimes[0]
    elif t_maxint > fieldvec.frametimes[-1]:
        t_maxint = fieldvec.frametimes[-1]
    ref_frame_index = fieldvec.get_t_index(t_maxint)
    # frame on which the pulse is at the focal point
    ref_frame = fieldvec.get_component_frame(component, ref_frame_index)

    ref_frame = mirror_frame(ref_frame, m)

    # ~~ set up normalization
    fieldmin = np.amin(physutils.get_qty(ref_frame, qty))
    fieldmax = np.amax(physutils.get_qty(ref_frame, qty))
    if qty in {"re", "im"}:
        if abs(fieldmin) < abs(fieldmax):
            fieldmin = -fieldmax
        else:
            fieldmax = -fieldmin

    # ~~ set up the figure and the elements using the (arbitrarily choosen) focal frame
    plot_z = z

    fig, ax = plt.subplots()
    # get the re/im/ampl/phase part asked for
    frame0 = physutils.get_qty(ref_frame, qty)
    line0 = frame0[r_index]
    (plot,) = ax.plot(plot_z, line0)
    txt = ax.text(
        0.1,
        0.1,
        "",
        ha="center",
        va="center",
        transform=ax.transAxes,
        fontdict={"color": "black", "size": 14},
    )

    fig.suptitle(title)
    ax.set_xlabel(r"$r (\lambda_0)$")
    # ax.set_xlabel(r"$r (μ m)$")
    # ax.set_ylabel(r"$z (μ m)$")

    # ~~ initialize the animation:
    def anim_init():
        plot.set_ydata([])
        txt.set_text("")
        return txt, plot

    # ~~ update the plot to the i-th frame:
    def animate(i):
        print(
            "Processing frame {} out of {}      ".format(i, fieldvec.nframes), end="\r",
        )
        frame = physutils.get_qty(fieldvec.get_component_frame(component, i), qty)  #!!!
        values = frame[r_index]
        plot.set_ydata(values)
        txt.set_text("t={:.4f}fs".format(plot_times[i]))
        return plot, txt

    im_ani = animation.FuncAnimation(
        fig,
        animate,
        init_func=anim_init,
        frames=fieldvec.nframes,
        interval=1000 / fps,
        repeat=True,
        save_count=10,
        blit=True,
    )

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        im_ani.save(savepath)
    else:
        plt.show()
    plt.close(fig)


def optax_line(
    fieldvec: FieldVector.FieldVector,
    t: float,
    r: float,
    component: Comp,
    qty: Norm,
    title="",
    logrange=10,
    rlims=None,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot a constant-r line of a frame of the result of :class:`tfield_optaxplane`,  #TODO update docstring
    symmetrical around r=0.

    Arguments:
        z {float} z coordinate of the plane the field is calculated on.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        fieldvals {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        qty {str} -- indicates which quantity to plot:
                * 're': real part of the field
                * 'im': imaginary part of the field
                * 'ampl': amplitude of the field
                * 'phase': phase of the field
                (default: {'re'})
        cmap {str} -- Matplotlib color map used for the plot.  (default: {'jet'})
        scale {str} -- the kind of scale used for the color map:
                * 'lin': linear
                * 'log': logarithmic from zero (negative values are not plotted)
                * 'symlog': two-way logarithmic symmetrical to 0
                (default: {'lin'})
        title {str} -- Title of the resulting animated plot. (default: {''})
        savepath {path} -- path for the saved animation. If None, the animation
                won't be saved. (default: {None})
        invisible {bool} -- Whether to display the animation or not. (default: {False})
        logscal_range {float}: magnitude range of the logarithmic scale (if used)
    """

    # get the index corresponding to the r value
    r = np.round(r, decimals=10)
    r_index = np.searchsorted(fieldvec.mesh.rticks, r, side="right")
    if not (0 <= r_index < fieldvec.mesh.nr):
        raise ValueError("radius out of bounds")

    z = fieldvec.mesh.zticks
    t_index = fieldvec.get_t_index(t)
    frame = fieldvec.get_component_frame(component, t_index)
    rline = frame[r_index]  # axes: r, z

    rline = physutils.get_qty(rline, qty)

    plot_z = z

    # ~~ set up the figure and the elements
    fig, ax = plt.subplots()
    pc = ax.plot(plot_z, rline)

    fig.suptitle(title)
    ax.set_xlabel(r"$z (\lambda_0)$")
    # ax.set_xlabel(r"$r (μ m)$")
    # ax.set_ylabel(r"$z (μ m)$")

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


def point_time(
    fieldvec: FieldVector.FieldVector,
    r: float,
    z: float,
    component: Comp,
    qty: Norm,
    title="",
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot a constant-r line of a frame of the result of :class:`tfield_optaxplane`,  #TODO update docstring
    symmetrical around r=0.

    Arguments:
        z {float} z coordinate of the plane the field is calculated on.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        fieldvals {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.
        qty {str} -- indicates which quantity to plot:
                * 're': real part of the field
                * 'im': imaginary part of the field
                * 'ampl': amplitude of the field
                * 'phase': phase of the field
                (default: {'re'})
        cmap {str} -- Matplotlib color map used for the plot.  (default: {'jet'})
        scale {str} -- the kind of scale used for the color map:
                * 'lin': linear
                * 'log': logarithmic from zero (negative values are not plotted)
                * 'symlog': two-way logarithmic symmetrical to 0
                (default: {'lin'})
        title {str} -- Title of the resulting animated plot. (default: {''})
        savepath {path} -- path for the saved animation. If None, the animation
                won't be saved. (default: {None})
    """

    # get the index corresponding to the r value
    r_index = fieldvec.get_r_index(r)
    z_index = fieldvec.get_z_index(z)

    t = fieldvec.t
    tline = np.empty_like(t, dtype="complex")
    for ti in range(fieldvec.nframes):
        tline[ti] = fieldvec.get_component_frame(component, ti)[r_index, z_index]

    tline = physutils.get_qty(tline, qty)

    # ~~ set up the figure and the elements
    fig, ax = plt.subplots()
    ax.plot(t, tline)

    fig.suptitle(title)
    ax.set_xlabel(r"$t (\nu_0^{-1})$")
    # ax.set_xlabel(r"$r (μ m)$")
    # ax.set_ylabel(r"$z (μ m)$")

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


# ~~ Plotters in the plane adjacent to the optical axis
################################################################################


def adj_anim(
    fieldvec: FieldVector.FieldVector,
    z: float,
    component: Comp,
    qty: Norm,
    thetanum=200,
    scale="lin",
    cmap="gnuplot",
    title="",
    invisible=False,
    fps=20,
    logrange=10,
    rlims=None,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot the result of :class:`tfield_optaxplane` on an animated colormap,
    symmetrical around r=0.

    Arguments:
        z {float} z coordinate of the plane the field is calculated on.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        fieldvals {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        qty {str} -- indicates which quantity to plot:
                * 're': real part of the field
                * 'im': imaginary part of the field
                * 'ampl': amplitude of the field
                * 'phase': phase of the field
                (default: {'re'})
        cmap {str} -- Matplotlib color map used for the plot.  (default: {'jet'})
        scale {str} -- the kind of scale used for the color map:
                * 'lin': linear
                * 'log': logarithmic from zero (negative values are not plotted)
                * 'symlog': two-way logarithmic symmetrical to 0
                (default: {'lin'})
        title {str} -- Title of the resulting animated plot. (default: {''})
        savepath {path} -- path for the saved animation. If None, the animation
                won't be saved. (default: {None})
        invisible {bool} -- Whether to display the animation or not. (default: {False})
        logscal_range {float}: magnitude range of the logarithmic scale (if used)
    """
    rticks = fieldvec.mesh.rticks
    zticks = fieldvec.mesh.zticks
    x, y = spin_rticks(rticks, thetanum)

    try:
        m = fieldvec.m
    except:
        m = 0

    plot_times = fieldvec.frametimes

    t_maxint = z / C_LIGHT  # time when z is at about the center of the pulse
    # if fieldvec.fieldparams.field_type == "pu_rif":
    #     t_maxint *= 1.41  # approx. adjust to the glass
    if t_maxint < fieldvec.frametimes[0]:
        t_maxint = fieldvec.frametimes[0]
    elif t_maxint > fieldvec.frametimes[-1]:
        t_maxint = fieldvec.frametimes[-1]
    ref_frame_index = fieldvec.get_t_index(t_maxint)
    # frame on which the pulse is at the focal point
    ref_frame = fieldvec.get_component_frame(component, ref_frame_index)
    ref_frame = spin_zline(ref_frame, z, thetanum, m, zticks=zticks, interpolate=True)

    # ~~ set up normalization
    fieldmin = np.amin(physutils.get_qty(ref_frame, qty))
    fieldmax = np.amax(physutils.get_qty(ref_frame, qty))
    if qty in {"re", "im"}:
        if abs(fieldmin) < abs(fieldmax):
            fieldmin = -fieldmax
        else:
            fieldmax = -fieldmin
    norm = get_norm(scale, fieldmin, fieldmax, logrange=logrange, symlog_linscale=0.03)

    # ~~ set up the figure and the elements using the (arbitrarily choosen) focal frame
    fig, ax = plt.subplots()
    # get the re/im/ampl/phase part asked for
    frame0 = physutils.get_qty(ref_frame, qty)
    pc = ax.pcolormesh(x, y, frame0, cmap=cmap, norm=norm)
    txt = ax.text(
        0.1,
        0.1,
        "",
        ha="center",
        va="center",
        transform=ax.transAxes,
        fontdict={"color": "black", "size": 14},
    )
    cbar = fig.colorbar(pc, ax=ax, extend="max")

    fig.suptitle(title)
    ax.set_xlabel(r"$r (μ m)$")
    ax.set_ylabel(r"$z (μ m)$")
    cbar.set_label("Re(fieldvals)")

    # ~~ initialize the animation:
    def anim_init():
        pc.set_array([])
        txt.set_text("")
        return txt, pc

    # ~~ update the plot to the i-th frame:
    def animate(i):
        print(
            "Processing frame {} out of {}      ".format(i, fieldvec.nframes), end="\r",
        )
        frame = fieldvec.get_component_frame(component, i)
        frame = spin_zline(frame, z, thetanum, m, interpolate=True, zticks=zticks)
        frame = physutils.get_qty(frame, qty)
        values = frame[:-1, :-1]
        pc.set_array(values.flatten())
        txt.set_text("t={:.4f}fs".format(plot_times[i]))
        return pc, txt

    im_ani = animation.FuncAnimation(
        fig,
        animate,
        init_func=anim_init,
        frames=fieldvec.nframes,
        interval=1000 / fps,
        repeat=True,
        save_count=10,
        blit=True,
    )

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        im_ani.save(savepath)
    else:
        plt.show()
    plt.close(fig)


def adj_frame(
    fieldvec: FieldVector.FieldVector,
    z: float,
    t: float,
    component: Comp,
    qty: Norm,
    thetanum=200,
    scale="lin",
    cmap="gnuplot",
    title="",
    logrange=10,
    rlims=None,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot the result of :class:`tfield_optaxplane` on a colormap, in a plane
        adjacent to the optical axis

    Arguments:
        z {float} z coordinate of the plane the field is calculated on.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        fieldvals {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        qty {str} -- indicates which quantity to plot:
                * 're': real part of the field
                * 'im': imaginary part of the field
                * 'ampl': amplitude of the field
                * 'phase': phase of the field
                (default: {'re'})
        cmap {str} -- Matplotlib color map used for the plot.  (default: {'jet'})
        scale {str} -- the kind of scale used for the color map:
                * 'lin': linear
                * 'log': logarithmic from zero (negative values are not plotted)
                * 'symlog': two-way logarithmic symmetrical to 0
                (default: {'lin'})
        title {str} -- Title of the resulting animated plot. (default: {''})
        logscal_range {float}: magnitude range of the logarithmic scale (if used)
    """
    rticks = fieldvec.mesh.rticks
    zticks = fieldvec.mesh.zticks
    x, y = spin_rticks(rticks, thetanum)
    # x /= fieldvec.fieldparams.lambda0
    # y /= fieldvec.fieldparams.lambda0

    try:
        m = fieldvec.m
    except:
        m = 0

    frameid = fieldvec.get_t_index(t)

    # frame on which the pulse is at the focal point
    frame = fieldvec.get_component_frame(component, frameid)
    frame = spin_zline(frame, z, thetanum, m, interpolate=True, zticks=zticks)

    fieldmin = np.amin(physutils.get_qty(frame, qty))
    fieldmax = np.amax(physutils.get_qty(frame, qty))

    if qty in {"re", "im"}:
        if abs(fieldmin) < abs(fieldmax):
            fieldmin = -fieldmax
        else:
            fieldmax = -fieldmin

    # choose the normalization type for the colormap according to `scale`
    norm = get_norm(scale, fieldmin, fieldmax, logrange=logrange, symlog_linscale=0.03)

    # set up the figure and the elements using the (arbitrarily choosen) focal frame
    fig, ax = plt.subplots()
    # get the re/im/ampl/phase part asked for
    frame0 = physutils.get_qty(frame, qty)
    pc = ax.pcolormesh(x, y, frame0, cmap=cmap, norm=norm)
    txt = ax.text(
        0.1,
        0.1,
        "",
        ha="center",
        va="center",
        transform=ax.transAxes,
        fontdict={"color": "black", "size": 14},
    )
    cbar = fig.colorbar(pc, ax=ax, extend="max")

    fig.suptitle(title)
    ax.set_xlabel(r"$r (\lambda_0)$")
    ax.set_ylabel(r"$z (\lambda_0)$")
    # ax.set_xlabel(r"$r (μ m)$")
    # ax.set_ylabel(r"$z (μ m)$")
    cbar.set_label("Re(fieldvals)")
    # ax.set_xlim(-2, 2)
    # ax.set_ylim(-2, 2)

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


def adj_line(
    fieldvec: FieldVector.FieldVector,
    z: float,
    t,
    component: Comp,
    qty: Norm,
    title="",
    rlims=None,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot the result of :class:`tfield_optaxplane` on a colormap, in a plane
        adjacent to the optical axis

    Arguments:
        z {float} z coordinate of the plane the field is calculated on.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        fieldvals {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Keyword Arguments:
        qty {str} -- indicates which quantity to plot:
                * 're': real part of the field
                * 'im': imaginary part of the field
                * 'ampl': amplitude of the field
                * 'phase': phase of the field
                (default: {'re'})
        cmap {str} -- Matplotlib color map used for the plot.  (default: {'jet'})
        scale {str} -- the kind of scale used for the color map:
                * 'lin': linear
                * 'log': logarithmic from zero (negative values are not plotted)
                * 'symlog': two-way logarithmic symmetrical to 0
                (default: {'lin'})
        title {str} -- Title of the resulting animated plot. (default: {''})
        logscal_range {float}: magnitude range of the logarithmic scale (if used)
    """

    rticks = fieldvec.mesh.rticks
    zticks = fieldvec.mesh.zticks

    t_index = fieldvec.get_t_index(t)
    z_index = np.searchsorted(zticks, z)

    # frame on which the pulse is at the focal point
    frame = fieldvec.get_component_frame(component, t_index)

    # mirror the line and the coordinatesaround the r=0 point
    line = frame[:, z_index]
    reflection = np.flip(line)
    line = np.concatenate((reflection[:-1], line))

    coord_reflection = np.flip(rticks)
    r = np.concatenate((-1 * coord_reflection[:-1], rticks))

    # set up the figure and the elements using the (arbitrarily choosen) focal frame
    fig, ax = plt.subplots()
    # get the re/im/ampl/phase part asked for
    plotline = physutils.get_qty(line, qty)
    pc = ax.plot(r, plotline)

    fig.suptitle(title)
    ax.set_xlabel(r"$r (\lambda_0)$")
    ax.set_ylabel(r"$z (\lambda_0)$")
    # ax.set_xlabel(r"$r (μ m)$")
    # ax.set_ylabel(r"$z (μ m)$")

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


def optax_snapshots(
    fieldvec: FieldVector.FieldVector,
    frametimes: List[float],
    component: Comp,
    qty="re",
    scale="lin",
    ampl_cmap="gnuplot",
    phase_cmap="jet",
    title="",
    invisible=False,
    logrange=10,
    zlims=None,
    omegalims=None,
    mask_phase=True,
    maskrange=1e5,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """
    Plot snapshots of the pulse next to each other, on the same scale.
    """

    axdist = 0.1
    mesh = fieldvec.mesh

    try:
        m = fieldvec.m
    except:
        m = 0

    mirror_ticks = -1 * mesh.rticks[-1::-1]
    plot_rpoints = np.concatenate((mirror_ticks, mesh.rticks), axis=0)
    plot_zpoints = mesh.zticks

    if zlims is None:
        zlims = [(mesh.zmin, mesh.zmax)] * len(frametimes)
    elif type(zlims[0]) is float:
        zlims = [zlims] * len(frametimes)
    elif len(zlims) != len(frametimes):
        raise ValueError("length of zlims does not correspond to the number of frames.")

    # set ratios of the subplots so that the x axis stays the same size even
    #   for different z ranges
    absolute_zmin, absolute_zmax = (zlims[0][0], zlims[-1][1])
    ratios = []
    for zlim in zlims:
        ratios.append(abs(zlim[1] - zlim[0]) / (absolute_zmax - absolute_zmin))

    # create the plots, next to each other, ampl in the upper and relative phase
    #   in the lower row
    fig, amaxes = plt.subplots(
        nrows=1,
        ncols=len(frametimes),
        sharey="row",
        gridspec_kw={"width_ratios": ratios},
    )
    #!!
    # fig, (amaxes, phaxes) = plt.subplots(nrows=2, ncols=len(frametimes),
    #    sharey="row", gridspec_kw = {'width_ratios': ratios})

    # for i, (amax, phax, frametime, zlim) in enumerate(zip(amaxes, phaxes, frametimes, zlims)):
    for i, (amax, frametime, zlim) in enumerate(zip(amaxes, frametimes, zlims)):
        # get the frame and it's min and max value
        frindex = fieldvec.get_t_index(frametime)
        frame = fieldvec.get_component_frame(component, frindex)
        # frame = elmfield.get_intensity_frame(frindex, "energy")
        # comoving = meshfield.get_comoving_frame(frame, frametime, fieldparams, mesh)
        # zoms, fft_frame = meshfield.get_z_fourier_frame(comoving, fieldparams, mesh)
        frame = physutils.get_qty(mirror_frame(frame, m), qty)
        # fft_frame = mirror_frame(fft_frame)
        fieldmin = np.amin(frame)
        fieldmax = np.amax(frame)
        frameampl = np.abs(frame)
        if qty in {"re", "im"}:
            if abs(fieldmin) < abs(fieldmax):
                fieldmin = -fieldmax
            else:
                fieldmax = -fieldmin

        # get the relative phase, mask where the field amplitude is smaller
        #   than fieldmax/maskrange
        mask = frameampl > fieldmax * maskrange
        # relphase = np.ma.array(np.abs(fft_frame), mask=mask)

        # Choose the normalization type according to `scale` (phase is always
        #   plotted on linear scale)
        # phasenorm = get_norm('lin', 0, 1)
        norm = get_norm(
            scale, fieldmin, fieldmax, logrange=logrange, symlog_linscale=0.03
        )

        # Create the amplitude plot
        amax.pcolormesh(plot_zpoints, plot_rpoints, frame, cmap=ampl_cmap, norm=norm)
        amax.set_xlim(zlim[0], zlim[1])
        amax.set_title("t = {:.1f}fs".format(frametime))
        if i < len(frametimes) - 1:
            amax.spines["right"].set_visible(False)
        if i != 0:
            amax.spines["left"].set_visible(False)
            amax.tick_params(left=False)
        # only label the rightmost plot's y ax
        if i == 0:
            amax.set_ylabel(r"$r (\mu m)$")
        # else:
        #     ax.set_yticks([])
        # label the x ax in the center
        if i == len(amaxes) // 2:
            amax.set_xlabel(r"$z (\mu m)$")

        # # Create the relative phase plot #!!!
        # phax.pcolormesh(zoms, plot_rpoints, relphase, cmap=phase_cmap,  norm=phasenorm)
        # if omegalims is not None:
        #     phax.set_xlim(omegalims[0], omegalims[1])
        # # only label the rightmost plot's y ax
        # if i == 0:
        #     phax.set_ylabel(r"$z (\mu m)$")
        # # else:
        # #     ax.set_yticks([])
        # # label the x ax in the center
        # if i == len(amaxes) // 2:
        #     phax.set_xlabel(r"$\omega_z (\omega_0 units)$")

        # get plot width

    # zrecbar = fig.colorbar(zre_plot, ax=zreax, extend='max')
    # zrecbar.set_label("Re(" + compstr[component] +") (arb. units)")

    # decorate

    # compstr = {'e_r': '$E_r$', 'e_phi': '$E_{phi}$', 'e_z': '$E_z$',
    #             'b_r': '$B_r$', 'b_phi': '$B_{phi}$', 'b_z': '$B_z$'}
    fig.suptitle(title)
    fig.set_size_inches(20, 11.25)
    fig.subplots_adjust(wspace=axdist)

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)


def make_all_plots(
    f: FieldVector.FieldVector,
    r: float,
    z: float,
    t: float,
    comp: Comp,
    val: Norm,
    savepath: str,
):
    # TODO fix this
    # r = 0.1
    # z = 0.1
    # t = 0.0
    val = Norm.AMPL
    # omega = params.omega0
    # vis.pulse_shape(f, "e_r", r, 0, z)
    # vis.plot_pu_integrand(r, z, omega, params, 'e_r')  #! broken
    total_energy(f, savepath=os.path.join(savepath, "total_energy"))
    centroid_speed(f, savepath=os.path.join(savepath, "centroid_speed"))
    # vis.phase_ampl_frame(f, i, comp, savepath=savepath)
    # vis.phase_ampl_anim(f, comp, savepath=savepath)
    optax_anim(f, comp, val, savepath=os.path.join(savepath, "optax_anim.mp4"))
    optax_frame(f, t, comp, val, savepath=os.path.join(savepath, "optax_frame"))
    optax_line_anim(
        f, r, comp, val, savepath=os.path.join(savepath, "optax_line_anim.mp4")
    )
    optax_line(f, t, r, comp, val, savepath=os.path.join(savepath, "optax_line"))
    adj_anim(z, f, comp, val, savepath=os.path.join(savepath, "adj_anim.mp4"))
    adj_frame(z, t, f, comp, val, savepath=os.path.join(savepath, "adj_frame"))
    adj_line(f, z, t, comp, val, savepath=os.path.join(savepath, "adj_line"))
    optax_snapshots(
        f, [-1, 0, 1], comp, val, savepath=os.path.join(savepath, "optax_snapshots")
    )


def time_eval_z_line(
    fieldvec: FieldVector.FieldVector,
    r: float,
    component: Comp,
    qty: Norm,
    scale="lin",
    cmap="gnuplot",
    title="",
    invisible=False,
    logrange=10,
    rlims=None,
    save: bool = False,
    savepath: Optional[str] = None,
):
    """Plot the result by dimesnions z, t """

    ir = fieldvec.get_r_index(r)
    t = fieldvec.t
    z = fieldvec.mesh.zticks

    try:
        m = fieldvec.m
    except:
        m = 0

    compvals = fieldvec.get_component(component)
    frame = compvals[:, ir, :]

    plot_x = z
    plot_y = t

    # ~~ set up normalization
    fieldmin = np.amin(physutils.get_qty(frame, qty))
    fieldmax = np.amax(physutils.get_qty(frame, qty))
    if qty in {"re", "im"}:
        if abs(fieldmin) < abs(fieldmax):
            fieldmin = -fieldmax
        else:
            fieldmax = -fieldmin
    norm = get_norm(scale, fieldmin, fieldmax, logrange=logrange, symlog_linscale=0.03)

    # ~~ set up the figure and the elements
    fig, ax = plt.subplots()
    field = physutils.get_qty(frame, qty)
    pc = ax.pcolormesh(plot_x, plot_y, field, cmap=cmap, norm=norm)
    cbar = fig.colorbar(pc, ax=ax, extend="max")

    fig.suptitle(title)
    ax.set_xlabel(r"$z (\lambda_0)$")
    ax.set_ylabel(r"$t (\nu_0^{-1})$")
    cbar.set_label("Re(fieldvals)")

    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()
    plt.close(fig)
