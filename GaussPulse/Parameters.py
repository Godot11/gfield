from abc import ABC
from copy import copy
from dataclasses import dataclass
from typing import Literal

import numpy as np

from . import C_LIGHT, LAMBDA_0, OMEGA_0, MonoField
from .Fields.VikartovskyVPField import c_Np
from .Fields.RWFields import C_RFLAG, C_PARAMS

RFLAGS_DICT = {
    "Pu": C_RFLAG.PU,
    "Pu_fixed": C_RFLAG.PU_FIX,
    "GenLG": C_RFLAG.LG,
    "flat": C_RFLAG.FLAT,
    "levy": C_RFLAG.LEVY,
    "ring": C_RFLAG.RING,
}


class Parameters(ABC):
    pass


@dataclass
class LevyParameters(Parameters):
    order: int
    theta: float
    theta_deg: float
    thetad: float
    n1: float
    isodiffracting: bool
    lambda0: float = 1.0
    omega0: float = 2 * np.pi * C_LIGHT

    def __init__(
        self, theta_deg, order=0, isodiffracting=False, n=1.0
    ):  # , lambda0=1, n=1.0):
        self.n1 = float(n)
        self.order = order
        self.theta_deg = theta_deg
        self.theta = np.deg2rad(self.theta_deg)
        self.thetad = np.tan(self.theta)
        self.isodiffracting = isodiffracting
        # self.lambda0 = lambda0
        self.omega0 = 2 * np.pi * C_LIGHT / LAMBDA_0


@dataclass
class RwParameters(Parameters):
    n1: float
    beta: float
    na: float
    alpha: float
    p: int
    m: int
    R_func_flag: int

    def __init__(self, beta, na, p=0, m=1, R_func="Pu_fixed", n=1.0):

        self.n1 = float(n)
        self.beta = beta
        self.na = na
        self.alpha = np.arcsin(
            self.na / self.n1
        )  #: the angle of view of the focusing lens from it's focal point
        self.theta = 1 / (self.beta * np.sqrt(self.n1 ** 2 / self.na ** 2 - 1))
        self.omega0 = OMEGA_0
        self.t_pulse = 1
        self.p = p
        self.m = m
        self.R_func_flag = RFLAGS_DICT[R_func]
        self.c_pms = C_PARAMS(
            self.n1, self.na, self.beta, self.p, self.m, self.R_func_flag, C_LIGHT
        )


@dataclass
class VikParameters(Parameters):
    m: int
    n: int
    pertOrder: int
    phi0: float
    s: float
    # k: float
    zr: float
    w0: float

    def __init__(
        self,
        s: float,
        w0: float,
        # theta_deg: float,
        m: int,
        n: int,
        pertOrder: int,
        phi0: float,
        # formula_type: Literal["analytical", "numerical", "spectral"],
    ):
        self.omega0 = OMEGA_0
        self.lambda0 = LAMBDA_0
        self.s = s
        self.t_pulse = np.sqrt(2 * s) / self.omega0

        # self.theta_deg = theta_deg
        # thetad = np.tan(np.deg2rad(self.theta_deg))
        self.w0 = w0

        self.m = m  # angular L-G index
        self.n = n  # radial L-G index.
        self.pertOrder = pertOrder  # = j_max in the BGV sum, not 2j

        self.phi0 = phi0  # initial phase

        self.k = 2.0 * np.pi / LAMBDA_0  # wave number
        self.zr = 0.5 * self.k * self.w0 ** 2  # Rayleigh range

        self.mmReal = float(self.m)
        self.nnReal = float(self.n)
        self.sReal = float(self.s)
        self.epsilonc2 = C_LIGHT / (2.0 * self.zr * self.omega0)  # \epsilon_c^2

        # lookup table for cAD values. Reference as #? cADs[N][p][j]
        lookup_shape = (
            self.pertOrder + 1,
            2 * self.pertOrder + 1,
            self.n + 2 * self.pertOrder + 1,
        )
        self.c_Np_lkp = np.full(lookup_shape, np.nan)
        for N in range(0, self.pertOrder + 1):
            for p in range(N, 2 * N + 1):
                for j in range(0, self.n + p + 1):
                    self.c_Np_lkp[N, p, j] = c_Np(
                        self.n, self.m, self.s, self.omega0, N, p, j
                    )

    def c_Np_from_lookup(self, N, p, j):
        return self.c_Np_lkp[N][p][j]
