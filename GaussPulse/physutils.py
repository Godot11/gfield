#!/usr/bin/env python
# -*- coding: utf-8 -*-

"Collection of miscellanious physics related functions."

from typing import List, Optional, Sequence, Tuple, TypeVar, Union, overload
from nptyping import NDArray

import numpy as np
import termcolor as tc
from scipy.optimize import fsolve

from . import (
    C_LIGHT,
    LAMBDA_0,
    NORMALIZED,
    ArrayLike,
    Norm,
    OMEGA_0,
    Vectorized,
    # Parameters,
)
from .Fields.LevyRadpolField import kh

USE_POISSON_ENVELOPE = True


def cgradient(f, *varargs, axis=-1, edge_order=1):
    """Derivate the complex data as (Re(f(x)) + Im(f(x)) using np.gradient.
    Axis must be given."""
    dre = np.gradient(np.real(f), *varargs, axis=axis, edge_order=edge_order)
    dim = np.gradient(np.imag(f), *varargs, axis=axis, edge_order=edge_order)
    # res = dabs * np.exp(1j * argf) +
    res = dre + 1j * dim
    return res


def cgradient_exp(f, *varargs, axis=-1, edge_order=1, discont=np.pi):
    """
    Alternative derivate calculation for complex data that avoids problems with
    rapid oscillation. Instead taking the gradient of (Re(f(x)) + Im(f(x)), this
    calculates it as (Abs(f)*exp(i*arg(f))'. Needs the phase to be accurately unwrappable.
    Axis must be given.

    Todo:
        - Implement checks on gradiend unwrapping.
    """
    absf = np.abs(f)
    argf = np.unwrap(np.angle(f), discont=discont, axis=axis)
    dabs = np.gradient(absf, *varargs, axis=axis, edge_order=edge_order)
    darg = np.gradient(argf, *varargs, axis=axis, edge_order=edge_order)
    # res = dabs * np.exp(1j * argf) +
    res = (dabs + 1j * absf * darg) * np.exp(1j * argf)
    return res


def get_qty(val: Vectorized, qty: Norm):
    """get the desired real quantity of the complex values

    Arguments:
        val {ndarray} -- the complex array
        qty {str} -- 're', 'im', 'ampl' or 'abs', and 'phase' or 'angle'; or 'compint' for component-vise intensity (|val|^2)

    Returns:
        ndarray: the quantity asked for in :param:`qty`
    """
    if qty is Norm.REAL:
        res = np.real(val)
    elif qty is Norm.IMAG:
        res = np.imag(val)
    elif qty is Norm.ABS:
        res = np.abs(val)
    elif qty is Norm.PHASE:
        res = np.angle(val)
    else:
        raise ValueError(f"invalid quantity: {qty} (should be in {list(Norm)})")
    return res


def cylind2cart(
    r: Vectorized, phi: Vectorized, z: Vectorized
) -> Tuple[Vectorized, Vectorized, Vectorized]:
    "Convert the cylindrical coordinates to cartesian."
    x = r * np.cos(phi)
    y = r * np.sin(phi)
    return (x, y, z)


def cart2cylind(
    x: Vectorized, y: Vectorized, z: Vectorized
) -> Tuple[Vectorized, Vectorized, Vectorized]:
    "Convert the cartesian coordinates to cylindrical."
    r = np.sqrt(x ** 2 + y ** 2)
    phi = np.arctan2(y, x)
    return (r, phi, z)


@overload
def normalize(
    lambda0: float,
    c: float,
    unit_type: str,
    vals: Vectorized,
    epsilon0: Optional[float],
) -> Vectorized:
    ...


@overload
def normalize(
    lambda0: float,
    c: float,
    unit_type: str,
    vals: Tuple[Vectorized],
    epsilon0: Optional[float],
) -> Tuple[Vectorized]:
    ...


def normalize(lambda0, c, unit_type, vals, epsilon0=None):
    """Normalize parameters for the unit system lambda0=1, c=1 used by the code.

    Values should be passed as array_like-s, with the unit type as keyword.
    Unit type of the input quantity can be:
    - "length": qty with unit L
    - "speed": qty with unit L/T
    - "time": qty with unit T
    - "freq": qty with unit 1/T
    - "elfield": qty with electric field unit (ML/T^3/I)
    - "bfield": qty with magnetic field unit (M/T^2/I)
    - "energy_density": qty with energy density unit (M/L/T^-2)
    - "energy": qty with energy unit (ML^2/T^2)
    - "epsilon_0: electric permeability (C^2T^4/M/L^3)

    The original length and time unit system is determined by the units of c
    and lambda0; other units are unaffected.

    Args:
        lambda0 (float): central wavelength in original unit system.
        c (float, optional): light speed in original unit system.
        unit_type(str): the dimension type of the unit
        vals: the value or list of values to convert (vectorized)

    Returns:
        tuple: the converted values with the same signature as they was supplied in **kwargs.

    Raises:
        TypeError: if key is not one of 'lengths', 'speeds', 'times', 'freqs' 'efields, 'bfields'.

    """

    # epsilon0 must be specified for energy type conversions
    if unit_type in ["energy_density", "energy", "poynting"] and epsilon0 is None:
        raise ValueError(
            "epsilon0 in the original unit system must be known "
            "to convert to/from reduced normalized energy-related quantities"
        )

    # normalization factor for speeds (like lambda0 for lengths)
    v0 = c / C_LIGHT
    l0 = lambda0 / LAMBDA_0

    # convert input to tuple if not iterable already
    try:
        iter(vals)
        isiter = True
    except TypeError:
        isiter = False
        vals = (vals,)

    # for key, val in kwargs.items():
    res = []
    for val in vals:
        if unit_type == "length":
            new = val / l0
        elif unit_type == "speed":
            new = val / v0
        elif unit_type == "time":
            new = val * v0 / l0
        elif unit_type == "freq":
            new = val * l0 / v0
        elif unit_type == "efield":
            new = val * l0 ** 2 / v0 ** 3
        elif unit_type == "bfield":
            new = val * l0 ** 2 / v0 ** 2
        elif unit_type == "energy_density":
            new = val * 2 / epsilon0 * l0 ** 4 / v0 ** 6
        elif unit_type == "energy":
            new = 2 / epsilon0 * l0 / v0 ** 6
        elif unit_type == "poynting":
            new = val * l0 ** 3 / v0 ** 3
        else:
            raise TypeError(f"'{unit_type}' is not implemented for normalization")
        res.append(new)

    if isiter:
        return tuple(res)  # len(vals) > 1
    else:
        assert len(res) == 1
        return res[0]


@overload
def denormalize(
    lambda0: float, c: float, unit_type: str, vals: Vectorized
) -> Vectorized:
    ...


@overload
def denormalize(
    lambda0: float, c: float, unit_type: str, vals: Tuple[Vectorized]
) -> Tuple[Vectorized]:
    ...


def denormalize(lambda0, c, unit_type, vals):
    """Denormalize parameters from the unit system lambda0=1, c=1 used by the code.

    Inverse of :meth:`normalize` with the same usage. The time and length units
    of the denormalized system are determined by the units of c and lambda0;
    other units are unaffected.

    Args:
        lambda0 (float): central wavelength in original unit system.
        c (float, optional): light speed in original unit system.
        **kwargs: unit=value or unit=(val1, val2, ...). Internally these will
            be converted to np.array.

    Returns:
        tuple: the converted values with the same signature as they was supplied in **kwargs.

    Raises:
        TypeError: if key is not one of 'lengths', 'speeds', 'times', 'freqs' 'efields, 'bfields'.

    """

    # normalization factors
    v0 = c / C_LIGHT
    l0 = lambda0 / LAMBDA_0

    istuple = isinstance(vals, tuple)

    res = []
    if not istuple:
        vals = (vals,)
    for val in vals:
        if unit_type == "length":
            new = val * l0
        elif unit_type == "speed":
            new = val * v0
        elif unit_type == "time":
            new = val * l0 / v0
        elif unit_type == "freq":
            new = val * v0 / l0
        elif unit_type == "efield":
            new = val * v0 ** 3 / (l0 ** 2)
        elif unit_type in "bfield":
            new = val * v0 ** 3 / (l0 ** 2)
        elif unit_type == "energy_density":
            new = val * v0 ** 2 / l0 ** 3
        elif unit_type == "epsilon_0":
            new = val * l0 / v0 ** 4
        elif unit_type == "energy":
            new = val * v0 ** 2
        elif unit_type == "reduced_edensity":  # W~ := W * 2 / epsilon0
            new = val * v0 ** 6 / l0 ** 4
        elif unit_type == "reduced_energy":  # E~ := E * 2 / epsilon0
            new = val * v0 ** 6 / l0
        elif unit_type == "poynting":
            new = val * v0 ** 3 / l0 ** 3
        else:
            raise TypeError(f"'{unit_type}' is not implemented for normalization")
        res.append(new)

    if istuple:
        ret = tuple(res)  # len(vals) > 1
    else:
        assert len(res) == 1
        ret = res[0]

    return ret


def t_to_poisson_s_limit(t_pulse: Vectorized) -> Vectorized:
    om0 = OMEGA_0
    s = (om0 * t_pulse) ** 2.0 * 0.5
    return s


def poisson_s_to_t_limit(s: Vectorized) -> Vectorized:
    om0 = OMEGA_0
    return np.sqrt(2 * s) / om0


def poisson_s_to_t_exact(s: Vectorized) -> Vectorized:
    om0 = OMEGA_0
    t_pulse = 2 * s / om0 * -np.sqrt(np.exp((s + 1) / 2) - 1)
    return t_pulse


def t_to_poisson_s_exact(t_pulse: float) -> float:
    """Get the Poisson s parameter yielding the given 1/e pulse length exactly,
    by numerically solving the implicit equation
    \frac{t^2 \omega ^2}{s^2}-e^{\frac{2}{s+1}}+1=0

    Args:
        t_pulse (float): the desired pulse duration

    Returns:
        float: the required s parameter
    """

    om0 = OMEGA_0
    te = 0.5 * t_pulse

    def eq(s):
        s = np.abs(s)
        return 1 + (om0 ** 2 * te ** 2 / s ** 2) - np.exp(2 / (s + 1))

    sol = fsolve(eq, x0=1.0, xtol=1.0e-8)
    return np.abs(float(sol))


def t_to_poisson_s(t):
    return t_to_poisson_s_exact(t)


def poisson_s_to_t(s):
    return poisson_s_to_t_exact(s)


# else:

#     def t_to_poisson_s(t_pulse: Vectorized, omega0: Vectorized) -> Vectorized:
#         """
#         Convert the t_pulse parameter of Gaussian spectrum to the s parameter of
#         the Poissonian spectrum according to the limit case s >> 1, where
#         t_pulse ~= sqrt(2s) / omega0.
#         """
#         return (omega0 * t_pulse) ** 2 * 0.5

#     def poisson_s_to_t(s: Vectorized, omega0: Vectorized) -> Vectorized:
#         """
#         Convert the s parameter of the Poissonian spectrum to the t_pulse parameter
#         of Gaussian spectrum according to the limit case s >> 1, where
#         t_pulse ~= sqrt(2s) / omega0.
#         """
#         return np.sqrt(2 * s) / omega0


# TODO: Update these


# def levy_from_pu(pu_pms):
#     theta = 1 / (pu_pms.beta * np.sqrt(pu_pms.n1 ** 2 / pu_pms.na ** 2 - 1))
#     return Parameters.LevyParameters(
#         pu_pms.lambda0, pu_pms.t_pulse, theta, pu_pms.n, pu_pms.phase_derivates,
#     )


def pu_pms_from_lens(beam_radius, lens_radius, focal_length, n1=1):
    theta = np.arctan(beam_radius / focal_length)
    beta = lens_radius / beam_radius
    na = n1 / np.sqrt(1 + 1 / (beta ** 2 * (np.tan(theta)) ** 2))
    return beta, na


# def pu_from_levy(levy_pms, beta=3.0):
#     na = levy_pms.n1 / np.sqrt(1 + 1 / (beta ** 2 * (np.tan(levy_pms.theta)) ** 2))
#     return Parameters.PuParameters(
#         lambda0=levy_pms.lambda0,
#         t_pulse=levy_pms.t_pulse,
#         beta=beta,
#         na=na,
#         n=levy_pms.n1,
#         phase_derivates=levy_pms.phase_derivates,
#     )


def pu_na_from_theta(theta_deg, beta=3.0, n=1.0):
    na = n / np.sqrt(1 + 1 / (beta ** 2 * (np.tan(np.deg2rad(theta_deg)) ** 2)))
    return na


# def levy_from_vik(vik_pms):

#     if vik_pms.n != vik_pms.m != 0:
#         warnings.warn(
#             "Vik.'s field correspond to Levy only if m=0 and n=0, but"
#             f"current has m={vik_pms.m} and n={vik_pms.n}"
#         )

#     theta_d = 2 / (kh(vik_pms.omega0) * vik_pms.w0)
#     theta = np.arctan(theta_d)
#     levy_pms = Parameters.LevyParameters(
#         divergency_angle=theta,
#         lambda0=vik_pms.lambda0,
#         t_pulse=vik_pms.t_pulse,
#         n=vik_pms.n1,
#         phase_derivates=vik_pms.phase_derivates,
#     )
#     return levy_pms


def w0_from_thetad(thetad: float) -> float:
    return 2 / (kh(OMEGA_0) * thetad)


# def vik_from_levy(levy_pms, pertOrder=0, phi0=0):
#     if not USE_POISSON_ENVELOPE:
#         s = t_to_poisson_s(levy_pms.t_pulse, levy_pms.omega0)
#     else:
#         s = levy_pms.s
#     w0 = 2 / (kh(levy_pms.omega0) * levy_pms.thetad)

#     return Parameters.VikPertParameters(
#         lambda0=levy_pms.lambda0,
#         s=s,
#         w0=w0,
#         m=0,
#         n=0,
#         pertOrder=pertOrder,
#         phi0=phi0,
#         phase_derivates=levy_pms.phase_derivates,
#     )
