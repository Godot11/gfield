""" Parse the input json file to get the correct field string and field vector. """

import argparse
import json
import os
import shutil
import sys
from operator import mul
from typing import Any, Dict, Optional, Union

import numpy as np
import termcolor as tc
from matplotlib import pyplot as plt
from scipy import constants as const

# from GaussPulse.VikartovskyVPField import er_v as vp_e_r
from GaussPulse import (
    Comp,
    FieldVector,
    MonoField,
    Norm,
    Parameters,
    meshfield,
    physutils,
    utils,
    vis,
)

SAVE = False
plot_spectral_shape = False  # TODO BROKEN

# denormalized unit system: um, fs
C_LIGHT_NNORM = const.c * const.femto / const.micro
EPSILON_0 = const.epsilon_0 * const.femto ** 4 / const.micro ** 3

# some flags
LEVY, PU, VIK, SPECT, TEMP = 1, 2, 3, 4, 5


class ConfigParser(object):
    def __init__(self, config_fpath: str) -> None:

        c: Dict[str, Any]
        try:
            with open(config_fpath) as f:
                c = json.load(f)
        except FileNotFoundError:
            try:
                c = json.loads(config_fpath)
            except json.JSONDecodeError:
                raise ValueError(
                    f"argument is not an existing file nor a valid JSON string: {config_fpath}"
                )
            config_fpath = None

        # ~~ read vals
        self.label = str(c["label"])

        self.field_type = str(c["field_type"])
        self.formula, pm_type, time_type = select_formula(self.field_type)

        self.multicore = bool(c["multicore"])
        self.tmin = float(c["tmin"])
        self.tmax = float(c["tmax"])
        self.nframes = int(c["nframes"])
        self.envelope_type = str(c["envelope_type"])
        self.t_pulse = float(c["t_pulse"])
        self.omega_log_range = float(c["omega_log_range"])
        self.phase_derivates = [float(i) for i in list(c["phase_derivates"])]

        self.normalized = bool(c["normalized"])
        self.lambda0 = float(c["lambda0"]) if not self.normalized else 1.0
        self.energy: Union[float, None] = float(c.get("pulse_energy", None))
        self.lightspeed: float = C_LIGHT_NNORM

        comps = c.get("components", None)
        if comps == "all":
            self.component_whitelist = [
                Comp.E_r,
                Comp.E_phi,
                Comp.E_z,
                Comp.B_r,
                Comp.B_phi,
                Comp.B_z,
            ]
        elif comps == "electric":
            self.component_whitelist = [Comp.E_r, Comp.E_phi, Comp.E_z]
        elif comps == "magnetic":
            self.component_whitelist = [Comp.B_r, Comp.B_phi, Comp.B_z]
        elif comps is None:
            self.component_whitelist = None
        else:
            try:
                self.component_whitelist = Comp(comps)
            except:
                self.component_whitelist = [
                    Comp(name) for name in list(c["components"])
                ]

        self.savepath = str(c["output_folder"])
        if not os.path.isabs(self.savepath):
            if config_fpath is not None:
                config_folder = os.path.dirname(os.path.abspath(config_fpath))
            else:
                config_folder = os.getcwd()
            self.savepath = os.path.join(config_folder, self.savepath)

        mesh_config = dict(c["mesh"])
        self.mesh = mesh_from_config_dict(
            mesh_config, self.normalized, self.lambda0, self.lightspeed
        )

        if not self.normalized:
            self.tmin, self.tmax, self.t_pulse = physutils.normalize(
                self.lambda0,
                self.lightspeed,
                unit_type="time",
                vals=(self.tmin, self.tmax, self.t_pulse),
            )
            self.energy = physutils.normalize(
                self.lambda0,
                self.lightspeed,
                unit_type="reduced_energy",
                vals=self.energy,
            )

        self.s = physutils.t_to_poisson_s_exact(self.t_pulse)

        # if self.envelope_type == "gauss":
        #     self.t_pulse_parameter = self.t_pulse
        # elif self.envelope_type == "poisson":
        #     # t_pulse_parameter1 = physutils.t_to_poisson_s_limit(t_pulse)
        #     self.t_pulse_parameter = physutils.t_to_poisson_s_exact(self.t_pulse)
        #     # print(t_pulse, t_pulse_parameter, t_pulse_parameter1)

        self.pms: Union[Parameters.Parameters, None] = None
        if pm_type is LEVY:
            pm_config = dict(c["levy_pms"])
            self.pms = levy_pms_from_config_dict(pm_config)
        elif pm_type is VIK:
            pm_config = dict(c["vik_pms"])
            self.pms = vik_pms_from_config_dict(pm_config, self.s)
        elif pm_type is PU:
            pm_config = dict(c["pu_pms"])
            self.pms = pu_pms_from_config_dict(pm_config)
        elif pm_type is None:
            self.pms = None
        else:
            raise ValueError

        # # ~~ construct fieldvector
        # if self.envelope_type == "poisson":
        #     self.tpulse = physutils.poisson_s_to_t(self.t_pulse_parameter)
        # else:
        #     self.tpulse = self.t_pulse_parameter

        if time_type is TEMP:
            self.field = FieldVector.FieldFromFormula(
                formula=self.formula,
                params=self.pms,
                mesh=self.mesh,
                nframes=self.nframes,
                tmin=self.tmin,
                tmax=self.tmax,
                t_pulse=self.t_pulse,
                allow_ondemand_calculation=False,
                component_whitelist=self.component_whitelist,
                label=self.label,
            )
        elif time_type is SPECT:
            self.field = FieldVector.FieldFromSpectralFormula(
                formula=self.formula,
                params=self.pms,
                mesh=self.mesh,
                nframes=self.nframes,
                tmin=self.tmin,
                tmax=self.tmax,
                t_pulse=self.t_pulse,
                envelope_type=self.envelope_type,
                phase_derivates=self.phase_derivates,
                omega_log_range=self.omega_log_range,
                allow_ondemand_calculation=False,
                component_whitelist=self.component_whitelist,
                label=self.label,
            )
        else:
            raise ValueError("oops, this should never happen")


def select_formula(field_type):
    field_dict = {
        # name: (formula, param, type)
        "levy": (MonoField.levy, LEVY, SPECT),
        # "pu": (MonoField.pu, PU, SPECT),
        "rw": (MonoField.pu_rw, PU, SPECT),
        "vik_sp": (MonoField.vikartovsky_spectral, VIK, SPECT),
        "vik_num": (MonoField.vikartovsky_time_numerical, VIK, TEMP),
        "vik_an": (MonoField.vikartovsky_time, VIK, TEMP),
        "planewave": (MonoField.planewave, None, SPECT),
        "gaussian": (MonoField.gaussian, LEVY, SPECT),
        "gaussian_time": (MonoField.time_gaussian, LEVY, TEMP),
    }
    try:
        return field_dict[field_type]
    except KeyError:
        raise ValueError(
            f"invalid field type: {field_type}. Valid names are: {[k for k in field_dict.keys()]}"
        )


def mesh_from_config_dict(
    config_dict: Dict[str, Any], normalized=False, lambda0=None, lightspeed=None
) -> meshfield.RectMesh:
    rmin = float(config_dict["rmin"])
    rmax = float(config_dict["rmax"])
    zmin = float(config_dict["zmin"])
    zmax = float(config_dict["zmax"])
    nr = int(config_dict["nr"])
    nz = int(config_dict["nz"])
    if not normalized:
        assert lambda0 is not None and lightspeed is not None
        rmin, rmax, zmin, zmax = physutils.normalize(
            lambda0, lightspeed, unit_type="length", vals=(rmin, rmax, zmin, zmax)
        )
    return meshfield.RectMesh(rmax, zmin, zmax, nr, nz, rmin=rmin)


def levy_pms_from_config_dict(config_dict: Dict[str, Any]) -> Parameters.LevyParameters:
    l = config_dict
    levy_theta_deg = float(l["theta_deg"])
    levy_order = int(l["order"])
    levy_isodiffracting = bool(l["isodiffracting"])
    return Parameters.LevyParameters(
        theta_deg=levy_theta_deg, order=levy_order, isodiffracting=levy_isodiffracting,
    )


def vik_pms_from_config_dict(
    config_dict: Dict[str, Any], s: float
) -> Parameters.VikParameters:
    v = config_dict
    vik_theta_deg = float(v["theta_deg"])
    vik_m = int(v["m"])
    vik_n = int(v["n"])
    vik_pertorder = int(v["pertorder"])
    vik_phi0 = float(v["phi0"])
    vik_w0 = physutils.w0_from_thetad(np.tan(np.deg2rad(vik_theta_deg)))
    return Parameters.VikParameters(
        s=s,
        w0=vik_w0,
        m=vik_m,
        n=vik_n,
        pertOrder=vik_pertorder,
        phi0=vik_phi0,  # 3.0 * const.pi / 2.0,
    )


def pu_pms_from_config_dict(config_dict: Dict[str, Any]) -> Parameters.RwParameters:
    p = config_dict
    pu_beta = p["beta"]
    pu_theta_deg = p["theta_deg"]
    pu_p = p.get("p", 0)
    pu_m = p.get("m", 1)
    pu_R_func = p.get("R_func", "Pu_fixed")
    pu_na = physutils.pu_na_from_theta(pu_theta_deg)
    return Parameters.RwParameters(
        beta=pu_beta, na=pu_na, p=pu_p, m=pu_m, R_func=pu_R_func
    )


def load_from_config(fpath):

    cf = ConfigParser(fpath)
    return cf.f, cf.normalized, cf.lambda0, cf.energy, cf.savepath, cf.multicore


def path_nonempty(dir_path):
    try:
        exists = 0 < len(os.listdir(dir_path))
    except NotADirectoryError:
        exists = os.path.isfile(dir_path)
    except FileNotFoundError:
        exists = False
    return exists


def output_path_exists_prompt(dir_path):
    """Check if a file or a non-empty folder like dir_path exists, and prompt
        with confirmation whether to remove it. If file exists and the user asked
        not to remove it, a FileExistsError is raised."""

    if path_nonempty(dir_path):
        q1 = (
            f'Output destination "{dir_path}" already exists; continuing will'
            " remove it alltogether. Continue?"
        )
        if not utils.query_yes_no(q1, default="no"):
            raise FileExistsError

        q2 = f"Warning: all data in {dir_path} will be deleted. Are you sure?"
        if not utils.query_yes_no(q2, default="no"):
            raise FileExistsError

        # didn't exit, so continue by removing the directory
        print(f'removing "{dir_path}"...', end="")
        if os.path.isdir(dir_path):
            shutil.rmtree(dir_path)
        else:
            os.remove(dir_path)
        print(" done")


# todo shorter name lol
def load_f_from_config_savepath_with_fail_prompt(cfile):

    config = ConfigParser(cfile)
    savepath = config.savepath

    if not os.path.isdir(savepath):
        q = f"Calculated data not found at {savepath}. Do you wish to calculate the field values?"
        if not utils.query_yes_no(q, default="yes"):
            print(f"Skipping {cfile}")
            raise FileNotFoundError(
                f"{cfile} has no associated data, and user refused to calculate it"
            )
        else:
            f = config.field
            f.calculate_field(
                savepath,
                show_progress=True,
                show_stats=True,
                multicore=config.multicore,
            )
            q = f"Do you wish to save the field values?"
            if utils.query_yes_no(q, default="no"):
                f.save(savepath)
    else:
        f = FieldVector.FieldFromSaved(savepath)

    if not config.normalized:
        f = f.denormalize(config.lambda0, C_LIGHT_NNORM)

    return f
