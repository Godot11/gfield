#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Process the data computed by tempfield.py: read it, display it, and/or
calculate the total energy of the pulse"""
from __future__ import annotations

import inspect
import importlib.util
import json
import os
import sys
import warnings
from dataclasses import make_dataclass
from types import ModuleType
from typing import Any, Dict, Tuple, Union, cast

import numpy as np
from progressbar import progressbar as prbar
from pyevtk import hl as vtk_writer
from scipy import constants as const
from scipy import integrate as intg

from . import Comp, FieldFFT, FieldVector, MonoField, meshfield, utils
from .Fields import LevyRadpolField, RWFields, VikartovskyVPField

PARAMETERS_FNAME = "parameters.json"
VALS_FOLDNAME = "fieldvals/"


def import_module_by_path(path: str) -> ModuleType:
    "based on code from psutils; import file from path"
    name = os.path.splitext(os.path.basename(path))[0]
    spec = importlib.util.spec_from_file_location(name, path)
    mod = importlib.util.module_from_spec(spec)
    assert isinstance(spec.loader, importlib.abc.Loader)
    spec.loader.exec_module(mod)
    return mod


def config_to_field_custom_module(
    config_path: str,
) -> Tuple[
    Union[FieldVector.FieldFromFormula, FieldVector.FieldFromSpectralFormula], str
]:
    """Load a custom module given in the config path, and use it to construct a FieldVector.

    Args:
        config_path (str): path to the custom field module (TODO: should this be
        the module itself? This way it can't be multithreaded.)

    Returns:
        Tuple: the field and the output folder
    """
    with open(config_path) as f:
        jdict: Dict[str, Any] = json.load(f)

    # load the supplied field module
    field_mod_path = str(jdict["field"])
    # field_module = cast(Any, import_module_by_path(field_mod_path))
    field_type = str(jdict["field_type"])
    field_module = cast(Any, import_module_by_path(field_mod_path))

    # load mesh
    meshdict = dict(jdict["mesh"])
    try:
        mesh = meshfield.RectMesh(**meshdict)
    except TypeError:
        raise TypeError("Invalid mesh attributes in config file")

    # load everything else needed
    tnum = int(jdict["tnum"])
    tmin = float(jdict["tmin"])
    tmax = float(jdict["tmax"])

    # find nonzero components
    if "components" in jdict.keys():
        nonzero_comps = list(jdict["components"])
    elif hasattr(field_module, "NONZERO_COMPS"):
        nonzero_comps = field_module.NONZERO_COMPS
    else:
        comps = ["e_r", "e_phi", "e_z", "b_r", "b_phi", "b_z"]
        nonzero_comps = []
        for comp in comps:
            if hasattr(field_module, comp):
                nonzero_comps.append(comp)

    # check if the nonzero components have the corresponding functions, and generate a comp: func dictionary
    comps_funcs = {}
    for comp in nonzero_comps:
        if not hasattr(field_module, comp):
            raise AttributeError(
                f"No function for {comp} in the supplied field module, "
                + "but it is listed as a nonzero component"
            )
        comps_funcs[comp] = getattr(field_module, comp)

    # load parameters to a Parameters (data)class if given
    if "params" in jdict.keys():
        paramsdict = dict(jdict["params"])
        try:
            params = field_module.Parameters(**paramsdict)
        except AttributeError:
            # the module has no Parameters class defined
            warnings.warn(
                "The supplied field module has no Parameters class "
                + "defined; using supplied dict as dataclass instead"
            )
            utils.dict_to_dataclass(paramsdict)
            Pms = make_dataclass("Parameters", list(paramsdict.keys()))
            params = Pms(**paramsdict)
        except TypeError:
            # the supplied dict doesn't match the signature of field_module.Parameters
            raise TypeError(
                "The parameters given in config file does not match the signature "
                + "of the Parameters class defined in the supplied field module."
            )
    else:
        params = None

    try:
        is_time_domain = field_module.IS_TIME_DOMAIN
    except AttributeError:
        is_time_domain = False

    if is_time_domain:
        t_pulse = float(jdict["t_pulse_parameter"])
        tformula = MonoField.TimeFormula(**comps_funcs)
        field = FieldVector.FieldFromFormula(
            tformula, params, mesh, tnum, tmin, tmax, t_pulse
        )

    else:
        envelope_type = jdict["envelope_type"]
        env_t_param = jdict["t_pulse_parameter"]
        if "phase_derivates" in jdict.keys():
            phase_derivates = jdict["phase_derivates"]
        else:
            phase_derivates = []
        if "omega_log_range" in jdict.keys():
            omrange = jdict["omega_log_range"]
        else:
            omrange = 10
        sformula = MonoField.SpectralFormula(**comps_funcs)
        field = FieldVector.FieldFromSpectralFormula(
            sformula,
            params,
            mesh,
            tnum,
            tmin,
            tmax,
            env_t_param,
            envelope_type,
            phase_derivates,
            omrange,
        )

    output_folder = jdict["output_folder"]

    return field, output_folder


def fieldvec_pms_to_json_dict(fieldvec: FieldVector.FieldVector) -> dict:
    mesh_json = utils.dataclass_to_json_dict(fieldvec.mesh)
    savedict = {
        "label": fieldvec.label,
        "tmin": fieldvec.tmin,
        "tmax": fieldvec.tmax,
        "nframes": fieldvec.nframes,
        "dt": fieldvec.dt,
        # "phase_derivates": fieldvec.phase_derivates,
        "zero_comps": fieldvec.zero_comps,
        "nonzero_comps": fieldvec.nonzero_comps,
        "mesh": mesh_json,
        "m": fieldvec.m,
    }
    if isinstance(fieldvec, FieldVector.FieldFromFormula):
        savedict["params"] = utils.dataclass_to_json_dict(fieldvec.params)
        savedict["t_pulse"] = fieldvec.t_pulse
    if isinstance(fieldvec, FieldVector.FieldFromSpectralFormula):
        fftpms_json = utils.dataclass_to_json_dict(fieldvec.fft_params)
        savedict["omega_log_range"] = fieldvec.omega_log_range
        savedict["fft_pms"] = fftpms_json
        savedict["phase_derivates"] = fieldvec.phase_derivates

    jsondict = {key: utils.to_serializable(item) for key, item in savedict.items()}
    return jsondict


def save_parameters_as_json(fieldvec: FieldVector.FieldVector, fpath: str) -> None:
    jsondict = fieldvec_pms_to_json_dict(fieldvec)
    with open(fpath, "w+") as f:
        json.dump(jsondict, f, sort_keys=False, indent=4)


def save_coordinates_as_json(fieldvec: FieldVector.FieldVector, fpath: str) -> None:
    jsondict = {"r": list(fieldvec.r), "z": list(fieldvec.z), "t": list(fieldvec.t)}
    with open(fpath, "w+") as f:
        json.dump(jsondict, f, sort_keys=False, indent=4)


def save_component_vals(
    fieldvec: FieldVector.FieldVector, component: Comp, comp_vals_folder: str
) -> None:
    """
    Save the field values to distinct files per frame, in the folder given
    by :param:`destination`

    Arguments:
        dest_folder {str} -- the folder where the files will be saved
        fieldvals {ndarray} -- the (time, r, z) field  values to be written out
                (result of :meth:`FieldFFT.tfield_optaxplane`)
    """
    if component in fieldvec.zero_comps:
        warnings.warn(
            "saving of zero component"
        )  # TODO: should we enable saving anyway?

    if not os.path.isdir(comp_vals_folder):
        os.makedirs(comp_vals_folder)
    for i in range(fieldvec.nframes):
        fname = f"{i:05d}.npy"
        compvals = fieldvec.get_component_frame(component, i)
        np.save(os.path.join(comp_vals_folder, fname), compvals)


def save_field(fieldvec: FieldVector.FieldVector, folder: str) -> None:
    if not os.path.isdir(folder):
        os.makedirs(folder)
    params_path = os.path.join(folder, PARAMETERS_FNAME)
    coords_path = os.path.join(folder, "coords.json")

    save_parameters_as_json(fieldvec, params_path)
    save_coordinates_as_json(fieldvec, coords_path)
    for comp in fieldvec.nonzero_comps:
        savefolder = os.path.join(folder, comp.value)
        fieldvec.save_component_vals(comp, savefolder)


# ~~ exports


def export_component_as_txt(
    fieldvec: FieldVector.FieldVector,
    comp: Comp,
    comp_folder: str,
    fmt="%.18e",
    delimiter=" ",
) -> None:

    if comp in fieldvec.zero_comps:
        warnings.warn(f"exporting always zero component {comp} ")

    if not os.path.isdir(comp_folder):
        os.makedirs(comp_folder)

    params_path = os.path.join(comp_folder, PARAMETERS_FNAME)
    save_parameters_as_json(fieldvec, params_path)

    # save coordinates
    np.savetxt(
        os.path.join(comp_folder, "t.txt"), fieldvec.frametimes, fmt, delimiter,
    )
    np.savetxt(os.path.join(comp_folder, "r.txt"), fieldvec.mesh.rticks, fmt, delimiter)
    np.savetxt(os.path.join(comp_folder, "z.txt"), fieldvec.mesh.zticks, fmt, delimiter)

    # save field values
    compvals = fieldvec.get_component(comp)
    print("\nExporting result to txt...")
    for i in prbar(range(fieldvec.nframes)):
        re_fname = f"real_{i:05d}.txt"
        im_fname = f"imag_{i:05d}.txt"
        compvals = fieldvec.get_component_frame(comp, i)
        np.savetxt(os.path.join(comp_folder, re_fname), compvals.real, fmt, delimiter)
        np.savetxt(os.path.join(comp_folder, im_fname), compvals.imag, fmt, delimiter)


# TODO: fix this one


def grid2dToVTK(
    path: str, x: np.ndarray, y: np.ndarray, pointData: np.ndarray, z: float = 0
) -> None:
    """Save the supplied 2D or 3D mesh as a VTK file. Inputs correspond to the ones of
    vtk_wtiter.gridToVTK, but work with 2D data. Only point data is accepted. It
    also converts the data arrays to be c-contigous.

    Arguments:
        fname {path} -- name of the resulting file, without the extension
        x {ndarray} -- array of x ticks
        y {ndarray} -- array of y ticks
        pointData {dict} -- dict of 2D data arrays
        z {float} -- z value of the data (defaults to 0)
    """
    z_arr = np.atleast_1d(z)
    for key in pointData.keys():
        pointData[key] = np.ascontiguousarray(np.atleast_3d(pointData[key]))
    vtk_writer.gridToVTK(path, x, y, z_arr, pointData=pointData)


def export_component_as_vtk(
    fieldvec: FieldVector.FieldVector, component: Comp, dest_folder: str, value="field",
) -> None:
    """
    Export the component as a bunch of VTK files to  (one per frame), containing
    the real and imaginary values. Note: these can't (yet?) be opened by this
    program later. Results are saved in :param:`dest_folder`.

    Arguments:
        value {str}: 'field', 'cm_field', 'fourier': which field representation should be exported
    """

    if component in fieldvec.zero_comps:
        warnings.warn(f"exporting always zero component {component} ")

    # create folders if neccessary
    if not os.path.isdir(dest_folder):
        os.makedirs(dest_folder)

    if value not in {"field", "comoving_field", "fourier"}:
        raise ValueError("invalid value identifier")

    x = fieldvec.mesh.rticks
    z = 0
    if value in {"field", "comoving_field"}:
        y = fieldvec.mesh.zticks
    else:  # value == "fourier"
        # do a z-fft calculation to get the z-freq values
        testcomp = fieldvec.nonzero_comps[0]
        fr = fieldvec.get_comoving_component_frame(testcomp, 0)
        y, _ = meshfield.get_z_fourier_frame(fr, fieldvec.mesh)

    print("\nExporting VTK file...")
    for i in prbar(range(fieldvec.nframes)):
        if value == "field":
            data = fieldvec.get_component_frame(component, i)
        elif value == "comoving_field":
            data = fieldvec.get_comoving_component_frame(component, i)
        elif value == "fourier":
            data = fieldvec.get_component_z_fourier_frame(component, i)
        else:
            raise ValueError(f"invalid value: {value}")

        fname = os.path.join(dest_folder, f"{value}_{i:05d}")
        data = {
            (component.value + "_real"): data.real,
            (component.value + "_imag"): data.imag,
        }
        grid2dToVTK(fname, x, y, pointData=data, z=0)


def export_as_vtk(
    fieldvec: FieldVector.FieldVector, dest_folder: str, value="field"
) -> None:
    """
    Save the result as a bunch of VTK files (one per frame), containing all
    nonzero field components. Note: these can't (yet?) be opened by this
    program later. Results are saved in the object's data folder in a new
    folder called "vtk".

    Arguments:
        value {str}: 'field', 'cm_field', 'fourier': which field representation should be exported
    """
    if not os.path.isdir(dest_folder):
        os.makedirs(dest_folder)

    if value not in {"field", "comoving_field", "fourier"}:
        raise ValueError("invalid value identifier")

    x = fieldvec.mesh.rticks
    z = 0
    if value in {"field", "comoving_field"}:
        y = fieldvec.mesh.zticks
    else:  # value == "fourier"
        # do a z-fft calculation to get the z-freq values
        testcomp = fieldvec.nonzero_comps[0]
        fr = fieldvec.get_comoving_component_frame(testcomp, 0)
        y, _ = meshfield.get_z_fourier_frame(fr, fieldvec.mesh)

    print("\nExporting VTK file...")
    fname = ""
    for i in prbar(range(fieldvec.nframes)):
        datadict = {}
        for comp in fieldvec.nonzero_comps:
            if value == "field":
                data = fieldvec.get_component_frame(comp, i)
            elif value == "comoving_field":
                data = fieldvec.get_comoving_component_frame(comp, i)
            elif value == "fourier":
                data = fieldvec.get_component_z_fourier_frame(comp, i)
            else:
                raise ValueError(f"invalid value: {value}")

            fname = os.path.join(dest_folder, f"{value}_{i:05d}")
            datadict[comp.value + "_real"] = data.real
            datadict[comp.value + "_imag"] = data.imag

        grid2dToVTK(fname, x, y, pointData=datadict, z=0)


def export_as_3d_vtk(
    fieldvec: FieldVector.FieldVector,
    dest_folder: str,
    value="field",
    thetanum: int = 100,
) -> None:
    """
        Save the result as a bunch of VTK files (one per frame), containing
        all nonzero fueld components. Note: these can't (yet?) be opened by
        this program later. Results are saved in the object's data folder,
        in a new folder called "vtk".

        Arguments:
            value {str}: 'field' or 'cm_field': which field representation
                    should be exported
        """
    # raise NotImplementedError("fix this first")  # TODO

    # create folders if neccessary
    if not os.path.isdir(dest_folder):
        os.makedirs(dest_folder)

    if value not in {"field", "cm_field"}:  # TODO implement Fourier?
        raise ValueError("invalid value identifier")

    # test frame, to get x,y,z values
    try:
        m = fieldvec.m
    except AttributeError:
        m = 0

    # fr = fieldvec.get_comoving_component_frame("e_r", 0)
    # x, y, z, _ = meshfield.get_3d_frame(fr, fieldvec.mesh, thetanum, m)

    print("\nExporting VTK file...")
    for i in prbar(range(fieldvec.nframes)):
        # if value == "field":
        #     (er, _, ez), (_, bp, _) = fieldvec.get_frame(i)
        fname = os.path.join(dest_folder, "field_{:05d}".format(i))
        # else:  # value == "cm_field":
        #     (er, _, ez), (_, bp, _) = fieldvec.get_comoving_frame(i)
        #     fname = os.path.join(dest_folder, "comoving_{:05d}".format(i))

        datadict = {}
        for comp in fieldvec.nonzero_comps:
            frame = fieldvec.get_component_frame(comp, i)
            x, y, z, data = meshfield.get_3d_frame(frame, fieldvec.mesh, thetanum, m)
            fname = os.path.join(dest_folder, f"{value}_{i:05d}")
            datadict[comp.value + "_real"] = np.ascontiguousarray(data.real)
            datadict[comp.value + "_imag"] = np.ascontiguousarray(data.imag)

        vtk_writer.gridToVTK(fname, x, y, z, pointData=datadict)
