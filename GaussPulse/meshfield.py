#!/opt/anaconda3/bin/python
# -*- coding: utf-8 -*-

"""
This module contains methods to calculate, the field on a given rectangular mesh.
The parameters of this mesh are stored in a :class:`MeshParameters` object.

Methods:
    * calculating:
        * tfield_optaxplane -- get the time-dependent field component values in a
                plane containing the r=0 optical axis. Returns a 3D array of
                [frame, r, z] and the frame times.
        * get_comoving_mesh -- transform the result of tfield_optaxplane to a frame
                of view moving with the pulse.
        * get_comoving_frame -- transform a single frame to the co-moving frame of view.
    * visualization:
        * plot_phiplane_tdep_field -- plot (and possibly save) the time-dependent
                field on an animated Matplotlib plot.
        * plot_pulse -- plot the amplitude and the phase of the time-dependent
                field in a given point, both in the spectral domain, and in the
                time domain derived from that.
        * plot_static_z_plane -- plot a static, monochromatic component of the
                field in a plane adjacent to the optical axis
        * plot_static_optax_plane --  plot a static, monochromatic component of
                the field in a plane containing the optical axis

"""


from dataclasses import dataclass
from dataclasses import field as dcfield
from typing import Any, Optional, Type, TypeVar, Union, cast

import numpy as np
from nptyping import NDArray
from scipy import constants as const
from scipy import interpolate
from scipy.fftpack import fft, fftfreq

# Speed of light in nm/fs
from . import C_LIGHT, utils

# typing stuff


@dataclass
class RectMesh(object):
    rmin: float
    rmax: float
    zmin: float
    zmax: float
    nr: int
    nz: int
    dr: float
    dz: float

    def __init__(
        self, rmax: float, zmin: float, zmax: float, nr: int, nz: int, rmin: float = 0,
    ) -> None:
        """Parameters determining the mesh.

        Arguments:
            rmax {double} -- maximal r of the mesh the field is calculated on. The
                radius will span from 0 to :param:`rbound`. (um)
            zmin {tuple} -- lower z bounds of the mesh the field is calculated on. (um)
            zmax {tuple} -- upper z bounds of the mesh the field is calculated on. (um)
            nr {int} -- number of r ticks on the mesh
            nz {int} -- number of z ticks on the mesh. The mesh will have
                nr * nz points.
   """
        # todo: is this a normal way?
        self.rticks: NDArray = cast(Any, None)
        self.zticks: NDArray = cast(Any, None)
        self.rmesh: NDArray = cast(Any, None)
        self.zmesh: NDArray = cast(Any, None)

        self.rmin = rmin
        self.rmax = rmax
        self.zmin = zmin
        self.zmax = zmax
        self.nr = int(nr)
        self.nz = int(nz)
        self.rticks, self.dr = np.linspace(
            0, self.rmax, self.nr, retstep=True, endpoint=True
        )
        self.zticks, self.dz = np.linspace(
            self.zmin, self.zmax, self.nz, retstep=True, endpoint=True
        )
        self.rmesh, self.zmesh = utils.meshgrid(self.rticks, self.zticks)

    def resample(self, new_nr: int, new_nz: int) -> "RectMesh":
        """Increase/decrease the mesh resolution while leaving bounds untouched.

        Args:
            new_nr (int): number of r points
            new_nz (int): number of z points

        Returns:
            RectMesh: the refined mesh with unchanged endpoints
        """
        return RectMesh(self.rmax, self.zmin, self.zmax, new_nr, new_nz, self.rmin)

    def get_r_index(self, r: float) -> int:
        """Get the index of the r mesh coordinate that is closest to `r`.

        Args:
            r (float): the desired r coordinate

        Raises:
            ValueError: if r < `self.mesh.rmin` or r > `self.mesh.rmax`

        Returns:
            int: index of self.mesh.rticks where the value is closest to r
        """
        # t = np.round(femtosec_time, decimals=10)
        index = np.searchsorted(self.rticks, r)  # "rounds" up
        if index < 0 or index > self.nr:
            raise ValueError(
                "r {} is outside of z bounds ({}-{})".format(r, self.rmin, self.rmax)
            )
        if index != 0:
            dleft = self.rticks[index - 1] - r
            dright = self.rticks[index] - r
            if dleft <= dright:
                index -= 1
        elif index == self.nr:
            index -= 1
        return int(index)

    def get_z_index(self, z: float) -> int:
        """Get the index of the z mesh coordinate that is closest to `z`.

        Args:
            z (float): the desired z coordinate

        Raises:
            ValueError: if z < `self.mesh.zmin` or z > `self.mesh.zmax`

        Returns:
            int: index of self.mesh.zticks where the value is closest to z
        """
        # t = np.round(femtosec_time, decimals=10)
        index = np.searchsorted(self.zticks, z)  # "rounds" up
        if index < 0 or index > self.nz:
            raise ValueError(
                "z {} is outside of z bounds ({}-{})".format(z, self.zmin, self.zmax)
            )
        if index != 0:
            dleft = self.zticks[index - 1] - z
            dright = self.zticks[index] - z
            if dleft <= dright:
                index -= 1
        elif index == self.nz:
            index -= 1
        return int(index)


def get_comoving_frame(
    frame: NDArray, frametime: float, mesh: RectMesh, n: float = 1
) -> NDArray:
    """
    Shift the given simgle frame of the mesh along the z axis by
    frametime * c/n. Works the same way as :meth:`get_comoving_mesh` does, but
    only processes a single frame. (Useful e.g. when we don't want to load the
    whole mesh from the disc)

    Arguments:
        field {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        params {:class:`field.FieldParameters`} -- Parameters describing the
                propertities of the field.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Returns:
        ndarray -- the field values on the mesh moving with the same speed the
                pulse does. Has the same dimensions as the input mesh, and
                corresponds to it's MeshParameters.
    """
    rpoints, zpoints, = mesh.rticks, mesh.zticks
    c = C_LIGHT / n
    re_fieldintp = interpolate.interp2d(
        zpoints, rpoints, frame.real, copy=False, fill_value=0.0
    )
    im_fieldintp = interpolate.interp2d(
        zpoints, rpoints, frame.imag, copy=False, fill_value=0.0
    )
    transformed_z = zpoints + frametime * c
    re_comoving_frame = re_fieldintp(transformed_z, rpoints, assume_sorted=True)
    im_comoving_frame = im_fieldintp(transformed_z, rpoints, assume_sorted=True)
    comoving_frame = re_comoving_frame + 1j * im_comoving_frame
    return comoving_frame


# TODO params has no frametimes attr anymore
def get_comoving_mesh(field: NDArray, params: Type, mesh: RectMesh):
    """
    Shift the mesh along the z axis by frametime * c/n for each frame, to get a
    point of view where the pulse is in the center of the frame. Unknown values
    outside of the original frame will be choosen as 0.

    Arguments:
        field {ndarray} -- 3D array of the field values along the [frames, r, z]
                axes. Result of :meth:`tfield_optaxplane`.
        plottimes {ndarray} -- 1D array of timepoints for the corresponding
                frames. Result of :meth:`tfield_optaxplane`.
        params {:class:`field.FieldParameters`} -- Parameters describing the
                propertities of the field.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Returns:
        ndarray -- the field values on the mesh moving with the same speed the
                pulse does. Has the same dimensions as the input mesh, and
                corresponds to it's MeshParameters.
    """
    comoving_meshvals = np.empty_like(field)
    for i, t in enumerate(field.frametimes):
        comoving_meshvals[i] = get_comoving_frame(field[i], t, mesh)
    return np.array(comoving_meshvals)


def get_z_fourier_frame(frame: NDArray, mesh: RectMesh, n: float = 1):
    """ Fourier transform the frame along the z axis as z=ct

    Arguments:
        frame {ndarray} -- the frame to convert; a 2D array of (complex) field values on the r-z mesh
        params {:class:`field.FieldParameters`} -- Parameters describing the
                propertities of the field.
        mesh {:class:`MeshParameters`} -- Parameters describing the mesh.

    Returns:
        tuple --
    """
    c = C_LIGHT / n
    # the FFT corresponds to a sample with z-points beginning at zero, but
    #   they actually begin here:
    z_shift = mesh.zmin
    zt_shift = z_shift / c

    # "z-time" difference between the adjacent z points
    d_zt = mesh.dz / c

    # the "z-omega" points corresponding to the FT-ed frame
    z_omegas = 2 * const.pi * fftfreq(2 * frame.shape[1], d=d_zt)
    # do FFT along the z-time axis
    fft_frame = mesh.dz * fft(frame, n=2 * frame.shape[1], axis=1)
    # account for the z shift introduced above
    zfreq_frame = fft_frame * np.exp(-1j * z_omegas * zt_shift)
    # cut the result to have the same dimensions as the input                    #TODO fix this patchwork
    return z_omegas[: frame.shape[1]], zfreq_frame[:, : frame.shape[1]]


#! TODO: review this v

# def get_z_fourier_mesh(field, params, mesh):
#     freq_mesh = fft(field, axis=2)
#     dt = mesh.dz / (C_LIGHT / params.n1)
#     # multiply with -1 to reverse the time
#     freqs = 2*const.pi * fftfreq(field.shape[2], d=dt)
#     return freqs, freq_mesh


# ~~ parallel to adjacent plane, and parallel to 3D conversions
################################################################################


def get_zplane_frame(
    z: float, frame: NDArray, mesh: RectMesh, thetanum: Optional[int] = None
):
    """get frame adjacent to the optical axis at the given z coordinate, return
    the new points and their Cartesian coordinates"""

    if thetanum is None:
        thetanum = mesh.nz

    angl = np.linspace(0, 2 * const.pi, thetanum)
    r, thetas = np.meshgrid(mesh.rticks, angl, indexing="ij")
    x = r * np.cos(thetas)
    y = r * np.sin(thetas)
    z_index = np.searchsorted(mesh.zticks, z)

    # linear interpolation to get the z values #!(no, messes up energy)
    z_line = frame[
        :, z_index
    ]  # + ((frame[:,z_index+1] - frame[:,z_index]) / mesh.dz * (z - mesh.zticks[z_index]))
    tiled = np.tile(z_line, (thetanum, 1))  # axes: [phi, r]
    return x, y, np.transpose(tiled, (1, 0))


def get_zplane_field(
    z: float, field: NDArray, mesh: RectMesh, thetanum: Optional[int] = None
):
    """get field values adjacent to the optical axis at the given z coordinate, return
    the new points and their Cartesian coordinates"""

    if thetanum is None:
        thetanum = mesh.nr

    angl = np.linspace(0, 2 * const.pi, thetanum)
    r, thetas = np.meshgrid(mesh.rticks, angl, indexing="ij")
    x = r * np.cos(thetas)
    y = r * np.sin(thetas)
    z_index = np.searchsorted(mesh.zticks, z)

    # linear interpolation to get the z values
    z_line_t = field[
        :, :, z_index
    ]  # + ((field[:,:,z_index+1] - field[:,:,z_index]) / mesh.dz * (z - mesh.zticks[z_index]))
    tiled = np.tile(z_line_t, (thetanum, 1, 1))  # axes: [phi, t, r]
    return x, y, np.transpose(tiled, (1, 2, 0))


def get_3d_frame(
    frame: NDArray, mesh: RectMesh, thetanum: Optional[int] = None, m: int = 0
):
    """get frame values adjacent to the optical axis at the given z coordinate, return
    the new points and their Cartesian coordinates"""

    if thetanum is None:
        thetanum = mesh.nr

    angl = np.linspace(0, 2 * const.pi, thetanum)
    r, thetas, z = np.meshgrid(mesh.rticks, angl, mesh.zticks)  # , indexing='ij')
    x = r * np.cos(thetas)
    y = r * np.sin(thetas)

    tiled = np.tile(frame, (thetanum, 1, 1)) * np.exp(
        1j * thetas * m
    )  # axes: theta, r, z
    return x, y, z, np.transpose(tiled, axes=(0, 1, 2))


def get_3d_field(field: NDArray, mesh: RectMesh, thetanum: Optional[int] = None):
    """get field values adjacent to the optical axis at the given z coordinate, return
    the new points and their Cartesian coordinates"""

    if thetanum is None:
        thetanum = mesh.nr

    angl = np.linspace(0, 2 * const.pi, thetanum)
    r, thetas, z = np.meshgrid(mesh.rticks, angl, mesh.zticks, indexing="ij")
    x = r * np.cos(thetas)
    y = r * np.sin(thetas)

    tiled = np.tile(field, (thetanum, 1, 1, 1))  # axes: theta, t, r, z
    return x, y, z, np.transpose(tiled, axes=(1, 2, 0, 3))
