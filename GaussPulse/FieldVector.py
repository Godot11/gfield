#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Process the data computed by tempfield.py: read it, display it, and/or
calculate the total energy of the pulse"""

from __future__ import annotations

import json
import operator
import os
import time
import warnings
from copy import copy
from numbers import Number
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Literal,
    Optional,
    Text,
    Tuple,
    Type,
    TypeVar,
    Union,
    cast,
    overload,
)

constants = None
integrate = None

import numpy as np
import termcolor as tc
from nptyping import NDArray
from progressbar import progressbar as prbar
from scipy import constants as const
from scipy import integrate as intg

from .MonoField import TimeFormula

from . import (
    C_LIGHT,
    Comp,
    EPSILON_0,
    Norm,
    NumberType,
    NormStr,
    FieldFFT,
    MonoField,
    Parameters,
    ioutils,
    meshfield,
    physutils,
    utils,
)
from .meshfield import RectMesh

COMPONENTS: List[Comp] = [
    Comp.E_r,
    Comp.E_phi,
    Comp.E_z,
    Comp.B_r,
    Comp.B_phi,
    Comp.B_z,
]
E_COMPONENTS: List[Comp] = [Comp.E_r, Comp.E_phi, Comp.E_z]
B_COMPONENTS: List[Comp] = [Comp.B_r, Comp.B_phi, Comp.B_z]

# COMPONENT_NAMES: List[ComponentStr] = ["e_r", "e_phi", "e_z", "b_r", "b_phi", "b_z"]
# E_COMPONENT_NAMES: List[ComponentStr] = ["e_r", "e_phi", "e_z"]
# B_COMPONENT_NAMES: List[ComponentStr] = ["b_r", "b_phi", "b_z"]

PARAMETERS_FNAME = "parameters.json"
FIELDPARAMS_FNAME = "fieldparams.json"  # TODO rename
MESH_FNAME = "mesh.json"
VALS_FOLDNAME = "fieldvals/"

NDAVecTuple = Tuple[NDArray, NDArray, NDArray]


class FieldVector(object):
    """Base class for the field vectors.

    A FieldVector object contains the time- and space mesh parameters, and the
    respective field values for each cylindrical component. Ignored/zero
    components can be defined, these appear as components with constant zero value.

    Basic arithmetics (+, -, *, /, **, %) is implemented between either two
    :obj:`FieldVector` object, or a :obj:`FieldVector` and a number. The result
    will be a :class:`FieldVector`. (-obj, +obj, abs(obj), obj.real, obj.imag)
    are also understood in a similar way. Indexing as obj[i] will return the i-th
    (vectorial) frame (equivalent to obj.get_frame(i)), and len(obj) returns the
    number of frames (obj.nframes).

    Note:
        Normally this class is not used directly, but generated as a result of
        arithmetics done with subclasses.

    Args:
        mesh: the :obj:`RectMesh` object defining the spatial mesh.
        tmin: the starting time of the simulation
        tmax: end time of the simulation
        nframes: number of temporal frames ("time steps")
        comp_vals: a dictionary of component: value entries containing the component
            values. The missing components will be treated as constant zero components.
        m: vorticity number

    Note:
        the vorticity number plays no role in any calculation, but is used for some
        cases when a 3D representation is generated.

    Attributes:
        mesh: the :obj:`RectMesh` object defining the spatial mesh.
        tmin: the starting time of the simulation
        tmax: end time of the simulation
        nframes: number of temporal frames ("time steps")
        m: vorticity number
        r: the r coordinate points; equivalent to `mesh.r`
        z: the z coordinate points; equivalent to `mesh.z`
        nr: number of r coordinate points; equivalent to `mesh.nr` or `len(r)`
        nz: number of z coordinate points; equivalent to `mesh.nz` or `len(z)`
        dt: time difference between frames
        t: the time values for the frames
        frametimes: alias of `t`
        nonzero_comps: list of components with nonzero value
        zero_comps: list of components which are considered as constant zeros
        parameters: parameters corresponding to the field, or None if not given.
            This is not used in the base class, only preserved for information.
        fft_params: FFT calculation parameters. Again, this is only preserved for info.

    """

    label: str
    mesh: meshfield.RectMesh
    tmin: float
    tmax: float
    nframes: int
    m: int
    r: NDArray
    z: NDArray
    nr: int
    nz: int
    dt: float
    frametimes: NDArray
    t: NDArray
    nonzero_comps: List[Comp]
    zero_comps: List[Comp]
    params: Optional[Parameters.Parameters]
    fft_params: Optional[FieldFFT.FFTParameters]

    def __init__(
        self,
        mesh: RectMesh,
        tmin: float,
        tmax: float,
        nframes: int,
        comp_vals: Dict[Comp, NDArray] = {},
        m: int = 0,
        params: Optional[Parameters.Parameters] = None,
        fft_params: Optional[FieldFFT.FFTParameters] = None,
        label="",
    ) -> None:

        # copy some important params to own namespace
        self.mesh = mesh
        self.tmin, self.tmax = tmin, tmax
        self.nframes = nframes
        self.m = m

        self.r = self.mesh.rticks
        self.z = self.mesh.zticks
        self.nr = self.mesh.nr
        self.nz = self.mesh.nz

        self.frametimes, self.dt = np.linspace(
            self.tmin, self.tmax, self.nframes, retstep=True
        )
        self.t = self.frametimes

        # nonzero component values will be stored here as {'e_r': erval, ...}
        self.nonzero_comps = list(comp_vals.keys())
        self.zero_comps = copy(COMPONENTS)
        if self.nonzero_comps:  # not empty
            for nzeroc in self.nonzero_comps:
                try:
                    self.zero_comps.remove(nzeroc)
                except ValueError:
                    raise ValueError(f"invalid component name: {nzeroc}")

        self.comp_vals: Dict[Comp, NDArray] = {}
        for comp, val in comp_vals.items():
            self._set_component(comp, val)

        self.params = params
        self.fft_params = fft_params
        self.label = label

    def _set_component(self, component: Comp, values: NDArray) -> None:
        "set the given component to the value, with dimension check"
        if component not in COMPONENTS:
            raise ValueError(f"invalid component: {component}")
        if component in self.zero_comps:
            raise ValueError(
                "attempted to set zero component"
            )  # TODO what would be the best behaviour?
        if values is not None and values.shape != (self.nframes, self.nr, self.nz,):
            # !BUG: sometimes this is raised which should never happen
            raise ValueError(
                f"Array shape ({values.shape}) is incompatible with "
                + f"mesh size of (t: {self.nframes}, r: {self.nr}, z: {self.nz})."
            )
        self.comp_vals[component] = np.array(values)

    # this is called by len(): len(data) == data.__len__()
    def __len__(self) -> int:
        "return the number of frames"
        return self.nframes

    def __repr__(self) -> str:
        "return a compact string representation"
        st = self._get_str(colored=False)
        return (
            "FieldVector("
            + st.replace("\n  ", "\n").replace("\n", ", ").replace(": ", "=")
            + ")"
        )

    def __str__(self) -> str:
        "return a fancy string representation"
        return "FieldVector(\n" + self._get_str(colored=False) + "\n)"

    # this is called via indexing the object, e.g. data[2] == data.__getitem__(2)
    def __getitem__(self, i: int) -> NDArray:
        "return the (e, b) vectors of the i-th frame"
        if i >= self.__len__():
            raise IndexError("frame index out of range")
        return self.get_frame(i)

    def _op(self, other: Union[NumberType, FieldVector], op: Callable) -> FieldVector:
        "do the operation defined in op"
        new_compvals = {}
        if isinstance(other, (int, float, complex)):
            for comp in self.nonzero_comps:
                val = np.array(self.get_component(comp))
                new_compvals[comp] = op(val, other)
            return FieldVector(
                self.mesh,
                self.tmin,
                self.tmax,
                self.nframes,
                new_compvals,
                self.m,
                self.params,
                self.fft_params,
                self.label,
            )
        elif isinstance(other, FieldVector):
            if self.m != other.m:
                warnings.warn(
                    "The compared fields have different vortexity "
                    + f"(m={self.m} and m={other.m}); the comparsion may be "
                    + "erroneous! New vortexity will be set to m=0!"
                )
            self._check_comparable(other)
            nonzeros = list(set(self.nonzero_comps).union(other.nonzero_comps))
            for comp in nonzeros:
                new_compvals[comp] = op(
                    self.get_component(comp), other.get_component(comp)
                )
            return FieldVector(
                self.mesh, self.tmin, self.tmax, self.nframes, new_compvals
            )
        else:
            raise TypeError(
                f"unsupported operand type(s) for {op}: {self.__class__} and {other.__class__}"
            )

    def __add__(self, other: Union[NumberType, FieldVector]) -> FieldVector:
        return self._op(other, operator.add)

    def __sub__(self, other: Union[NumberType, FieldVector]) -> FieldVector:
        return self._op(other, operator.sub)

    __rsub__ = __sub__

    def __truediv__(self, other: Union[NumberType, FieldVector]) -> FieldVector:
        return self._op(other, operator.truediv)

    def __mul__(self, other: Union[NumberType, FieldVector]) -> FieldVector:
        return self._op(other, operator.mul)

    __rmul__ = __mul__

    def __pow__(self, other: Union[NumberType, FieldVector]) -> FieldVector:
        return self._op(other, operator.pow)

    def __mod__(self, other: Union[NumberType, FieldVector]) -> FieldVector:
        return self._op(other, operator.mod)

    def __neg__(self) -> FieldVector:
        return self.__mul__(-1)

    def __pos__(self) -> FieldVector:
        return self

    def __abs__(self) -> FieldVector:
        new_compvals = {}
        for comp in self.nonzero_comps:
            new_compvals[comp] = np.abs(self.get_component(comp))
        return FieldVector(self.mesh, self.tmin, self.tmax, self.nframes, new_compvals)

    @property
    def real(self) -> FieldVector:
        "Real part of the field components as FieldVector."
        new_compvals = {}
        for comp in self.nonzero_comps:
            new_compvals[comp] = np.real(self.get_component(comp))
        return FieldVector(self.mesh, self.tmin, self.tmax, self.nframes, new_compvals)

    @property
    def imag(self) -> FieldVector:
        "Imaginary part of the field components as FieldVector."
        new_compvals = {}
        for comp in self.nonzero_comps:
            new_compvals[comp] = np.imag(self.get_component(comp))
        return FieldVector(self.mesh, self.tmin, self.tmax, self.nframes, new_compvals)

    @property
    def ampl(self) -> FieldVector:
        "Amplitude of the field components as FieldVector."
        return self.__abs__()

    @property
    def phase(self) -> FieldVector:
        "Phase of the field components as FieldVector."
        new_compvals = {}
        for comp in self.nonzero_comps:
            new_compvals[comp] = np.angle(self.get_component(comp))
        return FieldVector(self.mesh, self.tmin, self.tmax, self.nframes, new_compvals)

    def _get_str(
        self,
        colored: bool = False,
        namecolor: Optional[str] = "green",
        valcolor: Optional[str] = None,
    ) -> str:
        """Get a pretty string representation including the most important
        parameters, optionally colored.

        Args:
            colored (bool, optional): Whether the string should contain color
                defining codes. Defaults to False.
            namecolor (str, optional): Color of the parameter name. Defaults to "green".
            valcolor (str, optional): Color of the parameter value. Defaults to None.

        Returns:
            str: pretty string representation of the class.
        """

        props = {
            "label": self.label,
            "nonzero comps": self.nonzero_comps,
            "tmin": self.tmin,
            "tmax": self.tmax,
            "nframes": self.nframes,
            "dt": self.dt,
        }

        propsstr = utils.dict_to_pretty_str(
            props, colored=colored, namecolor=namecolor, valcolor=valcolor,
        )

        meshname = tc.colored("Mesh:", color=namecolor)

        meshstr = utils.dataclass_to_pretty_str(
            self.mesh,
            colored=colored,
            namecolor=namecolor,
            valcolor=valcolor,
            linestart="  ",
            blacklist=("rticks", "zticks", "r", "z"),
        )
        return propsstr + "\n" + meshname + "\n" + meshstr

    def prettyprint(
        self,
        colored: bool = False,
        namecolor: Optional[str] = "green",
        valcolor: Optional[str] = None,
        compact: bool = False,
    ) -> None:
        """Pretty print the most important parameters, optionally colored.

        Args:
            colored (bool, optional): Whether the string should contain color
            defining codes. Defaults to False.
            namecolor (str, optional): Color of the parameter name. Defaults to "green".
            valcolor (str, optional): Color of the parameter value. Defaults to None.
            compact (bool, optional): if set, no newline is used.

        Returns:
            str: pretty string representation of the class.
        """
        st = self._get_str(colored, namecolor, valcolor)
        if compact:
            st = st.replace("\n", ",").replace(": ", "=")
        print(st)

    def _check_comparable(self, other: Any) -> None:
        "Raise ValueError if `other` is not comparable to `self`."
        if not self.is_comparable_to(other):
            raise ValueError(
                "The two fields have different spatio-temporal coordinates"
            )

    def is_comparable_to(self, other: Any) -> bool:
        """Check whether `other` has the same mesh and frame times, so a
        quantitative compersion can be done without resampling (currently unimplemented).

        Args:
            other (Any): object to compare with

        Returns:
            bool: True if the mesh and time points are the same, False othervise.
        """

        "has the same spatio-temporal dimensions as the other one"
        if not isinstance(other, FieldVector):
            return False
        return (
            self.mesh == other.mesh
            and self.nframes == other.nframes
            and self.tmin == other.tmin
            and self.tmax == other.tmax  #!  #!
        )

    def get_component(self, component: Comp) -> NDArray:
        """
        Return the values of the specified component as a 3d array with
        axes [t, r, z]

        Args:
            component (str): the component to return.

        Returns:
            NDArray: the component values as [t,r,z].
        """
        if component in self.zero_comps:
            compval = np.zeros(
                (self.nframes, self.mesh.nr, self.mesh.nz), dtype="complex",
            )
        else:
            compval = self.comp_vals[component]

        return compval

    def get_component_frame(self, component: Comp, i: int) -> NDArray:
        """Get the i-th frame of the component.

        Args:
            component (str): the component to return.
            i (int): the number of the frame. Corresponding time will be `self.frametimes[i]`.

        Returns:
            NDArray: the values of the component at t=self.frametimes[i] as [r,z]
        """
        comp = self.get_component(component)
        return comp[i]

    def get_t_index(self, t: float) -> int:
        """Get the index of the frame (i) whose time is closest to `t`. The corresponding
        frame then will be self.get_frame(i), and the time is self.frametimes[i].

        Args:
            t (float): the desired timepoint

        Raises:
            ValueError: if t < `self.tmin` or t > `self.tmax`

        Returns:
            int: index of the frame closest to t
        """
        # t = np.round(femtosec_time, decimals=10)
        index = np.searchsorted(self.frametimes, t)  # "rounds" up
        if index < 0 or index > self.nframes:
            raise ValueError(
                "time {} is outside of time bounds ({}-{})".format(
                    t, self.tmin, self.tmax
                )
            )
        if index != 0:
            dleft = t - self.frametimes[index - 1]
            dright = self.frametimes[index] - t
            if dleft <= dright:
                index -= 1
        elif index == self.nframes:
            index -= 1
        return int(index)

    def get_r_index(self, r: float) -> int:
        """Get the index of the r mesh coordinate that is closest to `r`.

        Args:
            r (float): the desired r coordinate

        Raises:
            ValueError: if r < `self.mesh.rmin` or r > `self.mesh.rmax`

        Returns:
            int: index of self.mesh.rticks where the value is closest to r
        """
        return self.mesh.get_r_index(r)

    def get_z_index(self, z: float) -> int:
        """Get the index of the z mesh coordinate that is closest to `z`.

        Args:
            z (float): the desired z coordinate

        Raises:
            ValueError: if z < `self.mesh.zmin` or z > `self.mesh.zmax`

        Returns:
            int: index of self.mesh.zticks where the value is closest to z
        """
        return self.mesh.get_z_index(z)

    def get_frame(
        self, i: int
    ) -> Tuple[Tuple[NDArray, NDArray, NDArray], Tuple[NDArray, NDArray, NDArray]]:
        "Get the i-th frame of the field as a tuple of (E, B) vectors."

        e_vector: List[NDArray] = []
        b_vector: List[NDArray] = []
        for comp in E_COMPONENTS:
            e_vector.append(self.get_component_frame(comp, i))
        for comp in B_COMPONENTS:
            b_vector.append(self.get_component_frame(comp, i))

        return cast(NDAVecTuple, tuple(e_vector)), cast(NDAVecTuple, tuple(b_vector))

    def get_comoving_component_frame(self, comp: Comp, i: int) -> NDArray:
        "Return one frame of the selected component with the pulse being in it's center."

        compvals = self.get_component_frame(comp, i)
        if comp in self.zero_comps:
            return compvals  # it's zero anyway
        else:
            return meshfield.get_comoving_frame(compvals, self.frametimes[i], self.mesh)

    def get_comoving_frame(
        self, i: int
    ) -> Tuple[Tuple[NDArray, NDArray, NDArray], Tuple[NDArray, NDArray, NDArray]]:
        """
        Get the i-th frame of the mesh. Load the nonzero E and B field
        component values on-demand if they're not loaded, or get them from
        the loaded values. Return a tuple of the E and B vectors on the mesh,
        with axes [component, r, z].
        """
        e_vector = []
        b_vector = []
        for comp in E_COMPONENTS:
            e_vector.append(self.get_comoving_component_frame(comp, i))
        for comp in B_COMPONENTS:
            b_vector.append(self.get_comoving_component_frame(comp, i))

        return cast(NDAVecTuple, tuple(e_vector)), cast(NDAVecTuple, tuple(b_vector))

    def get_component_z_fourier_frame(
        self, comp: Comp, i: int, comoving: bool = True
    ) -> NDArray:
        """Get the i-th frame, Fourier transformed along the z axis as z = c*t.
            If comoving is True, the FFT of the comoving frame is returned"""

        if comoving is True:
            compvals = self.get_comoving_component_frame(comp, i)
        else:
            compvals = self.get_component_frame(comp, i)

        ft_comp = meshfield.get_z_fourier_frame(compvals, self.mesh)[1]

        return ft_comp

    def get_z_fourier_frame(
        self, i: int
    ) -> Tuple[Tuple[NDArray, NDArray, NDArray], Tuple[NDArray, NDArray, NDArray]]:
        e_vector = []
        b_vector = []
        for comp in E_COMPONENTS:
            e_vector.append(self.get_component_z_fourier_frame(comp, i))
        for comp in B_COMPONENTS:
            b_vector.append(self.get_component_z_fourier_frame(comp, i))

        return np.array(e_vector), np.array(b_vector)

    def get_component_limitvals(self, comp: Comp, norm: Norm) -> Tuple[float, float]:
        norms: Dict[NormStr, Callable] = {
            "re": np.real,
            "im": np.imag,
            "ampl": np.abs,
            "abs": np.abs,
        }
        maxval = -np.inf
        minval = np.inf
        for i in range(self.nframes):
            frame = self.get_component_frame(comp, i)
            frame = physutils.get_qty(frame, norm)
            mx = np.amax(frame)
            if maxval < mx:
                maxval = mx
            mn = np.amin(frame)
            if minval > mn:
                minval = mn
        return minval, maxval

    def get_limitvals(self, norm: Norm) -> Tuple[float, float]:
        lims = [self.get_component_limitvals(comp, norm) for comp in self.nonzero_comps]
        mins, maxes = np.transpose(lims)
        return np.amin(mins), np.amax(maxes)

    def normalize_compval(self, norm: Norm, comp: Optional[Comp] = None) -> FieldVector:
        return normalize_compval(self, norm, comp)
        # if comp is not None:
        #     _, maxval = self.get_component_limitvals(comp, norm)
        # else:
        #     _, maxval = self.get_limitvals(norm)
        # return self / maxval

    def get_intensity_frame(
        self, i: int, which: Literal["e", "b"] = "e", time_avg: bool = False
    ) -> NDArray:
        "return the total intensity at the frame; type can be momentary or 'avg', comp can be 'e' or 'b'"
        (er, ep, ez), (br, bp, bz) = self.get_frame(i)
        if time_avg:
            norm = np.abs
        else:
            norm = np.real

        if which == "e":
            return norm(er) ** 2 + norm(ep) ** 2 + norm(ez) ** 2
        elif which == "b":
            return norm(br) ** 2 + norm(bp) ** 2 + norm(bz) ** 2

    def get_energy_density_frame(self, i: int, time_avg: bool = True) -> NDArray:
        return frame_energy_density(self, i, time_avg)

    def get_frame_energy(
        self, i: int, time_avg: bool = True, exclude_optax: bool = False
    ) -> float:
        return frame_energy(self, i, time_avg, exclude_optax)

    def get_component_frame_energy(
        self, comp: Comp, i: int, time_avg: bool = True
    ) -> float:

        compval = self.get_component_frame(comp, i)
        if time_avg:
            edens = np.abs(compval) ** 2
        else:
            edens = compval.real ** 2

        rpoints = self.r
        zpoints = self.z
        r, _ = np.meshgrid(rpoints, zpoints, indexing="xy")
        # integrate over r
        en_r = intg.simps(edens * r.transpose(), x=rpoints, axis=0)
        # integrate over z
        energy = intg.simps(en_r, x=zpoints)
        return energy

    def get_centroid_zcoord(
        self, i: int, which: Literal["e", "b"] = "e", time_avg: bool = True
    ) -> float:
        "get the centroid of the beam (Pu article, eq. 9)"
        rpoints = self.mesh.rticks
        zpoints = self.mesh.zticks

        intensity = self.get_intensity_frame(i, which, time_avg)
        intg_i_dr = intg.simps(intensity, x=rpoints, axis=0)
        counter = intg.simps(zpoints * intg_i_dr, x=zpoints)
        divisor = intg.simps(intg_i_dr, x=zpoints)
        return counter / divisor

    def extract_part(
        self,
        tbounds: Optional[Tuple[float, float]] = None,
        rbounds: Optional[Tuple[float, float]] = None,
        zbounds: Optional[Tuple[float, float]] = None,
    ) -> FieldVector:
        if tbounds is None:
            ntmin, ntmax = 0, self.nframes
            tmin, tmax = self.tmin, self.tmax
        else:
            tmin, tmax = tbounds
            ntmin = self.get_t_index(tmin)
            ntmax = self.get_t_index(tmax)
            tmin, tmax = self.t[[ntmin, ntmax]]

        if rbounds is None:
            nrmin, nrmax = 0, self.mesh.nr
            rmin, rmax = 0.0, self.mesh.rmax
        else:
            rmin, rmax = rbounds
            nrmin = self.get_t_index(rmin)
            nrmax = self.get_t_index(rmax)
            rmin, rmax = self.r[[nrmin, nrmax]]

        if zbounds is None:
            nzmin, nzmax = 0, self.mesh.nz
            zmin, zmax = self.mesh.zmin, self.mesh.zmax
        else:
            zmin, zmax = zbounds
            nzmin = self.get_z_index(zmin)
            nzmax = self.get_z_index(zmax)
            zmin, zmax = self.z[[ntmin, ntmax]]

        mesh = meshfield.RectMesh(rmax, zmin, zmax, nrmax - nrmin, nzmax - nzmin)
        fields = {}
        for comp in self.nonzero_comps:
            val = self.get_component(comp)
            fields[comp] = val[ntmin:ntmax, nrmin:nrmax, nzmin:nzmax]

        return FieldVector(mesh, tmin, tmax, ntmax - ntmin, fields, self.m)

    def save_parameters(self, fpath: str) -> None:
        ioutils.save_parameters_as_json(self, fpath)

    def save_coordinates(self, fpath: str) -> None:
        ioutils.save_coordinates_as_json(self, fpath)

    def save_component_vals(self, comp: Comp, folder: str) -> None:
        ioutils.save_component_vals(self, comp, folder)

    def save(self, folder: str) -> None:
        ioutils.save_field(self, folder)

    def denormalize(self, lambda0: float, c_light: float) -> DenormalizedFieldVector:

        mp = self.mesh
        denorm_rmin, denorm_rmax, denorm_zmin, denorm_zmax = physutils.denormalize(
            lambda0,
            c_light,
            unit_type="length",
            vals=(mp.rmin, mp.rmax, mp.zmin, mp.zmax),
        )
        denorm_mesh = meshfield.RectMesh(
            denorm_rmax, denorm_zmin, denorm_zmax, mp.nr, mp.nz, rmin=denorm_rmin
        )
        denorm_tmin, denorm_tmax, = physutils.denormalize(
            lambda0, c_light, unit_type="time", vals=(self.tmin, self.tmax)
        )
        # r, z, t frametimes
        # t
        denorm_comps = {}
        for comp in self.nonzero_comps:
            compval = self.get_component(comp)
            if comp in E_COMPONENTS:
                unit_type = "efield"
            elif comp in B_COMPONENTS:
                unit_type = "bfield"
            else:
                raise ValueError("wtf happened")
            denorm_comps[comp] = physutils.denormalize(
                lambda0, c_light, unit_type=unit_type, vals=compval
            )

        denormed = DenormalizedFieldVector(
            denorm_mesh,
            denorm_tmin,
            denorm_tmax,
            self.nframes,
            lambda0,
            c_light,
            denorm_comps,
            self.m,
            self.params,
            self.fft_params,
        )

        # check some stuff
        denorm_r, denorm_z = physutils.denormalize(
            lambda0, c_light, "length", (self.r, self.z)
        )
        denorm_t = physutils.denormalize(lambda0, c_light, "time", self.t)
        assert np.allclose(denorm_r, denormed.r, rtol=1e-05, atol=1e-08)
        assert np.allclose(denorm_z, denormed.z, rtol=1e-05, atol=1e-08)
        assert np.allclose(denorm_t, denormed.t, rtol=1e-05, atol=1e-08)

        return denormed

    def set_energy(
        self,
        energy,
        ref_frame_i: Optional[int] = None,
        time_avg: bool = True,
        exclude_optax: bool = True,
    ) -> FieldVector:
        f = set_energy(self, energy, ref_frame_i, time_avg, exclude_optax)
        return f

    # def get_poynting_vec_frame(i):
    #     return frame_poynting(self, i)


class DenormalizedFieldVector(FieldVector):
    def __init__(
        self,
        mesh: RectMesh,
        tmin: float,
        tmax: float,
        nframes: int,
        lambda0: float,
        c_light: float,
        comp_vals: Dict[Comp, NDArray] = {},
        m: int = 0,
        parameters: Optional[Parameters.Parameters] = None,
        fft_params: Optional[FieldFFT.FFTParameters] = None,
        label: str = "",
    ) -> None:
        super(DenormalizedFieldVector, self).__init__(
            mesh, tmin, tmax, nframes, comp_vals, m, parameters, fft_params, label
        )
        self.lambda0 = lambda0
        self.c_light = c_light


class FieldFromFormula(FieldVector):
    formula: MonoField.FieldFormula
    params: Any
    t_pulse: float
    is_computed: Dict[Comp, bool]
    ondemand_allowed: bool

    def __init__(
        self,
        formula: MonoField.TimeFormula,
        params: Any,
        mesh: meshfield.RectMesh,
        nframes: int,
        tmin: float,
        tmax: float,
        t_pulse: float,
        allow_ondemand_calculation: bool = True,
        component_whitelist: Optional[List[Comp]] = None,
        label: str = "",
    ) -> None:

        # TODO: manage this properly
        if hasattr(params, "m"):
            self.m = params.m
        else:
            self.m = 0

        super(FieldFromFormula, self).__init__(
            mesh=mesh, tmin=tmin, tmax=tmax, nframes=nframes, comp_vals={}, label=label,
        )

        self.formula = formula
        self.zero_comps = self.formula.zero_comps
        self.nonzero_comps = self.formula.nonzero_comps

        if component_whitelist is not None:
            for comp in copy(self.nonzero_comps):
                if comp not in component_whitelist:
                    self.nonzero_comps.remove(comp)
                    self.zero_comps.append(comp)

        self.params = params
        self.t_pulse = t_pulse
        # self.omega0 = self.params.omega0

        self.is_computed = {}
        for comp in COMPONENTS:
            self.is_computed[comp] = False if comp in self.nonzero_comps else True

        self.ondemand_allowed = allow_ondemand_calculation

    def _get_str(
        self,
        colored: bool = False,
        namecolor: Optional[str] = "green",
        valcolor: Optional[str] = None,
    ) -> str:

        extra_str = tc.colored("t pulse: ", namecolor) + tc.colored(f"{self.t_pulse}")
        supstr = super(FieldFromFormula, self)._get_str(colored, namecolor, valcolor)

        pmsname = tc.colored("Parameters:", namecolor)
        pmsstr = utils.dataclass_to_pretty_str(
            self.params,
            colored=colored,
            namecolor=namecolor,
            valcolor=valcolor,
            linestart="  ",
        )
        return extra_str + "\n" + supstr + "\n" + pmsname + "\n" + pmsstr

    def _check_is_computed(self, component: Comp) -> None:
        if not self.is_computed_component(component):
            raise KeyError(f"{component} component is not yet calculated")

    def is_computed_component(self, component: Comp) -> bool:
        if component not in COMPONENTS:
            raise ValueError("invalid component name: {}".format(component))
        return self.is_computed[component]

    def del_component(self, component: Comp) -> None:
        "Delete the component from memory (free up space). Silently ignores zero components."
        if component in self.nonzero_comps:
            self._set_component(component, None)
            self.is_computed[component] = False

    def del_all_components(self) -> None:
        "Delete the loaded field from memory (free up space)."
        for comp in COMPONENTS:
            self.del_component(comp)

    def get_component_frame(self, component: Comp, i: int):
        self._check_is_computed(component)
        if self.is_computed_component(component):
            return super(FieldFromFormula, self).get_component_frame(component, i)
        elif self.ondemand_allowed:
            t = self.t[i]
            return self.formula.component(
                component, self.mesh.rmesh, 0, self.mesh.zmesh, t, self.params,
            )
        else:
            raise ValueError(
                f"Component '{component}' is not calculated, and on-demand calculation is disabled."
            )

    def _core_calculate_component(
        self,
        component: Comp,
        mesh: meshfield.RectMesh,
        multicore: bool,
        show_progress: bool,
    ) -> NDArray:
        fieldvals = FieldFFT.tfield_optaxplane_from_tformula(
            cast(MonoField.TimeFormula, self.formula),
            component,
            self.params,
            mesh,
            self.frametimes,
            multicore,
            show_progress,
        )
        return fieldvals

    def calculate_component(
        self,
        component: Comp,
        show_progress: bool = True,
        show_stats: bool = True,
        multicore: bool = True,
    ) -> None:

        if component in self.zero_comps:
            warnings.warn(f"{component} is always 0")
            return

        if self.is_computed_component(component):
            warnings.warn(f"{component} is already calculated")
            return
        start_time = time.time()
        # get the field values and frame times

        result = self._core_calculate_component(
            component, self.mesh, multicore, show_progress
        )

        self._set_component(component, result)
        self.is_computed[component] = True

        comp_elapsed_time = time.time() - start_time

        # print component-vise runtime
        if show_stats:
            timestr = time.strftime("%H:%M:%S", time.gmtime(comp_elapsed_time))
            time_per_point = comp_elapsed_time / (self.mesh.nr * self.mesh.nz) * 1000
            print(
                f"Component {component} calculated in {timestr} ({time_per_point:.5} ms per point)"
            )

    def calculate_field(
        self,
        show_progress: bool = True,
        show_stats: bool = True,
        multicore: bool = True,
    ) -> None:

        start_time = time.time()

        comps = copy(self.nonzero_comps)

        for comp in comps:
            if show_progress:
                tc.cprint(f"Calculating {comp.value} component...", "blue")
            self.calculate_component(comp, show_progress, show_stats, multicore)

        elapsed_time = time.time() - start_time
        if show_stats:
            timestr = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
            time_per_point = (
                elapsed_time / (self.mesh.nr * self.mesh.nz * len(comps)) * 1000
            )
            print(f"Field calculated in {timestr} ({time_per_point:.5} ms per point)")

    def estimate_comp_calc_time_ms(
        self,
        component: Comp,
        multicore: bool = True,
        test_rsize: int = 10,
        test_zsize: int = 10,
    ) -> float:
        "Estimate the time neccessary for calculating the given component; inaccurate for fast calculations"

        if component in self.zero_comps:
            return 0

        test_mesh = self.mesh.resample(test_rsize, test_zsize)

        startt = time.time()  #!
        self._core_calculate_component(
            component, test_mesh, multicore, show_progress=False
        )
        endt = time.time()
        t_per_point = (endt - startt) / (test_rsize * test_zsize)
        t_est_ms = self.mesh.nr * self.mesh.nz * t_per_point
        return t_est_ms

    def estimate_full_calc_time_ms(
        self, multicore: bool = True, test_rsize: int = 10, test_zsize: int = 10
    ) -> float:
        "Estimate the time neccessary for calculating all three components"
        tcalc = 0.0
        comps = copy(self.nonzero_comps)
        for comp in comps:
            tcalc += self.estimate_comp_calc_time_ms(
                comp, multicore, test_rsize, test_zsize
            )
        return tcalc

    def calculate_save_component_vise(
        self,
        folder: str,
        show_progress: bool = True,
        show_stats: bool = True,
        multicore: bool = True,
    ) -> None:
        "calculate component, save, unload, calculate next (to save memory). Result is the same structure as from self.save"
        for comp in self.nonzero_comps:
            self.calculate_component(comp, show_progress, show_stats, multicore)
            savefolder = os.path.join(folder, comp.value)
            ioutils.save_component_vals(self, comp, savefolder)
            self.del_component(comp)
        params_path = os.path.join(folder, PARAMETERS_FNAME)
        coords_path = os.path.join(folder, "coords.json")
        ioutils.save_parameters_as_json(self, params_path)
        ioutils.save_coordinates_as_json(self, coords_path)


class FieldFromSpectralFormula(FieldFromFormula):
    formula: MonoField.FieldFormula
    envl_t_param: float
    envelope_type: Literal["gauss", "poisson"]
    t_pulse: float
    s: float  # todo: manage t and s in FieldFromFormula?
    omega_log_range: float
    fft_params: FieldFFT.FFTParameters
    phase_derivates: List[float]

    def __init__(
        self,
        formula: MonoField.SpectralFormula,
        params: Any,
        mesh: meshfield.RectMesh,
        nframes: int,
        tmin: float,
        tmax: float,
        t_pulse: float,
        envelope_type: Literal["gauss", "poisson"],
        phase_derivates: List[float] = [],
        omega_log_range: float = 10,
        allow_ondemand_calculation: bool = True,
        component_whitelist: Optional[List[Comp]] = None,
        label="",
    ):

        super(FieldFromSpectralFormula, self).__init__(
            formula=cast(MonoField.TimeFormula, formula),
            params=params,
            mesh=mesh,
            nframes=nframes,
            tmin=tmin,
            tmax=tmax,
            t_pulse=t_pulse,
            allow_ondemand_calculation=allow_ondemand_calculation,
            component_whitelist=component_whitelist,
            label=label,
        )
        omega0 = 2 * np.pi * C_LIGHT
        # setup spectral space parameters

        self.envelope_type = envelope_type
        self.t_pulse = t_pulse
        self.s: float = physutils.t_to_poisson_s_exact(self.t_pulse)
        if self.envelope_type == "gauss":
            self.envl_t_param = t_pulse
        else:
            self.envl_t_param = self.s

        self.omega_log_range = omega_log_range

        ommin, ommax = FieldFFT.envelope_magnitude_range(
            self.envelope_type, self.t_pulse, self.omega_log_range, omega0
        )

        domega_max = (ommax - ommin) / (
            2 * nframes
        )  # ? what would be the best choice? user-defined?

        self.fft_params = FieldFFT.FFTParameters(
            eval_omega_min=ommin,
            eval_omega_max=ommax,
            dt=self.dt,
            maximal_domega=domega_max,
            tmin=tmin,
            tmax_aim=tmax,
        )

        # update time info from the fft params
        self.frametimes = self.fft_params.times
        self.tmin, self.tmax = self.fft_params.tmin, self.fft_params.tmax
        self.frametimes = self.fft_params.times
        self.t = self.frametimes
        self.phase_derivates = phase_derivates

    def _get_str(
        self,
        colored: bool = False,
        namecolor: Optional[str] = "green",
        valcolor: Optional[str] = None,
    ) -> str:
        extra_str = tc.colored("phase derivates: ", namecolor) + tc.colored(
            f"{self.phase_derivates}"
        )
        extra_str += "\n" + tc.colored("s: ", namecolor) + tc.colored(f"{self.s}")
        supstr = super(FieldFromSpectralFormula, self)._get_str(
            colored, namecolor, valcolor
        )
        fftname = tc.colored("FFT parameters:", color=namecolor)
        fftstr = utils.dataclass_to_pretty_str(
            self.fft_params,
            colored=colored,
            namecolor=namecolor,
            valcolor=valcolor,
            linestart="  ",
        )

        return supstr + "\n" + fftname + "\n" + fftstr

    def get_monochromatic_comp(
        self, comp: Comp, omega: Optional[float] = None
    ) -> NDArray:  #!
        if omega is None:
            omega = self.params.omega0
        return self.formula.component(
            comp, self.mesh.rmesh, 0, self.mesh.zmesh, omega, self.params,
        )

    def _core_calculate_component(
        self,
        component: Comp,
        mesh: meshfield.RectMesh,
        multicore: bool,
        show_progress: bool,
    ) -> NDArray:
        fieldvals = FieldFFT.tfield_optaxplane(
            formula=cast(MonoField.SpectralFormula, self.formula),
            component=component,
            mesh=mesh,
            envl_t_param=self.envl_t_param,
            params=self.params,
            fftparams=self.fft_params,
            envelope_type=self.envelope_type,
            phase_derivates=self.phase_derivates,
            multicore=multicore,
            show_progress=show_progress,
        )
        return fieldvals


class FieldFromSaved(FieldVector):
    path: str
    params_path: str
    t_pulse: Optional[float]
    omega_log_range: Optional[float]
    phase_derivates: Optional[list]
    params: Optional[Any]
    fft_params: Optional[FieldFFT.FFTParameters]
    is_loaded: Dict[Comp, bool]

    def __init__(self, field_path: str) -> None:
        """Class that reads the saved electromagnetic field values created by
        compute_field.py.

        The main point is not having to load all three component's full time-r-z
        array (which may be huge) at the same time, only the requested frames or
        components. Loading all three to memory is possible with self.load_field(),
        which may save time, but needs a lot of RAM for bigger meshes.

        Arguments:
            field_path {path} -- Path to the folder containing the saved
                    e_r, e_z (and b_phi, in case of Levy's parameters) field

        Attributes:
            path {str} -- the path of the saved field's folder
            params {:class:`GaussPulse.field.FieldParameters`} -- parameters
                    of the field
            mesh {:class:`GaussPulse.meshfield.MeshParameters`} -- parameters
                    of the mesh
            frametimes {ndarray} -- array of the frame times
            nframes {ndarray} -- number of the frames (equal to len(frametimes))
            field_type {str} -- indicator of the field type ('levy' or 'pu')
            is_loaded {bool} -- whether the values have been loaded from the disc
                    via load_field_vals(), or are acessed on-demand
            e_r_vals {ndarray} -- the loaded e_r values ({None} before
                     load_field_vals() called)
            e_z_vals {ndarray} -- the loaded e_z values ({None} before
                    load_field_vals() called)
            b_phi_vals {ndarray} -- the loaded b_phi values ({None} before
                    load_field_vals() called)
        """

        self.path = field_path
        self.params_path = os.path.join(self.path, ioutils.PARAMETERS_FNAME)

        # load the json data
        with open(self.params_path) as f:
            # print(f.read())
            jdict = json.load(f)

        # parse main parameters
        self.nframes = int(jdict["nframes"])
        self.tmin = float(jdict["tmin"])
        self.tmax = float(jdict["tmax"])
        self.dt = float(jdict["dt"])
        self.zero_comps = [Comp(i) for i in list(jdict["zero_comps"])]
        self.nonzero_comps = [Comp(i) for i in list(jdict["nonzero_comps"])]
        self.m = int(jdict["m"])

        # parse mesh
        mpmsdict: Dict[str, Union[int, float]] = jdict["mesh"]
        self.mesh = meshfield.RectMesh(
            rmax=float(mpmsdict["rmax"]),
            zmin=float(mpmsdict["zmin"]),
            zmax=float(mpmsdict["zmax"]),
            nr=int(mpmsdict["nr"]),
            nz=int(mpmsdict["nz"]),
        )

        # parse params (if exists)
        try:
            fpmsdict = jdict["params"]
        except KeyError:
            self.params = None
        else:
            self.params = utils.dict_to_dataclass(fpmsdict)

        # parse FFT parameters (if exists)
        try:
            fftdict = jdict["fft_pms"]
        except KeyError:
            self.fft_params = None
            self.frametimes = self.tmin + np.array(
                [i * self.dt for i in range(self.nframes)]
            )
        else:
            self.fft_params = FieldFFT.FFTParameters(
                eval_omega_min=float(fftdict["eval_omega_min"]),
                eval_omega_max=float(fftdict["eval_omega_max"]),
                dt=float(fftdict["dt"]),
                maximal_domega=float(fftdict["d_omega"]),
                tmin=float(fftdict["tmin"]),
                tmax_aim=float(fftdict["tmax"]),
            )
            # check if omega changed (#TODO: this shouldn't happen!)
            loaded_domega = self.fft_params.d_omega
            original_domega = fftdict["d_omega"]
            if loaded_domega != original_domega:
                warnings.warn(
                    f"domega of reconstructed fft_params ({loaded_domega:.2g})"
                    + f"does not correspond to original one({original_domega:.2g}); "
                    + "frequency-related fft parameters are inaccurate!"
                )

            # TODO: should be the same as self.fft_params.times -> check!
            self.frametimes = self.tmin + np.array(
                [i * self.dt for i in range(self.nframes)]
            )  # self.frametimes = self.fft_params.times

        self.r = self.mesh.rticks
        self.z = self.mesh.zticks
        self.t = self.frametimes

        self.is_loaded = {}
        # zero components are treated as loaded
        for comp in COMPONENTS:
            self.is_loaded[comp] = False if comp in self.nonzero_comps else True
        self.comp_vals = {}

        self.label = str(jdict.get("label", ""))

    def _get_str(
        self,
        colored: bool = False,
        namecolor: Optional[str] = "green",
        valcolor: Optional[str] = None,
    ) -> str:
        supstr = super(FieldFromSaved, self)._get_str(colored, namecolor, valcolor)
        if self.params is not None:
            supstr += "\n" + utils.dataclass_to_pretty_str(
                self.params, colored=colored, namecolor=namecolor, valcolor=valcolor,
            )
        if self.fft_params is not None:
            supstr += "\n" + utils.dataclass_to_pretty_str(
                self.fft_params,
                colored=colored,
                namecolor=namecolor,
                valcolor=valcolor,
            )
        if colored:
            path_str = (
                tc.colored("path:", namecolor)
                + tc.colored(str(self.path), valcolor)
                + "\n"
            )
        else:
            path_str = "path: " + str(self.path) + "\n"
        return path_str + supstr

    def _set_component(self, component: Comp, values: NDArray) -> None:
        if component not in COMPONENTS:
            raise ValueError(f"invalid component: {component}")
        self.comp_vals[component] = values

    def component_path(self, comp: Comp) -> Optional[str]:
        if comp not in COMPONENTS:
            raise ValueError(f"invalid component: {comp}" + str(comp))
        if comp in self.zero_comps:
            return None  # ? should this raise error?
        return os.path.join(self.path, comp.value, "")

    def frame_path(self, comp: Comp, i: int) -> str:
        datapath = self.component_path(comp)
        assert datapath is not None
        return os.path.join(datapath, "{:05d}.npy".format(i))

    def _read_frame(self, comp: Comp, i: int) -> NDArray:
        filepath = self.frame_path(comp, i)
        return np.load(filepath)

    def get_component_frame(self, comp: Comp, i: int) -> NDArray:
        "Return one frame of the selected component of the field."
        if comp not in COMPONENTS:
            raise ValueError("invalid component: {}".format(comp))

        if comp in self.zero_comps:
            return np.zeros((self.mesh.nr, self.mesh.nz), dtype="complex")

        if self.is_loaded[comp]:
            # field values are already loaded in the memory
            return super(FieldFromSaved, self).get_component_frame(comp, i)

        # field values have to be loaded from drive
        return self._read_frame(comp, i)

    def get_component(self, comp: Comp) -> NDArray:
        "Return the selected component (e_r, e_phi, e_z, b_r, b_phi or b_z) of the field."

        if comp in self.zero_comps:
            return np.zeros(
                (self.nframes, self.mesh.nr, self.mesh.nz), dtype="complex",
            )

        elif self.is_loaded[comp]:
            return super(FieldFromSaved, self).get_component(comp)

        # load all field values
        return np.array(
            [self.get_component_frame(comp, i) for i in range(self.nframes)]
        )

    def load_component(self, component: Comp) -> None:

        if self.is_loaded[component]:
            return

        if component in self.zero_comps:
            warnings.warn(f"{component} is always 0; no need to load")
            return

        val = self.get_component(component)
        self._set_component(component, val)
        self.is_loaded[component] = True

    def unload_component(self, component: Comp) -> None:
        if component in self.zero_comps:
            return
        self.is_loaded[component] = False
        self._set_component(component, None)

    def load_field(self) -> None:
        "Load all field values from the disc to memory."
        for comp in self.nonzero_comps:
            self.load_component(comp)

    def unload_field(self) -> None:
        "Delete the loaded field from memory, and start reading from the disc."
        for comp in self.nonzero_comps:
            self.unload_component(comp)


def normalize_compval(fieldvec: FieldVector, norm: Norm, comp: Optional[Comp] = None):
    if comp is not None:
        _, maxval = fieldvec.get_component_limitvals(comp, norm)
    else:
        _, maxval = fieldvec.get_limitvals(norm)
    return fieldvec / maxval


def set_energy(
    fieldvec: FieldVector,
    energy: float,
    ref_frame_i: Optional[int] = None,
    time_avg: bool = True,
    exclude_optax: bool = True,
):
    if isinstance(fieldvec, DenormalizedFieldVector):
        raise ValueError(
            "Energy must be set in normalized unit system! Set the"
            "normalized (reduced) energy first, and call denormalize afterwards."
        )
    if ref_frame_i is None:
        ref_frame_i = fieldvec.get_t_index(t=0)
    ref_energy = fieldvec.get_frame_energy(
        ref_frame_i, time_avg=time_avg, exclude_optax=exclude_optax
    )
    print(np.sqrt(ref_energy / energy))
    return fieldvec / np.sqrt(ref_energy / energy)


def frame_intensity(
    fieldvec: FieldVector,
    i: int,
    which: Literal["e", "b"] = "e",
    time_avg: bool = True,
) -> NDArray:
    "return the total intensity at the frame; type can be 'momentary' or 'avg', comp can be 'e', 'b' or'energy'"
    (er, ep, ez), (br, bp, bz) = fieldvec.get_frame(i)
    if time_avg:
        norm = np.real
    else:
        norm = np.abs

    if which == "e":
        return norm(er) ** 2 + norm(ep) ** 2 + norm(ez) ** 2
    elif which == "b":
        return norm(br) ** 2 + norm(bp) ** 2 + norm(bz) ** 2


def frame_energy_density(
    fieldvec: FieldVector, i: int, time_avg: bool = True
) -> NDArray:
    eint = frame_intensity(fieldvec, i, which="e", time_avg=time_avg)
    bint = frame_intensity(fieldvec, i, which="b", time_avg=time_avg)
    edensity = eint + C_LIGHT ** 2 * bint
    # edensity = 0.5 * EPSILON_0 * (eint + C_LIGHT ** 2 * bint)
    return edensity


def frame_energy(
    fieldvec: FieldVector, i: int, time_avg: bool = True, exclude_optax=False
) -> float:

    rpoints = fieldvec.r
    zpoints = fieldvec.z

    edensity = fieldvec.get_energy_density_frame(i, time_avg)
    r, _ = np.meshgrid(rpoints, zpoints, indexing="ij")
    if exclude_optax:
        edensity = np.ma.masked_where(r == 0, edensity)
    en_r = intg.simps(edensity * r, x=rpoints, axis=0)
    energy = intg.simps(en_r, x=zpoints)
    return energy


def frame_poynting(fieldvec: FieldVector, i: int):
    evec, bvec = fieldvec.get_frame(i)
    evec = np.array(evec)
    bvec = np.array(bvec)
    s = np.cross(evec, bvec, axis=0)
    return s
