#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
from enum import Enum
import sys
import os
import json
from copy import copy
from dataclasses import asdict, make_dataclass
from functools import wraps
from typing import Any, Callable, Dict, List, Optional, Type, TypeVar, Union, overload

import numpy as np
import termcolor as tc
from nptyping import NDArray
from pycallgraph2 import Config, GlobbingFilter, PyCallGraph
from pycallgraph2.output import GraphvizOutput

from . import C_LIGHT, NORMALIZED, Vectorized, ArrayLike, FieldCompFunction

T = TypeVar("T")


def query_yes_no(question: str, default="yes") -> bool:
    """
    Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".

    Source: https://code.activestate.com/recipes/577058/
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")


def is_array_like(a: Any) -> bool:
    """Deterimne if a is an array-like object; that is,
    return isinstance(a, (np.ndarray, collections.sequence))

    Args:
        a (any): object to check

    Returns:
        bool: whether a is array-like
    """
    return isinstance(a, (collections.Sequence, np.ndarray))


def round_relative(a: NDArray, decimals: int) -> NDArray:
    """Round :param:`a` to the given relative (decimal) accuracy.

    If a.max is (x)E-(y), and decimals is d, then the array is rounded up
    to y + d digits.


    Args:
        a (np.ndarray): array to round
        decimals (int): number of decimals relative to a.max

    Returns:
        np.ndarray: a rounded to int(-log10(a.max) + decimals) digits
    """
    absmax = np.amax(np.abs(a))
    ndigits = int(-np.log10(absmax) + decimals)
    return a.round(ndigits)


def mult_along_axis(A: NDArray, B: NDArray, axis: int) -> NDArray:
    """
    Multiply A with B along the specified axis.

    B.size must be the same as A.shape[axis].

    Args:
        A (np.ndarray): array 1
        B (np.ndarray): multiplier array
        axis (int): the axis along which A will be multiplied

    Raises:
        ValueError: if B.size != A.shape[axis]

    Returns:
        np.ndarray: A mutiplied with B along the specified axis.
    """
    # check if B is a single number
    try:
        return A * np.asscalar(np.array(B))
    except:
        pass

    A = np.array(A)
    B = np.array(B)
    # shape check
    if A.shape[axis] != B.size:
        raise ValueError(
            f"'A' and 'B' must have the same length along the given axis, but they have {A.shape[axis]} and {B.shape[axis]}"
        )
    shape = np.swapaxes(A, A.ndim - 1, axis).shape
    B_brc = np.broadcast_to(B, shape)
    B_brc = np.swapaxes(B_brc, A.ndim - 1, axis)
    return A * B_brc


def meshgrid(*xi, copy=True, sparse=False, indexing="ij"):
    "same as Numpy's linspace, but without the strange indexing: indexing defaults to 'ij'."
    return np.meshgrid(*xi, copy=copy, sparse=sparse, indexing=indexing)


@overload
def to_serializable(a: Enum) -> Any:
    ...


@overload
def to_serializable(a: NDArray) -> list:
    ...


@overload
def to_serializable(a: T) -> T:
    ...


def to_serializable(a):
    """If a is an np.ndarray, convert it to list, else return untouched"

    This helps in cases when :param:a nees to be a pickleable data, to e.g. pass
    to jsonstr.

    Args:
        a: item to pass through

    Returns:
        list(a) if a is a Numpy array, else a untouched.
    """

    if isinstance(a, collections.Sequence) and not isinstance(a, str):
        a = [to_serializable(x) for x in a]  # yay recursion
    if isinstance(a, np.integer):
        return int(a)
    if isinstance(a, np.floating):
        return float(a)
    if isinstance(a, Enum):
        return to_serializable(a.value)
    return a


def vectorize_fieldcomp(
    comp_func: Callable[[float, float, float, float, Any], float]
) -> FieldCompFunction:
    """

    Decorator that makes any function with the expected field component function
    signature fieldcomp(r, phi, z, omega_or_t, params) vectorized in r and z.

    Args:
        comp_func (function): field component function with signature

    Returns:
        function: comp_func vectorized up to 2d in the first and third parameter.

    Todo:
        Use np.vectorize instead of own implementation.
    """

    @wraps(comp_func)
    def comp_func_v(
        r: Vectorized, phi: float, z: Vectorized, omega_or_t: float, params: Type
    ) -> Vectorized:
        if is_array_like(r):
            ra = np.array(r)
            za = np.array(z)
            rsize, zsize = ra.shape
            # print(xsize, ysize)
            res = np.empty((rsize, zsize), dtype="complex")
            for i in range(rsize):
                for j in range(zsize):
                    rr, zz = ra[i, j], za[i, j]
                    a = comp_func(rr, phi, zz, omega_or_t, params)
                    res[i, j] = a
            return res
        else:
            assert isinstance(r, (int, float)) and isinstance(z, (int, float))
            return comp_func(r, phi, z, omega_or_t, params)

    return comp_func_v


def dict_to_dataclass(d: Dict[str, Any]) -> Type:
    """Convert the dictionary as a simple dataclass with the keys as fields and
    values as field values. d["key"] will become dict_to_dataclass(d).key.
    """
    Dclass = make_dataclass("Parameters", list(d.keys()))
    return Dclass(**d)


def dataclass_to_json_dict(
    dclass: Any,
    blacklist: Optional[List[str]] = None,
    whitelist: Optional[List[str]] = None,
) -> dict:
    """Pickle the daataclass into a dictionary suitable for json parsing
    according to the dataclass's fields.

    Args:
        dclass (type): dataclass to be parsed to dictionary. The dataclass fields
            should be pickleable or np.ndarray.
        blacklist (list of str, optional): Exclude fields with these names.
            Defaults to None.
        whitelist (list of str, optional): Only include fields with these names.
            Defaults to None.
        sort_keys (bool, optional): Whether to sort the keys in alphabetical
            order instead of the order provided by the dataclass. Defaults to True.
        indent (int, optional): Amoundt of indentation in the . Defaults to 4.

    Returns:
        [type]: [description]
    """
    dct = asdict(dclass)
    if whitelist is not None:
        d = dict((key, dct.pop(key)) for key in whitelist)
        dct = d
    if blacklist is not None:
        for delkey in blacklist:
            dct.pop(delkey, "")

    for key in dct.keys():
        dct[key] = to_serializable(dct[key])

    return dct


def dataclass_to_json_str(
    dclass: Any,
    blacklist: Optional[List[str]] = None,
    whitelist: Optional[List[str]] = None,
    sort_keys: bool = True,
    indent: int = 4,
) -> str:
    """Convert the dataclass to a json string.

    Args:
        dclass (type): dataclass to be parsed to dictionary. The dataclass fields
            should be pickleable or np.ndarray.
        blacklist (list of str, optional): Exclude fields with these names.
            Defaults to None.
        whitelist (list of str, optional): Only include fields with these names.
            Defaults to None.
        sort_keys (bool, optional): Whether to sort the keys in alphabetical
            order instead of the order provided by the dataclass. Defaults to True.
        indent (int, optional): Amoundt of indentation in the string. Defaults to 4.

    Returns:
        str: json string containing the dataclass fields.
    """

    dct = dataclass_to_json_dict(dclass, blacklist, whitelist)
    return json.dumps(dct, sort_keys=sort_keys, indent=indent)


def dataclass_to_json_file(
    dclass: Any,
    fpath=str,
    blacklist: Optional[List[str]] = None,
    whitelist: Optional[List[str]] = None,
    sort_keys: bool = True,
    indent: int = 4,
) -> None:
    """Save the dataclass to a json file.

    Args:
        dclass (type): dataclass to be parsed to dictionary. The dataclass fields
            should be pickleable or np.ndarray.
        fpath (str): Path to the resulting file.
        blacklist (list of str, optional): Exclude fields with these names.
            Defaults to None.
        whitelist (list of str, optional): Only include fields with these names.
            Defaults to None.
        sort_keys (bool, optional): Whether to sort the keys in alphabetical
            order instead of the order provided by the dataclass. Defaults to True.
        indent (int, optional): Amoundt of indentation in the string. Defaults to 4.

    """
    jsonstr = dataclass_to_json_str(dclass, blacklist, whitelist, sort_keys, indent)
    with open(fpath, "w+") as f:
        print(jsonstr, file=f)


def dict_to_pretty_str(
    dct: Dict,
    blacklist: Optional[List[str]] = None,
    whitelist: Optional[List[str]] = None,
    linestart: str = "",
    header: Optional[str] = None,
    colored: bool = False,
    headercolor: Optional[str] = "blue",
    namecolor: Optional[str] = "green",
    valcolor: Optional[str] = None,
) -> str:
    """Convert a dictionary to a formatted, and possibly colored, string as

    header (if given)
    [linestart]str(key1): str(value1)
    [linestart]str(key2): str(value2)
    ...

    Args:
        dct (dict): dictionary to prettyprint
        blacklist (, optional): Keys to exclude. Defaults to None.
        whitelist (, optional): Keys to include, or None to include all. Defaults to None.
        linestart (str, optional): Beginning of each line. Defaults to "".
        header: First line of the returned string, if given. Defaults to None.
        colored (bool, optional): Whether to color the output according to
            :param:`namecolor` and :param:`valcolor` If True, use `utils.cprint`
            instead of print to translate . Defaults to False.
        headercolor (str, optional): Color of the header. Defaults to "blue".
        namecolor (str, optional): Color of the keys. Defaults to "green".
        valcolor ([type], optional): Color of the field values. Defaults to None.
    """
    # assert type(dict) is dict
    dct = copy(dct)
    if whitelist is not None:
        d = dict((key, dct.pop(key)) for key in whitelist)
        dct = d
    if blacklist is not None:
        for delkey in blacklist:
            dct.pop(delkey, "")

    separator = ": "
    lineend = "\n"
    printstr = ""
    if colored:
        if header is not None:
            printstr += tc.colored(header, headercolor, attrs=["underline"]) + lineend
        for key in dct.keys():
            printstr += (
                linestart
                + tc.colored(str(key) + separator, namecolor)
                + tc.colored(str(dct[key]), valcolor)
                + lineend
            )
    else:
        if header is not None:
            printstr += header + lineend
        for key in dct.keys():
            printstr += linestart + key + separator + str(dct[key]) + lineend

    return printstr[: -len(lineend)]  # remove last newline


def dataclass_to_pretty_str(dataclass: Any, **kwargs) -> str:
    "Convert the dataclass to dictionary and pass to `dict_to_pretty_str` with the supplied arguments."
    return dict_to_pretty_str(asdict(dataclass), **kwargs)


# TODO are these two neccessary here?
def prettyprint_dict(dct: Any, **kwargs):
    "Pretty print the dictionary using `dataclass_to_pretty_str`."
    print(dataclass_to_pretty_str(dct, **kwargs))


def prettyprint_dataclass(dclass: Any, **kwargs):
    "Pretty print the dataclass's fields using `dataclass_to_pretty_str`."
    print(dataclass_to_pretty_str(dclass, **kwargs))


# TODO
def nozeros(v: Vectorized, val: float = 1e-100) -> Vectorized:
    "replace zeros with val (to avoid division by zeros)."
    if is_array_like(v):
        a = np.array(v)
        a[a == 0] = val
        return a
    else:
        return val if v == 0 else v


# ~~~~~ debug stuff ~~~~~


def generate_pycallgraph(
    func: Callable,
    *args,
    ofile="pycallgraph.png",
    depth=10,
    excluded=["_find_and_load.*"],
) -> None:
    """Profile the supplied function and create a Pycallgraph image to visualize the result.

    Args:
        func (function): function to benchmark.
        ofile (str, optional): Path ho output file. Defaults to "pycallgraph.png".
        depth (int, optional): Defaults to 10.
        excluded (list, optional): Defaults to ["_find_and_load.*"].
    """
    config = Config(max_depth=depth)
    config.trace_filter = GlobbingFilter(exclude=excluded)
    graphviz = GraphvizOutput(output_file=ofile)
    with PyCallGraph(graphviz):
        func(*args)
