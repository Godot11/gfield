#!/opt/anaconda3/bin/python
# -*- coding: utf-8 -*-

"""
Python wrapper for the RWFields c library
"""
from __future__ import annotations

import collections
import ctypes
from enum import IntEnum
import os
import warnings

import numpy as np
from scipy import constants as const
from scipy import fftpack as fft

from .. import C_LIGHT, Comp, utils  # import fft, ifft, fftfreq, next_fast_len

USE_CVECT = False
WARN_SUBDS_ERROR = True
WARN_ROUNDOFF_ERROR = True

LIB_PATH = "RWFields/rwfield.so"


# Component IDs used in the C code
# COMP_ID = {"pu_e_r": 0, "pu_e_z": 1, "pu_e_r_nrif": 2, "pu_e_z_nrif": 3}

# open the shared C library
c_lib = ctypes.CDLL(os.path.join(os.path.dirname(__file__), LIB_PATH))


class C_COMPFLAG(IntEnum):
    R = 1
    PHI = 2
    Z = 3

    @staticmethod
    def comp(comp: Comp):
        if comp is Comp.E_r:
            return C_COMPFLAG.R
        elif comp is Comp.E_phi:
            return C_COMPFLAG.PHI
        elif comp is Comp.E_z:
            return C_COMPFLAG.Z


class C_RFLAG(IntEnum):
    PU = 1
    PU_FIX = 2
    LG = 3
    FLAT = 4
    LEVY = 5
    RING = 6


# R_FLAG_PU = 1
# R_FLAG_PU_FIX = 2
# R_FLAG_LG = 3
# R_FLAG_FLAT = 4

# ~~ Argument types of the wrapped C functions ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


class C_PARAMS(ctypes.Structure):
    "Class cloning the struct `cmp_vars` in the c code."
    _fields_ = [
        ("n1", ctypes.c_double),
        ("na", ctypes.c_double),
        ("beta", ctypes.c_double),
        ("p", ctypes.c_int),
        ("m", ctypes.c_int),
        ("R_func_flag", ctypes.c_int),
        # ("d", ctypes.c_double),
        # ("omega", ctypes.c_double),
        # ("omega0", ctypes.c_double),
        # ("pulse_duration", ctypes.c_double),
        ("c_light", ctypes.c_double),
    ]


cint = ctypes.c_int
cdouble = ctypes.c_double
cdouble_ptr = ctypes.POINTER(ctypes.c_double)
cparams = C_PARAMS


k0_args = (cdouble, cparams)
k1_args = (cdouble, cparams)
apodization_args = (cdouble,)
Q_func_wrapper_args = (
    cdouble_ptr,
    cdouble_ptr,
    cdouble,
    cdouble,
    cdouble,
    cdouble,
    cdouble,
    cdouble,
    cparams,
    cint,
)
R_func_args = (cdouble, cdouble, cparams)
integrand_wrapper_args = (
    cdouble_ptr,
    cdouble_ptr,
    cdouble,
    cdouble,
    cdouble,
    cdouble,
    cdouble,
    cparams,
    cint,
)
rw_integral_wrapper_args = (
    cdouble_ptr,
    cdouble_ptr,
    cint,
    cdouble,
    cdouble,
    cdouble,
    cdouble,
    cparams,
    cint,
    cint,
)
rw_integral_wrapper_rz_array_args = (
    cdouble_ptr,
    cdouble_ptr,
    cint,
    cint,
    cdouble_ptr,
    cdouble,
    cdouble_ptr,
    cdouble,
    cparams,
    cint,
    cint,
)


# argument types of the integrand wrappers       # Name in the C definition:
# intwrapper_argtypes = (
#     ctypes.POINTER(ctypes.c_float),  # float* re_res
#     ctypes.POINTER(ctypes.c_float),  # float* im_res
#     ctypes.c_double,  # double theta1
#     C_COMP_VARS,  # cmp_vars vars
# )

c_lib.k0.argtypes = k0_args
c_lib.k0.restype = cdouble
c_lib.k1.argtypes = k1_args
c_lib.k1.restype = cdouble
c_lib.apodization.argtypes = apodization_args
c_lib.apodization.restype = cdouble
c_lib.Q_func_wrapper.argtypes = Q_func_wrapper_args
c_lib.R_func.argtypes = R_func_args
c_lib.R_func.restype = cdouble
c_lib.integrand_wrapper.argtypes = integrand_wrapper_args
c_lib.rw_integral_wrapper.argtypes = rw_integral_wrapper_args
c_lib.rw_integral_wrapper_rz_array.argtypes = rw_integral_wrapper_rz_array_args


# ~~ The wrappers ~~ #
# ~~~~~~~~~~~~~~~~~~~~#


def k0(omega, pms):
    return c_lib.k0(cdouble(omega), pms.c_pms)


def k1(omega, pms):
    return c_lib.k1(cdouble(omega), pms.c_pms)


def apodization(theta):
    return c_lib.apodization(cdouble(theta))


def Q_func(theta, r, phi, omega, pms, component):
    comp = C_COMPFLAG.comp(component)
    res = complex(0.0 + 0.0j)
    res_re = ctypes.c_double(res.real)
    res_im = ctypes.c_double(res.imag)
    c_lib.Q_func_wrapper(
        res_re, res_im, theta, r, phi, omega, pms.c_pms, comp,
    )
    return res


def R_func(theta, omega, pms):
    res = c_lib.R_func(theta, omega, pms.c_pms)
    return res


def integrand(theta, r, phi, z, omega, pms, component):
    comp = C_COMPFLAG.comp(component)
    res = complex(0.0 + 0.0j)
    res_re = ctypes.c_double(res.real)
    res_im = ctypes.c_double(res.imag)
    c_lib.integrand_wrapper(res_re, res_im, theta, r, phi, z, omega, pms.c_pms, comp)
    return complex(res_re.value, res_im.value)
    print(f"python integrand: res={res}, res_re={res_re}, res_im={res_im}")


def rw_integral(component, r, phi, z, omega, pms, warn_subdvs, warn_roundoff):
    res = complex(0.0 + 0.0j)
    res_re = ctypes.c_double(res.real)
    res_im = ctypes.c_double(res.imag)
    c_lib.rw_integral_wrapper(
        res_re,
        res_im,
        component,
        r,
        phi,
        z,
        omega,
        pms.c_pms,
        warn_subdvs,
        warn_roundoff,
    )
    return complex(res_re.value, res_im.value)


def rw_integral_rz_array(component, r, phi, z, omega, pms, warn_subdvs, warn_roundoff):
    # if not USE_CVECT:
    #     print("not vectorized")
    #     res = np.empty_like(r, dtype="complex")
    #     rsize, zsize = r.shape
    #     for i in range(rsize):
    #         for j in range(zsize):
    #             res[i, j] = rw_integral(
    #                 component,
    #                 r[i, j],
    #                 phi,
    #                 z[i, j],
    #                 omega,
    #                 pms,
    #                 warn_subdvs,
    #                 warn_roundoff,
    #             )
    #     return res

    # TODO: fix!!!!
    print("vectorized")

    # flatten the arrays so they're 1D, but save the dimensions
    shape = r.shape
    rflat = np.ascontiguousarray(r.flatten(), dtype=np.double)
    zflat = np.ascontiguousarray(z.flatten(), dtype=np.double)
    assert len(rflat) == len(zflat)

    # construct the result arrays
    npoints = len(rflat)
    c_rflat = (ctypes.c_double * npoints)(*rflat)
    c_zflat = (ctypes.c_double * npoints)(*zflat)
    c_re_res = (ctypes.c_double * npoints)()  # (ctypes.c_float * npoints)()
    c_im_res = (ctypes.c_double * npoints)()  # (ctypes.c_float * npoints)()

    c_lib.testfunc.argtypes = (cdouble_ptr, cdouble_ptr, ctypes.c_int)
    c_lib.testfunc(c_rflat, c_re_res, npoints)
    # return np.reshape(c_re_res + 1j * np.array(c_re_res), shape)

    # calculate the field values for the angular freqs and
    c_lib.rw_integral_wrapper_rz_array(
        c_re_res,
        c_im_res,
        npoints,
        component,
        c_rflat,
        phi,
        c_zflat,
        omega,
        pms.c_pms,
        WARN_SUBDS_ERROR,
        WARN_ROUNDOFF_ERROR,
    )
    return np.reshape(np.array(c_re_res) + 1j * np.array(c_im_res), shape)

    # rnum, znum = shape
    # a = []
    # for i in range(rnum):
    #     ai = c_re_res[i * znum : (i + 1) * znum]
    #     a.append(ai)
    # b = []
    # for i in range(rnum):
    #     bi = c_re_res[i * znum : (i + 1) * znum]
    #     b.append(bi)
    # return np.array(a) + 1j * np.array(b)


if USE_CVECT:

    def e_r(r, phi, z, omega, params):
        return rw_integral_rz_array(
            C_COMPFLAG.R,
            r,
            phi,
            z,
            omega,
            params,
            WARN_SUBDS_ERROR,
            WARN_ROUNDOFF_ERROR,
        )

    def e_phi(r, phi, z, omega, params):
        return rw_integral_rz_array(
            C_COMPFLAG.PHI,
            r,
            phi,
            z,
            omega,
            params,
            WARN_SUBDS_ERROR,
            WARN_ROUNDOFF_ERROR,
        )

    def e_z(r, phi, z, omega, params):
        return rw_integral_rz_array(
            C_COMPFLAG.Z,
            r,
            phi,
            z,
            omega,
            params,
            WARN_SUBDS_ERROR,
            WARN_ROUNDOFF_ERROR,
        )


else:

    @utils.vectorize_fieldcomp
    def e_r(r, phi, z, omega, params):
        return rw_integral(
            C_COMPFLAG.R,
            r,
            phi,
            z,
            omega,
            params,
            WARN_SUBDS_ERROR,
            WARN_ROUNDOFF_ERROR,
        )

    @utils.vectorize_fieldcomp
    def e_phi(r, phi, z, omega, params):
        return rw_integral(
            C_COMPFLAG.PHI,
            r,
            phi,
            z,
            omega,
            params,
            WARN_SUBDS_ERROR,
            WARN_ROUNDOFF_ERROR,
        )

    @utils.vectorize_fieldcomp
    def e_z(r, phi, z, omega, params):
        return rw_integral(
            C_COMPFLAG.Z,
            r,
            phi,
            z,
            omega,
            params,
            WARN_SUBDS_ERROR,
            WARN_ROUNDOFF_ERROR,
        )


# def pu_static_component(r, phi, z, omega, params, comp_name):
#     """
#     Wraps :meth:`e_field_component` in integrand_gsl_mpfr.c.

#     Expr. (4) of Pu's article evaluated numerically via QAG algorithm, using
#     41-point Gauss-Kronrod rule, with relative accuracy of 1E-4.
#     """

#     # if utils.is_array_like(omega):
#     #     return pu_static_component_omega_vectorized(r, phi, z, omega, params, comp_name)

#     if True:  # utils.is_array_like(r):
#         return pu_static_component_rz_vectorized(
#             r, phi, z, omega, params, comp_name, use_cvect=False
#         )

#     else:
#         return pu_static_component_nonvectorized(r, phi, z, omega, params, comp_name)


# def e_r_glass(r, phi, z, omega, params):
#     return pu_static_component(r, phi, z, omega, params, "pu_e_r")


# def e_z_glass(r, phi, z, omega, params):
#     return pu_static_component(r, phi, z, omega, params, "pu_e_z")


# def e_r(r, phi, z, omega, params):
#     return pu_static_component(r, phi, z, omega, params, "pu_e_r_nrif")


# def e_z(r, phi, z, omega, params):
#     return pu_static_component(r, phi, z, omega, params, "pu_e_z_nrif")
