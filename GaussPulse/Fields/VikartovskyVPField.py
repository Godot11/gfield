from copy import copy
from math import factorial, pi, sqrt
from sys import exit
from typing import Literal

import matplotlib.pyplot as plt
import numpy as np  # This is used by other modules importing * from here
from mpl_toolkits.mplot3d import Axes3D
from scipy import constants as const
from scipy.special import comb, gamma

from GaussPulse import C_LIGHT, OMEGA_0
from GaussPulse.utils import round_relative, vectorize_fieldcomp

C_SQUARED = C_LIGHT ** 2

NTOL_DECIMALS = 20

# ~~ Module setup


def component(comp, r, phi, z, omega, params):
    funcs = {
        "analytic": {
            "e_r": e_r_anl,
            "e_phi": e_phi_anl,
            "e_z": e_z_anl,
            "b_r": b_r_anl,
            "b_phi": b_phi_anl,
            "b_z": b_z_anl,
        },
        "numerical": {
            "e_r": e_r_num,
            "e_phi": e_phi_num,
            "e_z": ez_nozero_rho_num,  # e_z_curl
        },
        "spectral": {
            "e_r": e_r_sp,
            "e_phi": e_phi_sp,
            "e_z": e_z_sp,
            "b_r": b_r_sp,
            "b_phi": b_phi_sp,
            "b_z": b_z_sp,
        },
    }
    if comp in ("b_r", "b_phi", "b_z") and params.ftype == "numerical":
        return np.zeros_like(r)
    else:
        func = funcs[params.formula_type][comp]
        return func(r, phi, z, omega, params)


def e_r(r, phi, z, omega, params):
    return component("e_r", r, phi, z, omega, params)


def e_phi(r, phi, z, omega, params):
    return component("e_phi", r, phi, z, omega, params)


def e_z(r, phi, z, omega, params):
    return component("e_z", r, phi, z, omega, params)


def b_r(r, phi, z, omega, params):
    return component("b_r", r, phi, z, omega, params)


def b_phi(r, phi, z, omega, params):
    return component("b_phi", r, phi, z, omega, params)


def b_z(r, phi, z, omega, params):
    return component("b_z", r, phi, z, omega, params)


# ~~~~~ MATH FUNCTIONS ~~~~~~~~


def binomial(term1, term2):
    return float(comb(term1, term2, exact=True))
    # Note: scipy.special.binom is approximate, and scipy.special.comb(a,b,exact=True) is exact


def fact(var):
    if int(var) != var:
        print(
            "Not setup for factoials of decimals in fact function.\n...............\n..............."
        )
        exit()
    elif var < 0:
        return 1.0
    else:
        return float(factorial(var))


def Gamma(var):
    return float(gamma(var))


# ~~~~~ EXPRESSIONS ~~~~~~~~


def beta(z, zr):
    "below eq. 1"
    return 1.0 + 1.0j * z / zr


def xi(rho, beta, zr):
    "eq. 2a"
    return rho ** 2 / (2.0 * C_LIGHT * beta * zr)


def bigT(omega0, z, xi, t, s):
    "eq. 2b"
    return 1.0 + omega0 * (-1.0j * z / C_LIGHT + xi + 1.0j * t) / s


def lambdaNM(phi, xi, beta, pms):
    "eq. 2c    Lambda_{n,m}"
    return (
        (-1.0) ** (pms.n + pms.m)
        * 2.0 ** (2 * pms.n + pms.m)
        * sqrt(2 * pi)
        * fact(pms.n)
        * np.exp(1.0j * pms.phi0)
        * xi ** (pms.m / 2.0)
        * beta ** (-pms.n - pms.m / 2.0 - 1)
        * np.exp(1.0j * pms.m * phi)
    )


def G_nmj(n, m, j):
    "eq. 4    G_{n,m,j}"
    return (-1.0) ** j * fact(n + m) / (fact(n - j) * fact(m + j) * fact(j))


def c_Np(n, m, s, omega0, N, p, j):
    "eq. 22    c_{N, p}"

    gamma = m / 2.0 + s + j  # small gamma, in text after eq. 1

    knp = kappa_Np(N, p)
    g = G_nmj(n + p, m, j)
    f = fact(n + p) / fact(n)
    p = omega0 ** N * (s / omega0) ** (s - gamma + float(N))
    gm = Gamma(gamma + 1 - N) / Gamma(s + 1)

    return knp * g * f * p * gm


def kappa_Np(N, p):
    "eq. 23    kappa_{N, p}"
    return (-1.0) ** (p - N) * binomial(2 * N, 2 * N - p) / fact(p - N)


# ~~~~~ ANALYTIC CALCULATIONS ~~~~~~~~


def compute_radpol_comps(rho, phi, z, t, pms):

    zr = pms.zr
    omega0 = pms.omega0
    sReal = pms.sReal
    mmReal = pms.mmReal
    nnReal = pms.nnReal
    epsilonc2 = pms.epsilonc2

    # descartes to cylindrical
    # rho = sqrt(x ** 2 + y ** 2)
    if rho < pms.lambda0 * 1.0e-5:  #!hack fix this bullshit
        rho = pms.lambda0 * 1.0e-5
    # phi = np.arctan2(y, x)  # radians

    _beta = beta(z, zr)
    _xi = xi(rho, _beta, zr)
    _bigT = bigT(omega0, z, _xi, t, sReal)
    _lambda_nm = lambdaNM(phi, _xi, _beta, pms)

    # calculating the time-domain phasor, and its derivatives
    dU_dT = 0.0j
    d2U_dT2 = 0.0j
    dU_dxi = 0.0j
    d2U_dxi2 = 0.0j
    dU_db = 0.0j
    d2U_dxi_dT = 0.0j
    d2U_dxi_db = 0.0j
    d2U_db_dT = 0.0j
    results = np.zeros(
        7, dtype="complex"
    )  # complex values for [phasor,E1,E2,E3,B1,B2,B3]

    for N in range(0, pms.pertOrder + 1):
        for p in range(N, 2 * N + 1):
            term_U = 0.0j
            term_dU_dT = 0.0j
            term_dU_dxi = 0.0j
            term_d2U_dxi2 = 0.0j
            term_d2U_dT2 = 0.0j
            term_d2U_dxi_dT = 0.0j
            a_real = float(N)
            for j in range(0, pms.n + p + 1):
                jReal = float(j)
                gamma = mmReal / 2.0 + sReal + jReal

                cNp = pms.c_Np_from_lookup(N, p, j)
                term_U += cNp * _xi ** j * _bigT ** (-gamma - 1.0 + N)

                # .... these "more efficient" definitions seem to not work .... numerical division issues ???
                # term_dU_dT += term_U * (-gamma - 1.0 + a_real) / bigT
                # term_d2U_dT2 += term_dU_dT * (-gamma - 2.0 + a_real) / bigT
                # term_dU_dxi += term_U * jReal / xi
                # term_d2U_dxi2 += term_dU_dxi * (jReal - 1.0) / xi
                # term_d2U_dxi_dT += term_dU_dT * jReal / xi

                # .... these explicit definitions work better ....
                term_dU_dT += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )
                term_d2U_dT2 += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 3.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                    * (-gamma - 2.0 + a_real)
                )
                term_dU_dxi += (
                    cNp * jReal * _xi ** (j - 1) * _bigT ** (-gamma - 1.0 + a_real)
                )
                term_d2U_dxi2 += (
                    cNp
                    * jReal
                    * (jReal - 1.0)
                    * _xi ** (j - 2)
                    * _bigT ** (-gamma - 1.0 + a_real)
                )
                term_d2U_dxi_dT += (
                    cNp
                    * jReal
                    * _xi ** (j - 1)
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )

            ec2bn = (epsilonc2 / _beta) ** N
            results[0] += term_U * ec2bn
            dU_dT += term_dU_dT * ec2bn
            d2U_dT2 += term_d2U_dT2 * ec2bn
            dU_dxi += term_dU_dxi * ec2bn
            d2U_dxi2 += term_d2U_dxi2 * ec2bn
            dU_db += term_U * (-N * ec2bn / _beta)
            d2U_dxi_dT += term_d2U_dxi_dT * ec2bn
            d2U_dxi_db += term_dU_dxi * (-N * ec2bn / _beta)
            d2U_db_dT += term_dU_dT * (-N * ec2bn / _beta)

    results[0] *= _lambda_nm
    dU_dT *= _lambda_nm
    d2U_dT2 *= _lambda_nm
    dU_dxi *= _lambda_nm
    d2U_dxi2 *= _lambda_nm
    dU_db *= _lambda_nm
    d2U_dxi_dT *= _lambda_nm
    d2U_dxi_db *= _lambda_nm
    d2U_db_dT *= _lambda_nm

    # calculating the time-domain EM fields
    bzr = _beta * zr

    # if polar == 2:  # radial polariation
    # E_rho, c
    results[1] = -(1.0j / rho) * (
        mmReal * ((nnReal + mmReal + 1.0) / bzr) * results[0]
        - (2.0 * omega0 * _xi / (sReal * zr)) * d2U_db_dT
        + (omega0 / sReal)
        * (
            2.0 * _xi * (nnReal + mmReal + 2.0) / bzr
            + mmReal * (_xi / bzr + 1.0 / C_LIGHT)
        )
        * dU_dT
        + (_xi * (2.0 * nnReal + 3.0 * mmReal + 4.0) / bzr) * dU_dxi
        - (mmReal / zr) * dU_db
        + (2.0 * _xi ** 2 / bzr) * d2U_dxi2
        + (2.0 * omega0 * _xi / sReal) * (2.0 * _xi / bzr + 1.0 / C_LIGHT) * d2U_dxi_dT
        - (2.0 * _xi / zr) * d2U_dxi_db
        + (2.0 * omega0 ** 2 * _xi / sReal ** 2) * (_xi / bzr + 1.0 / C_LIGHT) * d2U_dT2
    )

    # E_phi, c
    results[2] = (mmReal / rho) * (
        ((nnReal + mmReal + 1.0) / bzr) * results[0]
        + (omega0 / sReal) * (_xi / bzr + 1.0 / C_LIGHT) * dU_dT
        + (_xi / bzr) * dU_dxi
        - (1.0 / zr) * dU_db
    )

    # E_z, c
    results[3] = (_xi / rho ** 2) * (
        (-4.0 * omega0 / sReal) * (mmReal + 1.0) * dU_dT
        - 4.0 * (mmReal + 1.0) * dU_dxi
        - (4.0 * omega0 ** 2 * _xi / sReal ** 2) * d2U_dT2
        - 4.0 * _xi * d2U_dxi2
        - (8.0 * omega0 * _xi / sReal) * d2U_dxi_dT
    )

    # B_rho, c
    results[4] = -(mmReal * omega0 / (C_SQUARED * sReal * rho)) * dU_dT

    # B_phi
    results[5] = -(1.0j * omega0 / (C_SQUARED * sReal * rho)) * (
        mmReal * dU_dT + 2.0 * _xi * ((omega0 / sReal) * d2U_dT2 + d2U_dxi_dT)
    )
    # B_z = 0 for RP fields
    # results[6]=0.0j # already set as the default value

    # else:
    #     print("Polarization not defined")
    #     exit()

    return results


def compute_phasor(rho, phi, z, t, pms):

    zr = pms.zr
    omega0 = pms.omega0
    sReal = pms.sReal
    mmReal = pms.mmReal
    nnReal = pms.nnReal
    epsilonc2 = pms.epsilonc2

    if rho < pms.lambda0 * 1.0e-5:  #!hack fix this?
        rho = pms.lambda0 * 1.0e-5

    _beta = beta(z, zr)
    _xi = xi(rho, _beta, zr)
    _bigT = bigT(omega0, z, _xi, t, sReal)
    _lambda_nm = lambdaNM(phi, _xi, _beta, pms)

    phasor = 0.0j
    for N in range(0, pms.pertOrder + 1):
        for p in range(N, 2 * N + 1):
            term_U = 0.0j
            for j in range(0, pms.n + p + 1):
                jReal = float(j)
                gamma = mmReal / 2.0 + sReal + jReal

                cNp = pms.c_Np_from_lookup(N, p, j)
                term_U += cNp * _xi ** j * _bigT ** (-gamma - 1.0 + N)

            ec2bn = (epsilonc2 / _beta) ** N
            phasor += term_U * ec2bn

    phasor *= _lambda_nm

    return phasor


@vectorize_fieldcomp
def compute_e_r_analytic(rho, phi, z, t, pms):

    zr = pms.zr
    omega0 = pms.omega0
    sReal = pms.sReal
    mmReal = pms.mmReal
    nnReal = pms.nnReal
    epsilonc2 = pms.epsilonc2

    # descartes to cylindrical
    # rho = sqrt(x ** 2 + y ** 2)
    if rho < pms.lambda0 * 1.0e-5:  #!hack fix this bullshit
        rho = pms.lambda0 * 1.0e-5
    # phi = np.arctan2(y, x)  # radians

    _beta = beta(z, zr)
    _xi = xi(rho, _beta, zr)
    _bigT = bigT(omega0, z, _xi, t, sReal)
    _lambda_nm = lambdaNM(phi, _xi, _beta, pms)

    # calculating the time-domain phasor, and its derivatives
    dU_dT = 0.0j
    d2U_dT2 = 0.0j
    dU_dxi = 0.0j
    d2U_dxi2 = 0.0j
    dU_db = 0.0j
    d2U_dxi_dT = 0.0j
    d2U_dxi_db = 0.0j
    d2U_db_dT = 0.0j
    results = np.zeros(
        7, dtype="complex"
    )  # complex values for [phasor,E1,E2,E3,B1,B2,B3]

    for N in range(0, pms.pertOrder + 1):
        for p in range(N, 2 * N + 1):
            term_U = 0.0j
            term_dU_dT = 0.0j
            term_dU_dxi = 0.0j
            term_d2U_dxi2 = 0.0j
            term_d2U_dT2 = 0.0j
            term_d2U_dxi_dT = 0.0j
            a_real = float(N)
            for j in range(0, pms.n + p + 1):
                jReal = float(j)
                gamma = mmReal / 2.0 + sReal + jReal

                cNp = pms.c_Np_from_lookup(N, p, j)
                term_U += cNp * _xi ** j * _bigT ** (-gamma - 1.0 + N)

                # .... these "more efficient" definitions seem to not work .... numerical division issues ???
                # term_dU_dT += term_U * (-gamma - 1.0 + a_real) / bigT
                # term_d2U_dT2 += term_dU_dT * (-gamma - 2.0 + a_real) / bigT
                # term_dU_dxi += term_U * jReal / xi
                # term_d2U_dxi2 += term_dU_dxi * (jReal - 1.0) / xi
                # term_d2U_dxi_dT += term_dU_dT * jReal / xi

                # .... these explicit definitions work better ....
                term_dU_dT += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )
                term_d2U_dT2 += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 3.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                    * (-gamma - 2.0 + a_real)
                )
                term_dU_dxi += (
                    cNp * jReal * _xi ** (j - 1) * _bigT ** (-gamma - 1.0 + a_real)
                )
                term_d2U_dxi2 += (
                    cNp
                    * jReal
                    * (jReal - 1.0)
                    * _xi ** (j - 2)
                    * _bigT ** (-gamma - 1.0 + a_real)
                )
                term_d2U_dxi_dT += (
                    cNp
                    * jReal
                    * _xi ** (j - 1)
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )

            ec2bn = (epsilonc2 / _beta) ** N
            results[0] += term_U * ec2bn
            dU_dT += term_dU_dT * ec2bn
            d2U_dT2 += term_d2U_dT2 * ec2bn
            dU_dxi += term_dU_dxi * ec2bn
            d2U_dxi2 += term_d2U_dxi2 * ec2bn
            dU_db += term_U * (-N * ec2bn / _beta)
            d2U_dxi_dT += term_d2U_dxi_dT * ec2bn
            d2U_dxi_db += term_dU_dxi * (-N * ec2bn / _beta)
            d2U_db_dT += term_dU_dT * (-N * ec2bn / _beta)

    results[0] *= _lambda_nm
    dU_dT *= _lambda_nm  #
    d2U_dT2 *= _lambda_nm  #
    dU_dxi *= _lambda_nm  #
    d2U_dxi2 *= _lambda_nm  #
    dU_db *= _lambda_nm  #
    d2U_dxi_dT *= _lambda_nm  #
    d2U_dxi_db *= _lambda_nm  #
    d2U_db_dT *= _lambda_nm  #

    # calculating the time-domain EM fields
    bzr = _beta * zr

    # if polar == 2:  # radial polariation
    # E_rho, c
    e_rho = -(1.0j / rho) * (
        mmReal * ((nnReal + mmReal + 1.0) / bzr) * results[0]
        - (2.0 * omega0 * _xi / (sReal * zr)) * d2U_db_dT
        + (omega0 / sReal)
        * (
            2.0 * _xi * (nnReal + mmReal + 2.0) / bzr
            + mmReal * (_xi / bzr + 1.0 / C_LIGHT)
        )
        * dU_dT
        + (_xi * (2.0 * nnReal + 3.0 * mmReal + 4.0) / bzr) * dU_dxi
        - (mmReal / zr) * dU_db
        + (2.0 * _xi ** 2 / bzr) * d2U_dxi2
        + (2.0 * omega0 * _xi / sReal) * (2.0 * _xi / bzr + 1.0 / C_LIGHT) * d2U_dxi_dT
        - (2.0 * _xi / zr) * d2U_dxi_db
        + (2.0 * omega0 ** 2 * _xi / sReal ** 2) * (_xi / bzr + 1.0 / C_LIGHT) * d2U_dT2
    )

    return e_rho


def compute_e_phi_analytic(rho, phi, z, t, pms):

    zr = pms.zr
    omega0 = pms.omega0
    sReal = pms.sReal
    mmReal = pms.mmReal
    nnReal = pms.nnReal
    epsilonc2 = pms.epsilonc2

    # descartes to cylindrical
    # rho = sqrt(x ** 2 + y ** 2)
    if rho < pms.lambda0 * 1.0e-5:  #!hack fix this bullshit
        rho = pms.lambda0 * 1.0e-5
    # phi = np.arctan2(y, x)  # radians

    _beta = beta(z, zr)
    _xi = xi(rho, _beta, zr)
    _bigT = bigT(omega0, z, _xi, t, sReal)
    _lambda_nm = lambdaNM(phi, _xi, _beta, pms)

    # calculating the time-domain phasor, and its derivatives
    dU_dT = 0.0j
    d2U_dT2 = 0.0j
    dU_dxi = 0.0j
    dU_db = 0.0j
    results = np.zeros(
        7, dtype="complex"
    )  # complex values for [phasor,E1,E2,E3,B1,B2,B3]

    for N in range(0, pms.pertOrder + 1):
        for p in range(N, 2 * N + 1):
            term_U = 0.0j
            term_dU_dT = 0.0j
            term_dU_dxi = 0.0j
            term_d2U_dxi_dT = 0.0j
            a_real = float(N)
            for j in range(0, pms.n + p + 1):
                jReal = float(j)
                gamma = mmReal / 2.0 + sReal + jReal

                cNp = pms.c_Np_from_lookup(N, p, j)
                term_U += cNp * _xi ** j * _bigT ** (-gamma - 1.0 + N)
                term_dU_dT += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )

                term_dU_dxi += (
                    cNp * jReal * _xi ** (j - 1) * _bigT ** (-gamma - 1.0 + a_real)
                )
                term_d2U_dxi_dT += (
                    cNp
                    * jReal
                    * _xi ** (j - 1)
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )

            ec2bn = (epsilonc2 / _beta) ** N
            dU_dT += term_dU_dT * ec2bn
            dU_dxi += term_dU_dxi * ec2bn
            dU_db += term_U * (-N * ec2bn / _beta)

    dU_dT *= _lambda_nm  #
    dU_dxi *= _lambda_nm  #
    dU_db *= _lambda_nm  #

    # calculating the time-domain EM fields
    bzr = _beta * zr

    e_phi = (mmReal / rho) * (
        ((nnReal + mmReal + 1.0) / bzr) * results[0]
        + (omega0 / sReal) * (_xi / bzr + 1.0 / C_LIGHT) * dU_dT
        + (_xi / bzr) * dU_dxi
        - (1.0 / zr) * dU_db
    )
    return e_phi


def compute_e_z_analytic(rho, phi, z, t, pms):

    zr = pms.zr
    omega0 = pms.omega0
    sReal = pms.sReal
    mmReal = pms.mmReal
    nnReal = pms.nnReal
    epsilonc2 = pms.epsilonc2

    # descartes to cylindrical
    # rho = sqrt(x ** 2 + y ** 2)
    if rho < pms.lambda0 * 1.0e-5:  #!hack fix this bullshit
        rho = pms.lambda0 * 1.0e-5
    # phi = np.arctan2(y, x)  # radians

    _beta = beta(z, zr)
    _xi = xi(rho, _beta, zr)
    _bigT = bigT(omega0, z, _xi, t, sReal)
    _lambda_nm = lambdaNM(phi, _xi, _beta, pms)

    # calculating the time-domain phasor, and its derivatives
    dU_dT = 0.0j
    d2U_dT2 = 0.0j
    dU_dxi = 0.0j
    d2U_dxi2 = 0.0j
    d2U_dxi_dT = 0.0j

    for N in range(0, pms.pertOrder + 1):
        for p in range(N, 2 * N + 1):
            term_dU_dT = 0.0j
            term_dU_dxi = 0.0j
            term_d2U_dxi2 = 0.0j
            term_d2U_dT2 = 0.0j
            term_d2U_dxi_dT = 0.0j
            a_real = float(N)
            for j in range(0, pms.n + p + 1):
                jReal = float(j)
                gamma = mmReal / 2.0 + sReal + jReal

                cNp = pms.c_Np_from_lookup(N, p, j)

                term_dU_dT += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )
                term_d2U_dT2 += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 3.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                    * (-gamma - 2.0 + a_real)
                )
                term_dU_dxi += (
                    cNp * jReal * _xi ** (j - 1) * _bigT ** (-gamma - 1.0 + a_real)
                )
                term_d2U_dxi2 += (
                    cNp
                    * jReal
                    * (jReal - 1.0)
                    * _xi ** (j - 2)
                    * _bigT ** (-gamma - 1.0 + a_real)
                )
                term_d2U_dxi_dT += (
                    cNp
                    * jReal
                    * _xi ** (j - 1)
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )

            ec2bn = (epsilonc2 / _beta) ** N
            dU_dT += term_dU_dT * ec2bn
            d2U_dT2 += term_d2U_dT2 * ec2bn
            dU_dxi += term_dU_dxi * ec2bn
            d2U_dxi2 += term_d2U_dxi2 * ec2bn
            d2U_dxi_dT += term_d2U_dxi_dT * ec2bn

    dU_dT *= _lambda_nm  #
    d2U_dT2 *= _lambda_nm  #
    dU_dxi *= _lambda_nm  #
    d2U_dxi2 *= _lambda_nm  #
    d2U_dxi_dT *= _lambda_nm  #

    e_z = (_xi / rho ** 2) * (
        (-4.0 * omega0 / sReal) * (mmReal + 1.0) * dU_dT
        - 4.0 * (mmReal + 1.0) * dU_dxi
        - (4.0 * omega0 ** 2 * _xi / sReal ** 2) * d2U_dT2
        - 4.0 * _xi * d2U_dxi2
        - (8.0 * omega0 * _xi / sReal) * d2U_dxi_dT
    )

    return e_z


def compute_b_r_analytic(rho, phi, z, t, pms):

    zr = pms.zr
    omega0 = pms.omega0
    sReal = pms.sReal
    mmReal = pms.mmReal
    nnReal = pms.nnReal
    epsilonc2 = pms.epsilonc2

    # descartes to cylindrical
    # rho = sqrt(x ** 2 + y ** 2)
    if rho < pms.lambda0 * 1.0e-5:  #!hack fix this bullshit
        rho = pms.lambda0 * 1.0e-5
    # phi = np.arctan2(y, x)  # radians

    _beta = beta(z, zr)
    _xi = xi(rho, _beta, zr)
    _bigT = bigT(omega0, z, _xi, t, sReal)
    _lambda_nm = lambdaNM(phi, _xi, _beta, pms)

    # calculating the time-domain phasor, and its derivatives
    dU_dT = 0.0j
    for N in range(0, pms.pertOrder + 1):
        for p in range(N, 2 * N + 1):
            term_dU_dT = 0.0j
            a_real = float(N)
            for j in range(0, pms.n + p + 1):
                jReal = float(j)
                gamma = mmReal / 2.0 + sReal + jReal

                cNp = pms.c_Np_from_lookup(N, p, j)

                # .... these explicit definitions work better ....
                term_dU_dT += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )

            ec2bn = (epsilonc2 / _beta) ** N
            dU_dT += term_dU_dT * ec2bn

    dU_dT *= _lambda_nm
    # calculating the time-domain EM fields

    # B_rho, c
    b_rho = -(mmReal * omega0 / (C_SQUARED * sReal * rho)) * dU_dT

    return b_rho


def compute_b_phi_analytic(rho, phi, z, t, pms):

    zr = pms.zr
    omega0 = pms.omega0
    sReal = pms.sReal
    mmReal = pms.mmReal
    nnReal = pms.nnReal
    epsilonc2 = pms.epsilonc2

    # descartes to cylindrical
    # rho = sqrt(x ** 2 + y ** 2)
    if rho < pms.lambda0 * 1.0e-5:  #!hack fix this bullshit
        rho = pms.lambda0 * 1.0e-5
    # phi = np.arctan2(y, x)  # radians

    _beta = beta(z, zr)
    _xi = xi(rho, _beta, zr)
    _bigT = bigT(omega0, z, _xi, t, sReal)
    _lambda_nm = lambdaNM(phi, _xi, _beta, pms)

    # calculating the time-domain phasor, and its derivatives
    dU_dT = 0.0j
    d2U_dT2 = 0.0j
    d2U_dxi_dT = 0.0j
    for N in range(0, pms.pertOrder + 1):
        for p in range(N, 2 * N + 1):
            term_dU_dT = 0.0j
            term_d2U_dT2 = 0.0j
            term_d2U_dxi_dT = 0.0j
            a_real = float(N)
            for j in range(0, pms.n + p + 1):
                jReal = float(j)
                gamma = mmReal / 2.0 + sReal + jReal

                cNp = pms.c_Np_from_lookup(N, p, j)

                term_dU_dT += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )
                term_d2U_dT2 += (
                    cNp
                    * _xi ** j
                    * _bigT ** (-gamma - 3.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                    * (-gamma - 2.0 + a_real)
                )
                term_d2U_dxi_dT += (
                    cNp
                    * jReal
                    * _xi ** (j - 1)
                    * _bigT ** (-gamma - 2.0 + a_real)
                    * (-gamma - 1.0 + a_real)
                )

            ec2bn = (epsilonc2 / _beta) ** N
            dU_dT += term_dU_dT * ec2bn
            d2U_dT2 += term_d2U_dT2 * ec2bn
            d2U_dxi_dT += term_d2U_dxi_dT * ec2bn

    dU_dT *= _lambda_nm
    d2U_dT2 *= _lambda_nm
    d2U_dxi_dT *= _lambda_nm

    b_phi = -(1.0j * omega0 / (C_SQUARED * sReal * rho)) * (
        mmReal * dU_dT + 2.0 * _xi * ((omega0 / sReal) * d2U_dT2 + d2U_dxi_dT)
    )

    return b_phi


def compute_b_z_analytic(rho, phi, z, t, pms):
    return 0.0j


def compute_phasor_omega(rho, phi, z, omega, pms):
    "the freq domain phasor w/o the poissonian multiplication"
    if rho < 1.0e-5:  #!hack fix this bullshit
        rho = 1.0e-5

    n, m, zr = pms.m, pms.n, pms.zr

    _beta = beta(z, zr)
    _xi = xi(rho, _beta, zr)
    # _bigT = bigT(pms.omega0, z, _xi, t, pms.s)
    _lambda_nm = lambdaNM(phi, _xi, _beta, pms)

    ccoeff = (
        _lambda_nm
        / (np.sqrt(2 * np.pi) * fact(n))
        * np.exp(1j * omega * z / C_LIGHT)
        * omega ** (pms.m / 2)
        * np.exp(-_xi * omega)
    )

    # calculating the time-domain phasor, and its derivatives
    phasor = 0.0j
    for N in range(0, pms.pertOrder + 1):
        a = (pms.omega0 / omega) ** N
        U_2n = 0.0j
        for p in range(N, 2 * N + 1):
            kappa = kappa_Np(N, p)
            for j in range(0, pms.n + p + 1):
                coeff = (
                    kappa * fact(pms.n + p) * G_nmj(n + p, m, j) * (_xi * omega) ** j
                )
                U_2n += coeff

        ec2ba = (pms.epsilonc2 / _beta) ** N
        phasor += U_2n * ec2ba

    phasor *= ccoeff

    return phasor


# ~~~~~ todo: remove these ~~~~~~~~


def get_field_comp(comp, rho, phi, z, t, pms):
    indexes = {
        "phasor": 0,
        "e_r": 1,
        "e_phi": 2,
        "e_z": 3,
        "b_r": 4,
        "b_phi": 5,
        "b_z": 6,
    }
    ceb = compute_radpol_comps(rho, phi, z, t, pms)
    i = indexes[comp]
    return ceb[i]


@vectorize_fieldcomp
def phasor(r, phi, z, t, params):
    return compute_phasor(r, phi, z, t, params)


@vectorize_fieldcomp
def phasor_spectral(r, phi, z, omega, params):
    return compute_phasor_omega(r, phi, z, omega, params)


@vectorize_fieldcomp
def phasor_orig(r, phi, z, t, params):
    return get_field_comp("phasor", r, phi, z, t, params)


@vectorize_fieldcomp
def e_r_anl(r, phi, z, t, params):
    return get_field_comp("e_r", r, phi, z, t, params)


@vectorize_fieldcomp
def e_phi_anl(r, phi, z, t, params):
    return get_field_comp("e_phi", r, phi, z, t, params)


@vectorize_fieldcomp
def e_z_anl(r, phi, z, t, params):
    return get_field_comp("e_z", r, phi, z, t, params)


@vectorize_fieldcomp
def b_r_anl(r, phi, z, t, params):
    return get_field_comp("b_r", r, phi, z, t, params)


@vectorize_fieldcomp
def b_phi_anl(r, phi, z, t, params):
    return get_field_comp("b_phi", r, phi, z, t, params)


@vectorize_fieldcomp
def b_z_anl(r, phi, z, t, params):
    return get_field_comp("b_z", r, phi, z, t, params)


# ~~~~~ CALCULATIONS WITH NUMERICAL NABLA ~~~~~~~~

TOLDIGITS = 5


def e_r_num(rho, phi, z, t, pms):
    u = phasor(rho, phi, z, t, pms)
    rho_ax, z_ax = 0, 1
    rhos = rho[:, 0].transpose()
    zs = z[0]
    drho_u = np.gradient(u, rhos, axis=rho_ax, edge_order=2)
    dz_drho_u = np.gradient(drho_u, zs, axis=z_ax, edge_order=2)
    e_r = round_relative(dz_drho_u, TOLDIGITS)
    return e_r


def e_phi_num(rho, phi, z, t, pms):
    if pms.m == 0:
        return np.zeros_like(rho, dtype="complex")
    u = phasor(rho, phi, z, t, pms)
    z_ax = 1
    zs = z[0]
    dz_u = np.gradient(u, zs, axis=z_ax, edge_order=2)
    e_phi = 1j * pms.m / (rho + 1e-100) * round_relative(dz_u, TOLDIGITS)
    return e_phi


def e_z_num(rho, phi, z, t, pms):
    u = phasor(rho, phi, z, t, pms)
    rho_ax = 0
    rs = rho[:, 0].transpose()
    drho_u = np.gradient(u, rs, axis=rho_ax, edge_order=2)
    drho_rho_u = np.gradient(rho * drho_u, rs, axis=rho_ax, edge_order=2)
    e_z = 1j * pms.m * round_relative(drho_u, TOLDIGITS) - 1 / (
        rho + 1e-100
    ) * round_relative(drho_rho_u, TOLDIGITS)
    return e_z


# def e_z_curl(rho, phi, z, t, pms):
#     u = phasor(rho, phi, z, t, pms)
#     rho_ax = 0
#     rs = rho[:, 0].transpose()
#     du_drho = np.gradient(u, rs, axis=rho_ax, edge_order=2)
#     d2u_drho2 = np.gradient(du_drho, rs, axis=rho_ax, edge_order=2)
#     e_z = (
#         1
#         / (rho + pms.lambda0 * 1e-10)
#         * (du_drho + rho * d2u_drho2 - pms.m ** 2 / (rho + pms.lambda0 * 1e-10) * u)
#     )  #! << validate
#     return e_z


# ~~~~~ CALCULATIONS WITH SPECTRAL PHASOR (NUMERICAL NABLA) ~~~~~~~~


def e_r_sp(rho, phi, z, omega, pms):
    u = phasor_spectral(rho, phi, z, omega, pms)
    rho_ax, z_ax = 0, 1
    rhos = rho[:, 0].transpose()
    zs = z[0]
    drho_u = np.gradient(u, rhos, axis=rho_ax, edge_order=0)
    dz_drho_u = np.gradient(drho_u, zs, axis=z_ax, edge_order=0)

    # e_r = round_relative(dz_drho_u, TOLDIGITS)
    return dz_drho_u  # e_r


def e_phi_sp(rho, phi, z, omega, pms):
    if pms.m == 0:
        return np.zeros_like(rho, dtype="complex")
    u = phasor_spectral(rho, phi, z, omega, pms)
    z_ax = 1
    zs = z[0]
    dz_u = np.gradient(u, zs, axis=z_ax, edge_order=2)
    e_phi = 1j * pms.m / (rho + 1e-100) * round_relative(dz_u, TOLDIGITS)
    return e_phi


def e_z_sp(rho, phi, z, omega, pms):
    u = phasor_spectral(rho, phi, z, omega, pms)
    rho_ax = 0
    rs = rho[:, 0].transpose()
    drho_u = np.gradient(u, rs, axis=rho_ax, edge_order=2)
    drho_rho_u = np.gradient(rho * drho_u, rs, axis=rho_ax, edge_order=2)
    e_z = 1j * pms.m * round_relative(drho_u, TOLDIGITS) - 1 / (
        rho + 1e-100
    ) * round_relative(drho_rho_u, TOLDIGITS)
    iz0 = np.where(rho == 0)
    return e_z


def b_r_sp(rho, phi, z, omega, pms):
    u = phasor_spectral(rho, phi, z, omega, pms)
    return -1 * pms.m * omega / (rho + 1e-100) * u


def b_phi_sp(rho, phi, z, omega, pms):
    u = phasor_spectral(rho, phi, z, omega, pms)
    rho_ax = 0
    rs = rho[:, 0].transpose()
    drho_u = np.gradient(u, rs, axis=rho_ax, edge_order=2)
    return -1j * omega * round_relative(drho_u, TOLDIGITS)


def b_z_sp(rho, phi, z, omega, pms):
    return np.zeros_like(rho)


# def e_r_curl_spectral(rho, phi, z, omega, pms):
#     u = phasor_spectral(rho, phi, z, omega, pms)
#     rho_ax, z_ax = 0, 1
#     rs = rho[:, 0].transpose()
#     zs = z[0]
#     du_drho = np.gradient(u, rs, axis=rho_ax, edge_order=2)
#     d2u_drho_dz = np.gradient(du_drho, zs, axis=z_ax, edge_order=2)
#     e_r = d2u_drho_dz
#     return e_r


# def e_phi_curl_spectral(rho, phi, z, omega, pms):
#     u = phasor_spectral(rho, phi, z, omega, pms)
#     z_ax = 1
#     zs = z[0]
#     du_dz = np.gradient(u, zs, axis=z_ax, edge_order=2)
#     m = pms.m
#     e_phi = 1j * m / (rho + pms.lambda0 * 1e-10) * du_dz
#     return e_phi


# def e_z_curl_spectral(rho, phi, z, omega, pms):

#     u = phasor_spectral(rho, phi, z, omega, pms)
#     rho_ax = 0
#     rs = rho[:, 0].transpose()
#     du_drho = np.gradient(u, rs, axis=rho_ax, edge_order=2)
#     d2u_drho2 = np.gradient(du_drho, rs, axis=rho_ax, edge_order=2)
#     m = pms.m
#     e_z = (
#         1
#         / (rho + pms.lambda0 * 1e-10)
#         * (du_drho + rho * d2u_drho2 + m ** 2 / (rho + pms.lambda0 * 1e-10) * m ** 2)
#     )
#     return e_z


# def b_r_curl_spectral(rho, phi, z, omega, pms):
#     u = phasor_spectral(rho, phi, z, omega, pms)
#     return -1 * pms.m * omega / rho * e_r


# def b_phi_curl_spectral(rho, phi, z, omega, pms):
#     u = phasor_spectral(rho, phi, z, omega, pms)
#     rho_ax = 0
#     rs = rho[:, 0].transpose()
#     du_drho = np.gradient(u, rs, axis=rho_ax, edge_order=2)
#     return 1j * omega * du_drho


# def b_z_curl_spectral(rho, phi, z, omega, pms):
#     return 0.0j


# ~~~~~ FUNCTION LOOKUP ~~~~~~~~
def ez_nozero_rho_an(rho, phi, z, omega, pms):
    rho = copy(rho)
    rho[0, :] = rho[1, :] * 0.1
    return e_z_anl(rho, phi, z, omega, pms)


def ez_nozero_rho_num(rho, phi, z, omega, pms):
    rho = copy(rho)
    rho[0, :] = rho[1, :] * 0.1
    return e_z_num(rho, phi, z, omega, pms)


def ez_nozero_rho_sp(rho, phi, z, omega, pms):
    rho = copy(rho)
    rho[0, :] = rho[1, :] * 0.1
    return e_z_sp(rho, phi, z, omega, pms)


def vpf_func(comp, ftype):
    funcs = {
        "analytic": {
            "e_r": e_r_anl,
            "e_phi": e_phi_anl,
            "e_z": e_z_anl,
            "b_r": b_r_anl,
            "b_phi": b_phi_anl,
            "b_z": b_z_anl,
        },
        "numerical": {"e_r": e_r_num, "e_phi": e_phi_num, "e_z": e_z_num,},  # e_z_curl
        "spectral": {
            "e_r": e_r_sp,
            "e_phi": e_phi_sp,
            "e_z": e_z_sp,
            "b_r": b_r_sp,
            "b_phi": b_phi_sp,
            "b_z": b_z_sp,
        },
    }
    if comp in ("b_r", "b_phi", "b_z") and ftype == "numerical":
        raise NotImplementedError(
            "Numerical calculation of B(t) field needs time-derivation and is not implemented."
        )
    return funcs[ftype][comp]
