#!/opt/anaconda3/bin/python
# -*- coding: utf-8 -*-

"""
Equations for radially polarized Gaussian fields, cylindrical coordinates (table 2)
"""

from copy import copy

import numpy as np

from GaussPulse import C_LIGHT

refr_index = 1.0
E0 = 1  # 1.143029860657537  # algorithmically found to correspond the article, but can be choosen arbitrary


def component(comp, r, phi, z, omega, params):
    if params.order == 0:
        comps = {"e_r": e_rdl_r0, "e_z": e_rdl_z0, "b_phi": b_rdl_phi0}
    elif params.order == 2:
        comps = {"e_r": e_rdl_r, "e_z": e_rdl_z, "b_phi": b_rdl_phi}
    elif params.order == -1:
        comps = {"e_r": e_rdl_r2, "e_z": e_rdl_z2, "b_phi": b_rdl_phi2}
    else:
        raise NotImplementedError(
            f"only order 0 and order 2 is implemented; order must be 0, 2 or -1 (for only 2nd order corrections) (given: {params.order})"
        )
    if params.isodiffracting:
        params = copy(params)
        params.thetad = params.thetad * omega / params.omega0
    return comps[comp](r, phi, z, omega, params)


def e_r(r, phi, z, omega, params):
    return component("e_r", r, phi, z, omega, params)


def e_z(r, phi, z, omega, params):
    return component("e_z", r, phi, z, omega, params)


def b_phi(r, phi, z, omega, params):
    return component("b_phi", r, phi, z, omega, params)


# Generally used functions
################################################################################


def A0(E0, omega):
    # return 1  # todo: should this be omega-independent? they derive it from dimension analysis
    try:
        return E0 / (1 * k0(omega))
    except ZeroDivisionError:
        return 0.0j


def k0(omega):
    return omega / C_LIGHT


def kh(omega):
    return refr_index * omega / C_LIGHT


def mu(z, omega, thetad):
    return 1 / (1 + 1j * z * kh(omega) * thetad ** 2 / 2)


def G(r, phi, z, omega, thetad):
    _mu = mu(z, omega, thetad)
    _kh = kh(omega)
    a = _mu * _kh ** 2 * thetad ** 2 * (r ** 2 / 4)
    b = np.exp(-a)
    c = A0(E0, k0(omega)) * _mu
    return b * c
    # return A0 * _mu * np.exp(-1*_mu * _kh**2 * params.thetad**2 * r/4)


def gaussian_beam(r, phi, z, omega, thetad):
    _kh = kh(omega)
    return E0 * G(r, phi, z, omega, thetad) * np.exp(1j * _kh * z)


# def ggaussian_beam(r, phi, z, omega, params):
#     _kh = kh(omega)
#     w0 = 2/(_kh * params.thetad)
#     zr = 2/(_kh * params.thetad**2)
#     wz = w0 * np.sqrt(1+(z/zr)**2)
#     Rz = z*(1+(zr/z)**2)
#     psi = np.arctan(z/zr)
#     amp = E0 * w0 / wz * np.exp(-r**2 / wz**2)
#     phase = _kh * r**2 / (2*Rz) - psi #+ _kh*z
#     return amp * np.exp(1j * phase)


def polynom(thetad, p0=1, p2=0, p4=0, p6=0, p8=0, p10=0):
    "only even powered terms exists"
    return (
        p0
        + p2 * thetad ** 2
        + p4 * thetad ** 4
        + p6 * thetad ** 6
        + p8 * thetad ** 8
        + p10 * thetad ** 10
    )


def field_comp(r, phi, z, omega, thetad, coeff, p0=1, p2=0, p4=0, p6=0, p8=0, p10=0):
    "general shape of the functions in the article"
    return (
        gaussian_beam(r, phi, z, omega, thetad)
        * coeff
        * polynom(thetad, p0, p2, p4, p6, p8, p10)
    )


# electric field:
################################################################################


def e_rdl_r0(r, phi, z, omega, params):
    _mu = mu(z, omega, params.thetad)
    _kh = kh(omega)
    coeff = 1j * params.thetad ** 2 * _kh * r * _mu / 2
    p0 = -1
    p2 = _mu
    p4 = -1 * _kh ** 2 * r ** 2 * _mu ** 2 / 8
    return field_comp(r, phi, z, omega, params.thetad, coeff, p0, p2, p4)


def e_rdl_r2(r, phi, z, omega, params):
    _mu = mu(z, omega, params.thetad)
    _kh = kh(omega)
    coeff = 1j * params.thetad ** 2 * _kh * r / 2
    p0 = -1 * _mu ** 2 / 2
    p2 = _mu ** 3 * (3 / 4 - r ** 2 * _kh ** 2 / 8)
    p4 = r ** 2 * _kh ** 2 * _mu ** 4 * (3 / 2 ** 4 + r ** 2 * _kh ** 2 / 2 ** 6)
    p6 = -7 * r ** 2 * _kh ** 2 * _mu ** 5 / 2 ** 7
    p8 = r ** 6 * _kh ** 6 * _mu ** 6 / 2 ** 9
    return field_comp(r, phi, z, omega, params.thetad, coeff, p0, p2, p4, p6, p8)


def e_rdl_phi0(r, phi, z, omega, params):
    return np.zeros_like(r)


def e_rdl_phi2(r, phi, z, omega, params):
    return np.zeros_like(r)


def e_rdl_z0(r, phi, z, omega, params):
    _mu = mu(z, omega, params.thetad)
    _kh = kh(omega)
    coeff = params.thetad ** 2
    p0 = _mu
    p2 = -1 * _mu ** 2 * (1 / 2 + _kh ** 2 * r ** 2 / 4)
    p4 = _mu ** 3 * _kh ** 2 * r ** 2 / 4
    p6 = -1 * _mu ** 4 * _kh ** 4 * r ** 4 / 2 ** 6
    return field_comp(r, phi, z, omega, params.thetad, coeff, p0, p2, p4, p6)


def e_rdl_z2(r, phi, z, omega, params):
    _mu = mu(z, omega, params.thetad)
    _kh = kh(omega)
    coeff = params.thetad ** 2
    p0 = _mu ** 2
    p2 = _mu ** 3 * (3 / 4 + r ** 2 * _kh ** 2 / 8)
    p4 = r ** 2 * _kh ** 2 * _mu ** 4 * (3 / 2 ** 4 - r ** 2 * _kh ** 2 / 2 ** 4)
    p6 = r ** 4 * _kh ** 4 * _mu ** 5 * (9 / 2 ** 7 + r ** 2 * _kh ** 2 / 2 ** 8)
    p8 = -5 * r ** 6 * _kh ** 6 * _mu ** 6 / 2 ** 9
    p10 = r ** 8 * _kh ** 8 * _mu ** 7 / 2 ** 12
    return field_comp(r, phi, z, omega, params.thetad, coeff, p0, p2, p4, p6, p8, p10)


# magnetic field:
################################################################################


def b_rdl_r0(r, phi, z, omega, params):
    return np.zeros_like(r)


def b_rdl_r2(r, phi, z, omega, params):
    return np.zeros_like(r)


def b_rdl_phi0(r, phi, z, omega, params):
    _mu = mu(z, omega, params.thetad)
    _kh = kh(omega)
    _k0 = k0(omega)
    coeff = -1j * params.thetad ** 2 * _kh ** 2 * r * _mu / (2 * _k0)
    return field_comp(r, phi, z, omega, params.thetad, coeff)


def b_rdl_phi2(r, phi, z, omega, params):
    _mu = mu(z, omega, params.thetad)
    _kh = kh(omega)
    _k0 = k0(omega)
    coeff = -1j * params.thetad ** 2 * _kh ** 2 * r / (4 * _k0)
    p0 = _mu ** 2
    p2 = r ** 2 * _kh ** 2 * _mu ** 3 / 4
    p4 = r ** 4 * _kh ** 4 * _mu ** 4 / 2 ** 5
    return field_comp(r, phi, z, omega, params.thetad, coeff, p0, p2, p4)


def b_rdl_z0(r, phi, z, omega, params):
    return np.zeros_like(r)


def b_rdl_z2(r, phi, z, omega, params):
    return np.zeros_like(r)


# 0th and 2nd order vectors
################################################################################


def e_rdl_0(r, phi, z, omega, params):
    return np.array(
        [
            b_rdl_r0(r, phi, z, omega, params),
            b_rdl_phi0(r, phi, z, omega, params),
            b_rdl_z0(r, phi, z, omega, params),
        ]
    )


def e_rdl_2(r, phi, z, omega, params):
    return np.array(
        [
            b_rdl_r2(r, phi, z, omega, params),
            b_rdl_phi2(r, phi, z, omega, params),
            b_rdl_z2(r, phi, z, omega, params),
        ]
    )


def b_rdl_0(r, phi, z, omega, params):
    return np.array(
        [
            b_rdl_r0(r, phi, z, omega, params),
            b_rdl_phi0(r, phi, z, omega, params),
            b_rdl_z0(r, phi, z, omega, params),
        ]
    )


def b_rdl_2(r, phi, z, omega, params):
    return np.array(
        [
            b_rdl_r2(r, phi, z, omega, params),
            b_rdl_phi2(r, phi, z, omega, params),
            b_rdl_z2(r, phi, z, omega, params),
        ]
    )


# components with corrections
################################################################################


def e_rdl_r(r, phi, z, omega, params):
    return (
        e_rdl_r0(r, phi, z, omega, params)
        + e_rdl_r2(r, phi, z, omega, params) * params.thetad ** 2
    )


def e_rdl_phi(r, phi, z, omega, params):
    return (
        e_rdl_phi0(r, phi, z, omega, params)
        + e_rdl_phi2(r, phi, z, omega, params) * params.thetad ** 2
    )


def e_rdl_z(r, phi, z, omega, params):
    return (
        e_rdl_z0(r, phi, z, omega, params)
        + e_rdl_z2(r, phi, z, omega, params) * params.thetad ** 2
    )


def b_rdl_r(r, phi, z, omega, params):
    return (
        b_rdl_r0(r, phi, z, omega, params)
        + b_rdl_r2(r, phi, z, omega, params) * params.thetad ** 2
    )


def b_rdl_phi(r, phi, z, omega, params):
    return (
        b_rdl_phi0(r, phi, z, omega, params)
        + b_rdl_phi2(r, phi, z, omega, params) * params.thetad ** 2
    )


def b_rdl_z(r, phi, z, omega, params):
    return (
        b_rdl_z0(r, phi, z, omega, params)
        + b_rdl_z2(r, phi, z, omega, params) * params.thetad ** 2
    )


# complete vectors
################################################################################


def e_rdl(r, phi, z, omega, params):
    return e_rdl_0(r, phi, z, omega, params) + params.thetad ** 2 * e_rdl_2(
        r, phi, z, omega, params
    )


def b_rdl(r, phi, z, omega, params):
    return b_rdl_0(r, phi, z, omega, params) + params.thetad ** 2 * b_rdl_2(
        r, phi, z, omega, params
    )
