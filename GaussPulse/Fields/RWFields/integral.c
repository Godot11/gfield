#include "rwfield.h"

typedef struct intg_vars
{
    /* Struct encapsulating the parameters of the field (see field.py for
    a detailed description) */
    double r;
    double phi;
    double z;
    double omega;
    params pms;
    int component;
} intg_vars;

double re_integrand_gsl(double theta, void *varsptr)
{
    intg_vars vars = *(intg_vars *)varsptr;
    return creal(integrand(theta, vars.r, vars.phi, vars.z, vars.omega, vars.pms, vars.component));
}

double im_integrand_gsl(double theta, void *varsptr)
{
    intg_vars vars = *(intg_vars *)varsptr;
    return cimag(integrand(theta, vars.r, vars.phi, vars.z, vars.omega, vars.pms, vars.component));
}

double _Complex rw_integral(int component, //double *re_result, double *im_result,
                            double r, double phi, double z, double omega, params pms,
                            int warn_subdvs, int warn_roundoff)
{
    /*Compute the field value for a given omega, given in the `vars` struct.*/

    double re_result, im_result;
    double _Complex result;

    //do not mess with infinite lambda...
    if (omega == 0.0)
    {
        return CMPLX(0.0, 0.0);
    }

    double alpha = asin(pms.na / pms.n1);

    //set up integration
    gsl_integration_workspace *workspace = gsl_integration_workspace_alloc(2000);
    gsl_function re_integrand, im_integrand;
    double re_error, im_error;

    //parameters other than the first argument for the integrated function
    intg_vars vars = (intg_vars){r, phi, z, omega, pms, component};
    re_integrand.params = &vars;
    im_integrand.params = &vars;

    //choose the real and imaginary integrand as asked
    re_integrand.function = &re_integrand_gsl;
    im_integrand.function = &im_integrand_gsl;

    //disable the default GSL error handler that kills the program every time an intergal fails
    gsl_set_error_handler_off();

    int re_qag_flag = gsl_integration_qag(&re_integrand, 0, alpha,                                             //* arguments:
                                          0, INTEGRATION_RELERR, INTEGRATION_MAX_DIVISIONS, GSL_INTEG_GAUSS41, //gsl_integration_qag (const gsl_function* f, double a, double b,
                                          workspace, &re_result, &re_error);                                   //      double epsabs, double epsrel, size_t limit, int key,
                                                                                                               //      gsl_integration_workspace* workspace, double* result, double* abserr)
    int im_qag_flag = gsl_integration_qag(&im_integrand, 0, alpha,
                                          0, INTEGRATION_RELERR, INTEGRATION_MAX_DIVISIONS, GSL_INTEG_GAUSS41,
                                          workspace, &im_result, &im_error);

    // set the result to 0 if the integration fails, and print warnings if needed
    if (re_qag_flag == GSL_EROUND)
    {
        re_result = 0;
        if (warn_roundoff)
            printf("GSL: roundoff error: real part at r=%.2g, z=%.2g\n", vars.r, vars.z);
    }
    if (im_qag_flag == GSL_EROUND)
    {
        im_result = 0;
        if (warn_roundoff)
            printf("GSL: roundoff error: imaginary part at r=%.2g, z=%.2g\n", vars.r, vars.z);
    }
    if (re_qag_flag == GSL_EMAXITER)
    {
        re_result = 0;
        if (warn_subdvs)
            printf("GSL: subdivision limit exceeded: real part at r=%.2g, z=%.2g\n", vars.r, vars.z);
    }
    if (im_qag_flag == GSL_EMAXITER)
    {
        im_result = 0;
        if (warn_subdvs)
            printf("GSL: subdivision limit exceeded: imaginary part at r=%.2g, z=%.2g\n", vars.r, vars.z);
    }

    gsl_integration_workspace_free(workspace);
    return CMPLXL(re_result, im_result);
}