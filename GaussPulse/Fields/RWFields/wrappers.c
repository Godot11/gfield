#include "rwfield.h"

void testfunc(double *inarray, double *outarray, int npoints) // to test array python<->c interface
{
    int i;
    double a;
    for (i = 0; i < npoints; i++)
    {
        a = inarray[i];
        *(outarray + i) = a * a;
    }
}

void Q_func_wrapper(double *re_res, double *im_res, double theta, double r, double phi, double omega, params pms, int component)
{
    double _Complex res = Q_func(theta, r, phi, omega, pms, component);
    (*re_res) = (double)creal(res);
    (*im_res) = (double)cimag(res);
}

void integrand_wrapper(double *re_res, double *im_res, double theta, double r, double phi, double z, double omega, params pms, int component)
{
    debug_print("integrand_wrapper: in: theta=%f, r=%f, phi=%f, z=%f, omega=%f, component=%d\n", theta, r, phi, z, omega, component);
    double _Complex res = integrand(theta, r, phi, z, omega, pms, component);
    (*re_res) = (double)creal(res);
    (*im_res) = (double)cimag(res);
    debug_print("integrand_wrapper: locals: creal(res)=%f, cimag(res)=%f, re_res=%f, im_res=%f\n", creal(res), cimag(res), *re_res, *im_res);
}

void rw_integral_wrapper(double *re_res, double *im_res, int component,
                         double r, double phi, double z, double omega, params pms,
                         int warn_subdvs, int warn_roundoff)
{
    double _Complex res = rw_integral(component, r, phi, z, omega, pms, warn_subdvs, warn_roundoff);
    (*re_res) = (double)creal(res);
    (*im_res) = (double)cimag(res);
}

void rw_integral_wrapper_rz_array(double *re_res_array, double *im_res_array, int n_points, int component,
                                  double *r_array, double phi, double *z_array, double omega, params pms,
                                  int warn_subdvs, int warn_roundoff)
{
    int i;
    double r, z;
    double *re_res, *im_res;
    for (i = 0; i < n_points; i++)
    {
        r = r_array[i];
        z = z_array[i];
        re_res = re_res_array + i;
        im_res = im_res_array + i;
        rw_integral_wrapper(re_res, im_res, component, r, phi, z, omega, pms, warn_subdvs, warn_roundoff);
    }
}
