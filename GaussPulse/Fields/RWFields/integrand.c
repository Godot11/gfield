#include "rwfield.h"

double _Complex integrand(double theta, double r, double phi, double z, double omega, params pms, int component)
{
    double T, R;
    double _Complex Q, exponent;
    T = apodization(theta);
    R = R_func(theta, omega, pms);
    Q = Q_func(theta, r, phi, omega, pms, component);
    exponent = cexp(_Complex_I * z * k1(omega, pms) * cos(theta));

    debug_print("integrand: locals: T=%f, R=%f, Q=%f+%fi, exponent=%f+%fi\n", T, R, creal(Q), cimag(Q), creal(exponent), cimag(exponent));
    return T * R * Q * sin(theta) * exponent;
}

double laguerre(int p, int m, double x)
{
    switch (p)
    {
    case 0:
        return 1;
    case 1:
        return gsl_sf_laguerre_1(m, x);
    case 2:
        return gsl_sf_laguerre_2(m, x);
    case 3:
        return gsl_sf_laguerre_3(m, x);
    default:
        return gsl_sf_laguerre_n(p, m, x);
    }
}

double k0(double omega, params pms)
{
    /*Wave number in vacuum*/
    return omega / pms.c_light;
}

double k1(double omega, params pms)
{
    /*Wave number in the material the pulse propagates in (before the refractive
     interface, if there's any)*/
    return pms.n1 * omega / pms.c_light;
}

double apodization(double theta)
{
    return sqrt(cos(theta));
}

double _Complex pu_Q(double theta, double r, double phi, double omega, params pms, int component)
{
    double _Complex result;
    double arg, bessel, k1_, sintheta;
    switch (component)
    {
    case COMP_E_R:
        k1_ = k1(omega, pms);
        sintheta = sin(theta);
        arg = k1_ * r * sin(theta);
        bessel = gsl_sf_bessel_J1(arg);
        result = I * cos(theta) * bessel;
        debug_print("Q_func: internal R: k1=%f, r=%f, sintheta=%f, arg=%f, bessel=%f, result=%f+%fi\n", k1_, r, sintheta, arg, bessel, creal(result), cimag(result));
        break;
    case COMP_E_PHI:
        result = 0;
        break;
    case COMP_E_Z:
        result = sin(theta) * gsl_sf_bessel_J0(k1(omega, pms) * r * sin(theta));
        break;
    default:
        result = 0;
        printf("C error: Invalid component ID.");
        break;
    }
    return result;
}

double _Complex kohina_Q(double theta, double r, double phi, double omega, params pms, int component)
{
    double _Complex result;
    double v, bessel, k1_, sintheta;
    k1_ = k1(omega, pms);
    sintheta = sin(theta);
    v = k1_ * r * sintheta;
    switch (component)
    {
    case COMP_E_R:
        bessel = gsl_sf_bessel_Jn(pms.m - 1, v) - gsl_sf_bessel_Jn(pms.m + 1, v);
        result = I * cos(theta) * bessel;
        break;
    case COMP_E_PHI:
        result = pms.m / v * gsl_sf_bessel_Jn(pms.m, v) * cos(theta);
        break;
    case COMP_E_Z:
        result = sin(theta) * gsl_sf_bessel_Jn(pms.m, v);
        break;
    default:
        result = 0;
        printf("C error: Invalid component ID.");
        break;
    }
    return result;
}

double _Complex Q_func(double theta, double r, double phi, double omega, params pms, int component)
{
    // return pu_Q(theta, r, phi, omega, pms, component);
    return kohina_Q(theta, r, phi, omega, pms, component);
}

// ~~ different R definitions

double pu_R(double theta, double omega, params pms)
{
    debug_print("pu_R: in: theta=%f, omega=%f, pms.na=%f, pms.n1=%f, pms.beta=%f\n", theta, omega, pms.na, pms.n1, pms.beta);
    double sinalpha = pms.na / pms.n1;
    double sintheta = sin(theta);

    double btpera = pms.beta * sintheta / sinalpha;
    double lagpolynom = 1;
    double result = btpera / sinalpha * exp(-1 * btpera * btpera) * lagpolynom;
    debug_print("pu_R: locals: sinalpha=%f, sintheta=%f, btpera=%f, result=%f, \n", sinalpha, sintheta, btpera, result);
    return result;
}

double pu_fix_R(double theta, double omega, params pms)
{
    debug_print("using pu_fix_R %d\n", 1);
    double sinalpha = pms.na / pms.n1;
    double tgalpha = sinalpha / sqrt(1 - sinalpha * sinalpha); //! validate needed
    double tgtheta = tan(theta);

    double btpera = pms.beta * tgtheta / tgalpha;
    double lagpolynom = 1;
    double result = btpera / tgalpha * exp(-1 * btpera * btpera) * lagpolynom;
    return result;
}

double lg_R(double theta, double omega, params pms)
{
    debug_print("lg_R:in: theta=%f, omega=%f, pms.na=%f, pms.n1=%f, pms.beta=%f, p=%d, m=%d\n", theta, omega, pms.na, pms.n1, pms.beta, pms.p, pms.m);
    double sinalpha = pms.na / pms.n1;
    double tgalpha = tan(asin(sinalpha)); //sinalpha / sqrt(1 - sinalpha * sinalpha); //! validate needed
    double tgtheta = tan(theta);

    double btpera = pms.beta * tgtheta / tgalpha;
    double lagpolynom = laguerre(pms.p, pms.m, 2 * btpera * btpera);
    double result = pow(btpera, pms.m + 1) / tgtheta * exp(-1 * btpera * btpera) * lagpolynom;
    return result; //result;
}

double levy_R(double theta, double omega, params pms)
{
    double z, thetad, thetad2, thetad4, r, r2, kh, _kh2;
    double _Complex mu, coeff, p4, G, gaussian, res;
    z = 10000;
    thetad = 1 / (pms.beta * sqrt((pms.n1 * pms.n1) / (pms.na * pms.na) - 1)); //is this levy's theta or thetad
    r = z * tan(theta);
    kh = k1(omega, pms);
    mu = 1 / (1 + I * z * kh * (thetad * thetad) / 2);
    thetad2 = thetad * thetad;
    thetad4 = thetad2 * thetad2;
    r2 = r * r;
    _kh2 = kh * kh;

    coeff = I * thetad2 * kh * r * mu / 2;
    G = 1 * mu * cexp(-mu * _kh2 * thetad2 * (r2 / 4));
    gaussian = G; //* cexp(I * kh * z);
    p4 = -1 * _kh2 * r2 * (mu * mu) / 8;
    res = gaussian * coeff * (-1 + mu * thetad2 + p4 * thetad4);
    return cabs(res);
}

double flat_R(double theta, double omega, params pms)
{
    debug_print("using flat_R %d\n", 1);
    double sinalpha = pms.na / pms.n1;
    double tgalpha = sinalpha / sqrt(1 - sinalpha * sinalpha); //! validate needed
    return (pms.beta * tan(theta) <= tgalpha) ? 1 : 0;
}

double ring_R(double theta, double omega, params pms)
{
    debug_print("using flat_R %d\n", 1);
    double sinalpha = pms.na / pms.n1;
    double tgalpha = sinalpha / sqrt(1 - sinalpha * sinalpha); //! validate needed
    double bound = tgalpha / pms.beta;
    return (0.95 * bound <= tan(theta) && tan(theta) <= bound) ? 1 : 0;
}

double R_func(double theta, double omega, params pms)
{
    double res;
    switch (pms.R_func_flag)
    {
    case R_FLAG_PU:
        res = pu_R(theta, omega, pms);
        break;
    case R_FLAG_PU_FIX:
        res = pu_fix_R(theta, omega, pms);
        break;
    case R_FLAG_LG:
        res = lg_R(theta, omega, pms);
        break;
    case R_FLAG_FLAT:
        res = flat_R(theta, omega, pms);
        break;
    case R_FLAG_RING:
        res = ring_R(theta, omega, pms);
        break;
    case R_FLAG_LEVY:
        res = levy_R(theta, omega, pms);
        break;
    default:
        printf("INVALID R FUNCTION FLAG");
        res = 0;
        break;
    }
}
