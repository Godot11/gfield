#ifndef INTEGRAND_H
#define INTEGRAND_H

#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_sf_laguerre.h>

//debug prints

// #define DEBUG
#ifdef DEBUG
#define debug_print(fmt, ...)                  \
    do                                         \
    {                                          \
        if (DEBUG)                             \
            fprintf(stderr, fmt, __VA_ARGS__); \
    } while (0)
#else
#define debug_print(fmt, ...)
#endif // DEBUG

//#define C_LIGHT (GSL_CONST_MKSA_SPEED_OF_LIGHT / 1E9) //lightspeed in um/fs
#define M_PI 3.14159265358979323846
#define SQRT_2 1.4142135623730951 //square root of 2
//id for func names
// #define R_ID 0
// #define Z_ID 1
// #define R_NRIF_ID 2
// #define Z_NRIF_ID 3

typedef enum comp_flag
{
    COMP_E_R = 1,
    COMP_E_PHI = 2,
    COMP_E_Z = 3,
} comp_flag;

typedef enum R_flag
{
    R_FLAG_PU = 1,
    R_FLAG_PU_FIX = 2,
    R_FLAG_LG = 3,
    R_FLAG_FLAT = 4,
    R_FLAG_LEVY = 5,
    R_FLAG_RING = 6
} R_flag;

//TODO: make these available in API
#define INTEGRATION_RELERR 1E-4       // relative error of integration
#define INTEGRATION_MAX_DIVISIONS 300 // max division points used in the integration before halt

#define WARN_ON_INTEGRATION_FAIL

typedef struct params
{
    double n1;
    double na;
    double beta;
    int p;
    int m;
    R_flag R_func_flag;
    // double d;
    // double omega;
    // double omega0;
    // double pulse_duration;
    double c_light;
} params;

double k0(double omega, params pms);

double k1(double omega, params pms);

double apodization(double theta);

double _Complex Q_func(
    double theta,
    double r,
    double phi,
    double omega,
    params pms,
    int component);

double R_func(
    double theta,
    double omega,
    params pms);

double _Complex integrand(
    double theta,
    double r,
    double phi,
    double z,
    double omega,
    params pms,
    int component);

double re_integrand(double theta, void *varsptr);

double im_integrand(double theta, void *varsptr);

double _Complex rw_integral(
    int component,
    double r,
    double phi,
    double z,
    double omega,
    params pms,
    int warn_subdvs,
    int warn_roundoff);

void integrand_wrapper(
    double *re_res,
    double *im_res,
    double theta,
    double r,
    double phi,
    double z,
    double omega,
    params pms,
    int component);

void rw_integral_wrapper(
    double *re_res,
    double *im_res,
    int component,
    double r,
    double phi,
    double z,
    double omega,
    params pms,
    int warn_subdvs,
    int warn_roundoff);

void rw_integral_wrapper_rz_array(
    double *re_res_array,
    double *im_res_array,
    int n_points,
    int component,
    double *r_array,
    double phi,
    double *z_array,
    double omega,
    params pms,
    int warn_subdvs,
    int warn_roundoff);

void Q_func_wrapper(
    double *re_res,
    double *im_res,
    double theta,
    double r,
    double phi,
    double omega,
    params pms,
    int component);

void testfunc(double *inarray, double *outarray, int npoints);

#endif //# INTEGRAND_H