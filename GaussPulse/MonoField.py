# -*- coding: utf-8 -*-

from __future__ import annotations

from copy import copy
from typing import TYPE_CHECKING, Any, List, Optional, Union

import numpy as np
from nptyping import NDArray
from . import Comp, Parameters

# Speed of light in nm/fs
from . import C_LIGHT, FieldCompFunction, Parameters
from .Fields import LevyRadpolField, RWFields  # PuRadpolField,  # static components
from .Fields import VikartovskyVPField as VikField

# if TYPE_CHECKING:


FIELD_COMPS = ["e_r", "e_phi", "e_z", "b_r", "b_phi", "b_z"]


class FieldFormula(object):
    def __init__(
        self,
        e_r: Optional[FieldCompFunction] = None,
        e_phi: Optional[FieldCompFunction] = None,
        e_z: Optional[FieldCompFunction] = None,
        b_r: Optional[FieldCompFunction] = None,
        b_phi: Optional[FieldCompFunction] = None,
        b_z: Optional[FieldCompFunction] = None,
    ):

        self.e_r_func: Union[FieldCompFunction, None] = e_r
        self.b_r_func: Union[FieldCompFunction, None] = b_r
        self.e_phi_func: Union[FieldCompFunction, None] = e_phi
        self.b_phi_func: Union[FieldCompFunction, None] = b_phi
        self.e_z_func: Union[FieldCompFunction, None] = e_z
        self.b_z_func: Union[FieldCompFunction, None] = b_z

        comp_func_pairs = (
            (Comp.E_r, e_r),
            (Comp.E_phi, e_phi),
            (Comp.E_z, e_z),
            (Comp.B_r, b_r),
            (Comp.B_phi, b_phi),
            (Comp.B_z, b_z),
        )
        self.zero_comps: List[Comp] = []
        self.nonzero_comps: List[Comp] = []

        for comp, func in comp_func_pairs:
            if func is None:
                self.zero_comps.append(comp)
            else:
                self.nonzero_comps.append(comp)

    def e_r(
        self, r: NDArray, phi: float, z: NDArray, t_or_omega: float, params: Any
    ) -> NDArray:
        assert self.e_r_func is not None
        return self.e_r_func(r, phi, z, t_or_omega, params)

    def e_phi(
        self, r: NDArray, phi: float, z: NDArray, t_or_omega: float, params: Any
    ) -> NDArray:
        assert self.e_phi_func is not None
        return self.e_phi_func(r, phi, z, t_or_omega, params)

    def e_z(
        self, r: NDArray, phi: float, z: NDArray, t_or_omega: float, params: Any
    ) -> NDArray:
        assert self.e_z_func is not None
        return self.e_z_func(r, phi, z, t_or_omega, params)

    def b_r(
        self, r: NDArray, phi: float, z: NDArray, t_or_omega: float, params: Any
    ) -> NDArray:
        assert self.e_r_func is not None
        return self.e_r_func(r, phi, z, t_or_omega, params)

    def b_phi(
        self, r: NDArray, phi: float, z: NDArray, t_or_omega: float, params: Any
    ) -> NDArray:
        assert self.b_phi_func is not None
        return self.b_phi_func(r, phi, z, t_or_omega, params)

    def b_z(
        self, r: NDArray, phi: float, z: NDArray, t_or_omega: float, params: Any
    ) -> NDArray:
        assert self.e_r_func is not None
        return self.e_r_func(r, phi, z, t_or_omega, params)

    def component(
        self,
        comp: Comp,
        r: NDArray,
        phi: float,
        z: NDArray,
        t_or_omega: float,
        params: Any,
    ) -> NDArray:
        if comp in self.zero_comps:
            raise ValueError(f"{comp} is zero component")
        elif comp is Comp.E_r:
            return self.e_r(r, phi, z, t_or_omega, params)
        elif comp is Comp.E_phi:
            return self.e_phi(r, phi, z, t_or_omega, params)
        elif comp is Comp.E_z:
            return self.e_z(r, phi, z, t_or_omega, params)
        elif comp is Comp.B_r:
            return self.b_r(r, phi, z, t_or_omega, params)
        elif comp is Comp.B_phi:
            return self.b_phi(r, phi, z, t_or_omega, params)
        elif comp is Comp.B_z:
            return self.b_z(r, phi, z, t_or_omega, params)
        else:
            raise ValueError(f"invalid component: {comp}")


class SpectralFormula(FieldFormula):
    pass


class TimeFormula(SpectralFormula):
    pass


# ~~ More formulas
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def plane_wave(r, phi, z, omega, params):
    k = 2 * np.pi * omega / C_LIGHT
    return np.exp(1j * k * z)


def t_gaussian(r, phi, z, t, params):
    return gaussian_beam(r, phi, z, params.omega0, params) * np.exp(
        1j * params.omega0 * t
    )


def isodiffr_levypm(params, omega):
    newparams = copy(params)
    thetas = params.thetad * np.sqrt(params.omega0 / np.abs(omega))
    newparams.thetad = np.nan_to_num(thetas, copy=False)
    return newparams


# def levy_r0_isodiffr(r, phi, z, omega, params):
#     return LevyRadpolField.e_rdl_r0(r, phi, z, omega, isodiffr_levypm(params, omega))


# def levy_z0_isodiffr(r, phi, z, omega, params):
#     return LevyRadpolField.e_rdl_z0(r, phi, z, omega, isodiffr_levypm(params, omega))


# def levy_b0_isodiffr(r, phi, z, omega, params):
#     return LevyRadpolField.b_rdl_phi0(r, phi, z, omega, isodiffr_levypm(params, omega))


# def levy_r_isodiffr(r, phi, z, omega, params):
#     return LevyRadpolField.e_rdl_r(r, phi, z, omega, isodiffr_levypm(params, omega))


# def levy_z_isodiffr(r, phi, z, omega, params):
#     return LevyRadpolField.e_rdl_z(r, phi, z, omega, isodiffr_levypm(params, omega))


# def levy_b_isodiffr(r, phi, z, omega, params):
#     return LevyRadpolField.b_rdl_phi(r, phi, z, omega, isodiffr_levypm(params, omega))


def gaussian_beam(r, phi, z, omega, params: Parameters.LevyParameters):
    if params.isodiffracting:
        return gaussian_beam(r, phi, z, omega, isodiffr_levypm(params, omega))
    else:
        return LevyRadpolField.gaussian_beam(r, phi, z, omega, params.thetad)


# ~~ Define some useful formulas


# simpler formulas for testing
# class e_r_formulas:
planewave = SpectralFormula(e_r=plane_wave)
gaussian = SpectralFormula(e_r=gaussian_beam)
time_gaussian = TimeFormula(e_r=t_gaussian)

# levy_e_r = SpectralFormula(e_r=LevyRadpolField.e_rdl_r0)
# pu_e_r = SpectralFormula(e_r=PuRadpolField.e_r)

# main formulas
# class full_formulas:
# levy_isodiffr = SpectralFormula(
#     e_r=levy_r0_isodiffr, e_z=levy_z0_isodiffr, b_phi=levy_b0_isodiffr,
# )

# levy_isodiffr_o2 = SpectralFormula(
#     e_r=levy_r_isodiffr, e_z=levy_z_isodiffr, b_phi=levy_b_isodiffr
# )
# pu = SpectralFormula(e_r=PuRadpolField.e_r, e_z=PuRadpolField.e_z,)
pu_rw = SpectralFormula(e_r=RWFields.e_r, e_z=RWFields.e_z,)

levy = SpectralFormula(
    e_r=LevyRadpolField.e_r, e_z=LevyRadpolField.e_z, b_phi=LevyRadpolField.b_phi,
)

vikartovsky_time_numerical = TimeFormula(
    e_r=VikField.e_r_num, e_phi=VikField.e_r_num, e_z=VikField.e_r_num,
)

vikartovsky_time = TimeFormula(
    e_r=VikField.e_r_anl,
    e_phi=VikField.e_phi_anl,
    e_z=VikField.e_z_anl,
    b_r=VikField.b_r_anl,
    b_phi=VikField.b_phi_anl,
)

vikartovsky_spectral = SpectralFormula(
    e_r=VikField.e_r_sp,
    e_phi=VikField.e_r_sp,
    e_z=VikField.e_r_sp,
    b_r=VikField.e_r_sp,
    b_phi=VikField.e_r_sp,
)
