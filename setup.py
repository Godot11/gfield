import setuptools
from setuptools import setup, Extension, find_packages
from glob import glob
import numpy

with open("README.md", "r") as fh:
    long_description = fh.read()


rwfield_path = os.path.join("GaussPulse", "Fields", "RWFields")
Extension('rwfield', [os.path.join(rwfield_path, integral.c), os.path.join(rwfield_path, ), os.path.join(rwfield_path, ), os.path.join(rwfield_path, ))


setup(
    name="PyRadPulse",  # Replace with your own username
    version="0.0.1",
    author="Gergely Nagy",
    author_email="nagygeri11@gmail.com",
    description="Simulation of tightly focused radially polarized ultrashort light pulses",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
        install_requires=[
        'matplotlib',
        'numpy',
        'scipy',
        'expresso[pycas]'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    ext_modules=[
    Extension('_pypropagate',
                sources = ['source/finite_difference.cpp','source/python.cpp'],
                include_dirs=['libs/lars/include','libs/lars/modules/NDArray/include',numpy.get_include()],
                libraries=['boost_python'],
                library_dirs=['/'],
                extra_compile_args=['-g','-std=c++11','-Wno-unknown-pragmas','-Wno-unused-local-typedef','-ffast-math','-O3']
                ),
    ],
    python_requires=">=3.8",
)
