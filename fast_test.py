from GaussPulse import InputParse, vis, Comp, Norm
from GaussPulse.FieldVector import FieldFromSaved
import numpy as np
import os
import traceback

field_folder = "example_fields"
fields = ["example_levy.json", "example_pu.json", "example_vik.json"]
field = fields[0]
log = ""


def test(func, label=""):
    funcname = func.__name__

    def testfunc(*args, **kwargs):
        start(label, funcname)
        try:
            res = func(*args, **kwargs)
        except Exception as e:
            fail(label, funcname, traceback.format_exc())
            res = None
        else:
            succeed(label, funcname)
        return res

    return testfunc


def start(label, what_started):
    global log
    msg = "[START] " + label + " - " + what_started + "\n"
    log += msg


def fail(label, what_failed, trace):
    global log
    msg = "[FAIL] " + label + " - " + what_failed + "\n" + trace
    log += msg


def succeed(label, what_succeeded):
    global log
    msg = "[SUCCESS] " + label + " - " + what_succeeded + "\n"
    log += msg


# ------------------------------------------------------------------------------


def test_project(field_config_file):
    field_config = test(InputParse.ConfigParser)(field_config_file)
    field = field_config.field
    savepath = field_config.savepath
    label = field_config.label

    print(f"Processing field: {label}")
    print(field.prettyprint(colored=True))
    print("Computing field")
    field.calculate_field(multicore=field_config.multicore)

    print("calculation successful")

    print("testing plots")
    for comp in field.nonzero_comps:
        vis.optax_anim(
            field,
            comp,
            Norm.REAL,
            save=False,
            savepath=os.path.join(savepath, f"optaxanim_{comp.name}.mp4"),
        )

    vis.total_energy(
        field,
        save=False,
        savepath=os.path.join(savepath, f"total_energy_evolution.png"),
    )


if __name__ == "__main__":
    fpath = os.path.join(field_folder, field)
    test_project(fpath)
