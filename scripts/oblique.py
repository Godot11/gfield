import os

# from GaussPulse.meshfield import RectMesh
import numpy as np
from GaussPulse import Comp, InputParse, Norm, utils, physutils, vis
import scipy.interpolate as interp
from matplotlib import pyplot as plt
from matplotlib import animation as animation
from progressbar import progressbar as prbar


configpath = "/home/ezio/Codes/gfield/field_configs/test_levy.json"

# phi cordinate of right angle to z
phi0 = np.deg2rad(0)
# angle of incidence
alpha = np.deg2rad(35)
# z position (where the surface meets te z ax)
z_surface = 5


xmin, xmax, ymin, ymax = -10, 25, -10, 10
nx, ny = 200, 200
xt = np.linspace(xmin, xmax, nx)
yt = np.linspace(ymin, ymax, ny)
x, y = utils.meshgrid(xt, yt)


def oblique_surface_slice(f, i_frame, alpha, z_surface, phi0, intp_method="linear"):

    r = np.sqrt(x ** 2 * np.sin(alpha) ** 2 + y ** 2)
    z = z_surface + x * np.cos(alpha)
    phi = phi0 + np.arctan2(y, (x * np.sin(alpha)))

    # frame = f.get_component_frame(Comp.E_r, i_frame)
    frame = f.get_intensity_frame(i_frame, time_avg=True)
    # plt.pcolormesh(f.mesh.zticks, f.mesh.rticks, frame.real)
    # plt.show()
    # interp = interp2d(f.mesh.rmesh, f.mesh.zmesh, frame.real, kind="linear", fill_value=0)

    surf_re = interp.griddata(
        np.array([f.mesh.rmesh.ravel(), f.mesh.zmesh.ravel()]).T,
        frame.real.ravel(),
        (r, z),
        method="linear",
    )
    # surf_im = interp.griddata(
    #     np.array([f.mesh.rmesh.ravel(), f.mesh.zmesh.ravel()]).T,
    #     frame.imag.ravel(),
    #     (r, z),
    #     method=intp_method,
    # )
    surf = surf_re  # + 1j * surf_im
    # surf_rpf = interp.Rbf(
    #     f.mesh.rmesh, f.mesh.zmesh, frame.real, function="linear", smooth=0
    # )  # default smooth=0 for interpolation
    # surf *= np.exp(1j * field.m * phi)
    return surf


def surface_rpz_to_sp(surface_field, alpha, n):
    er, ep, ez = surface_field

    pass


config = InputParse.ConfigParser(configpath)
field = config.field
field.calculate_field(show_progress=True, show_stats=True, multicore=True)
f = field.set_energy(
    config.energy, ref_frame_i=field.get_t_index(0), time_avg=True, exclude_optax=False
)

vis.optax_anim(f, Comp.E_r, Norm.ABS)

# a_frame = oblique_surface_slice(
#     field, 50, alpha, z_surface, phi0, intp_method="linear",
# )
# plt.pcolormesh(x, y, a_frame.real)
# plt.colorbar()
# plt.xlabel("x")
# plt.ylabel("y")
# plt.show()
# exit()

frames = np.empty((field.nframes, nx, ny), dtype=complex)
for i in prbar(range(0, field.nframes, 1)):
    frames[i] = oblique_surface_slice(
        field, i, alpha, z_surface, phi0, intp_method="linear",
    )


def animate():
    component = Comp.E_r
    qty = Norm.ABS
    t_maxint = 0
    cmap = "afmhot"
    fps = 20
    save = False
    savepath = "oblique_demo2.mp4"

    # if t_maxint < field.t[0]:
    #     t_maxint = field.t[0]
    # elif t_maxint > field.t[-1]:
    #     t_maxint = field.t[-1]
    # ref_frame_index = field.get_t_index(t_maxint)
    # frame on which the pulse is at the focal point
    ref_frame = frames[50]

    # ~~ set up normalization
    fieldmin = np.amin(physutils.get_qty(ref_frame, qty))
    fieldmax = np.amax(physutils.get_qty(ref_frame, qty))
    if qty in {Norm.REAL, Norm.IMAG}:
        if abs(fieldmin) < abs(fieldmax):
            fieldmin = -fieldmax
        else:
            fieldmax = -fieldmin
    norm = vis.get_norm("lin", fieldmin, fieldmax, logrange=5, symlog_linscale=0.03)

    # ~~ set up the figure and the elements using the (arbitrarily choosen) focal frame
    fig, ax = plt.subplots()
    # get the re/im/ampl/phase part asked for
    frame0 = physutils.get_qty(ref_frame, qty)
    pc = ax.pcolormesh(x, y, frames[0], cmap=cmap, norm=norm, shading="nearest")
    txt = ax.text(
        0.1,
        0.1,
        "",
        ha="center",
        va="center",
        transform=ax.transAxes,
        fontdict={"color": "lightgrey", "size": 14},
    )
    cbar = fig.colorbar(pc, ax=ax, extend="max")

    # fig.suptitle(title)
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$y$")
    cbar.set_label("Re(fieldvals)")

    # ~~ initialize the animation:
    def anim_init():
        pc.set_array([])
        txt.set_text("")
        return txt, pc

    # ~~ update the plot to the i-th frame:
    def animate(i):
        print("Processing frame {} out of {}      ".format(i, field.nframes), end="\r")
        frame = frames[i]
        frame = physutils.get_qty(frame, qty)
        values = frame  # [:, :-1]
        pc.set_array(values.flatten())
        txt.set_text("t={:.4f}fs".format(field.t[i]))
        return pc, txt

    im_ani = animation.FuncAnimation(
        fig,
        animate,
        init_func=anim_init,
        frames=field.nframes,
        interval=1000 / fps,
        repeat=True,
        save_count=10,
        blit=True,
    )
    if save:
        if savepath is None:
            raise ValueError("missing savepath")
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        im_ani.save(savepath)
    else:
        plt.show()
    plt.close(fig)


animate()
exit()
# -----------------------
