import argparse
import os
from dataclasses import dataclass
from typing import Callable
from matplotlib import pyplot as plt
import termcolor as tc

import numpy as np

from GaussPulse import C_LIGHT, Comp, Norm, utils, vis, physutils
from GaussPulse.FieldVector import FieldFromSaved, FieldVector
from GaussPulse.InputParse import (
    C_LIGHT_NNORM,
    ConfigParser,
    load_f_from_config_savepath_with_fail_prompt,
)

cmap = vis.blue_black_red_colormap


@dataclass
class Action(object):
    argument: str
    dest: str
    func: Callable


def ACTIONS():
    actions = [
        Action("--energy", "plot_energy", plot_energy),
        Action("--speed", "plot_speed", plot_speed),
        Action("--animate-optax", "animate_optax", animate_optax),
        Action("--animate-slice", "animate_slice", animate_slice),
        Action("--frame-optax", "frame_optax", frame_optax),
        Action("--frame-slice", "frame_slice", frame_slice),
        Action("--animate-optax-line", "animate_optax_line", animate_optax_line),
        Action("--frame-optax-line", "frame_optax_line", frame_optax_line),
        Action("--frame-slice-line", "frame_slice_line", frame_slice_line),
        Action("--point-time-eval", "point_time_eval", point_time_eval),
        Action("--time-eval-z", "time_eval_z", time_eval_z),
        Action("--ampl-ph-slices", "ampl_ph_slices", plot_field_phase_separate),
        Action(
            "--ampl-ph-slices-shared", "ampl_ph_slices_shared", plot_field_phase_shared,
        ),
    ]
    return actions


def main():
    parser = construct_parser("Plot various aspects of the supplied field(s).")

    args = parser.parse_args()
    # args.config_files = [
    #     "/home/nagyg/Codes/gfield/field_configs/new_config.json",
    # ]
    if not args.config_files:
        print("Error: at least one config file must be supplied.")
        return

    if args.outdir is not None and not os.path.isdir(args.outdir):
        os.makedirs(args.outdir)

    for cfile in args.config_files:
        process_config_file(cfile, args)


def process_config_file(cfile, args):
    config = ConfigParser(cfile)
    try:
        f = load_f_from_config_savepath_with_fail_prompt(cfile)
    except FileNotFoundError:
        print("Skipping " + cfile)
        return

    tc.cprint(f"Processing {f.label} (from {cfile})")

    for action in ACTIONS():
        if getattr(args, action.dest):
            action.func(f, args)


def construct_parser(description: str):
    actions = ACTIONS()
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "-o, --outdir",
        dest="outdir",
        action="store",
        type=str,
        default=None,
        const="./",
        nargs="?",
    )
    parser.add_argument(
        "-c, --comp",
        dest="comps",
        action="store",
        type=Comp,
        choices=list(Comp),
        default=[Comp.E_r,],
        nargs="+",
    )
    parser.add_argument(
        "-q, --qty",
        dest="qtys",
        action="store",
        type=Norm.fromstr,
        choices=list(Norm),
        default=["real",],
        nargs="+",
    )
    parser.add_argument(
        "-r, --rcoord", dest="rs", action="store", type=float, nargs="+", default=0.5
    )
    parser.add_argument(
        "-p, --phicoord", dest="phi", action="store", type=float, nargs="+", default=0,
    )
    parser.add_argument(
        "-z, --zcoord", dest="zs", action="store", type=float, nargs="+", default=0
    )
    parser.add_argument(
        "-t, --time", dest="ts", action="store", type=float, nargs="+", default=0
    )
    parser.add_argument("config_files", nargs="+")
    for action in actions:
        parser.add_argument(action.argument, dest=action.dest, action="store_true")
    return parser


def title_to_savepath(title, args, prefix, is_anim=False):

    s = title.replace(r"\lambda_0", "")
    s = s.replace(r"\nu_0^{-1}", "")
    if prefix:
        s = prefix + "_" + s
    s = s.replace(r"$", "")
    s = s.replace(r"(", "")
    s = s.replace(r",", "")
    s = s.replace(r")", "")
    s = s.replace(" ", "_")
    s = s.replace("=", "_")
    s = s.replace(":", "")
    s = s.replace("__", "_")
    if is_anim:
        s = s + ".mp4"
    else:
        s = s + ".png"

    if args.outdir is not None:
        saving = True
        path = os.path.join(args.outdir, s)
    else:
        saving = False
        path = None

    return saving, path


def plot_energy(f, args):
    title = rf"Total energy evolution"
    saving, path = title_to_savepath(title, args, f.label, is_anim=False)
    vis.total_energy(f, time_avg=True, exclude_optax=True, savepath=path, save=saving)


def plot_speed(f, args):
    title = rf"Centroid speed evolution"
    saving, path = title_to_savepath(title, args, f.label, is_anim=False)
    vis.centroid_speed(f, savepath=path, save=saving)


def animate_optax(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            title = rf"${comp.name} {qty.name.lower()}$"
            saving, path = title_to_savepath(title, args, f.label, is_anim=True)
            vis.optax_anim(
                f,
                component=comp,
                qty=qty,
                cmap=cmap,
                title=title,
                save=saving,
                savepath=path,
            )


def animate_slice(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            for z in args.zs:
                title = rf"${comp.name}(z={z:.2f}\lambda_0$) {qty.name.lower()}$"
                saving, path = title_to_savepath(title, args, f.label, is_anim=True)
                vis.adj_anim(
                    z,
                    f,
                    component=comp,
                    qty=qty,
                    cmap=cmap,
                    title=title,
                    save=saving,
                    savepath=path,
                )


def frame_optax(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            for t in args.ts:
                title = (
                    rf"${comp.name}(t={t:.2f}"
                    + r"\nu_0^{-1}"
                    + rf"$) {qty.name.lower()}$"
                )
                saving, path = title_to_savepath(title, args, f.label, is_anim=False)
                vis.optax_frame(
                    f,
                    t,
                    component=comp,
                    qty=qty,
                    cmap=cmap,
                    title=title,
                    save=saving,
                    savepath=path,
                )


def frame_slice(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            for z in args.zs:
                for t in args.ts:
                    title = (
                        rf"${comp.name}(z={z:.2f}\lambda_0$, t={t:.2f}"
                        + r"\nu_0^{-1}"
                        + rf"$) {qty.name.lower()}$"
                    )
                    saving, path = title_to_savepath(
                        title, args, f.label, is_anim=False
                    )
                    vis.adj_frame(
                        z,
                        t,
                        f,
                        component=comp,
                        qty=qty,
                        title=title,
                        save=saving,
                        savepath=path,
                    )


def animate_optax_line(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            for r in args.rs:
                title = rf"${comp.name}(r={r:.2f}\lambda_0$) {qty.name.lower()}$"
                saving, path = title_to_savepath(title, args, f.label, is_anim=True)
                vis.optax_line_anim(
                    f,
                    r,
                    component=comp,
                    qty=qty,
                    title=title,
                    save=saving,
                    savepath=path,
                )


def frame_optax_line(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            for r in args.rs:
                for t in args.ts:
                    title = (
                        rf"${comp.name}(r={r:.2f}\lambda_0$, $t={t:.2f}"
                        + r"\nu_0^{-1}"
                        + rf"$) {qty.name.lower()}"
                    )
                    saving, path = title_to_savepath(
                        title, args, f.label, is_anim=False
                    )
                    vis.optax_line(
                        f,
                        t,
                        r,
                        component=comp,
                        qty=qty,
                        title=title,
                        save=saving,
                        savepath=path,
                    )


def frame_slice_line(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            for z in args.zs:
                for t in args.ts:
                    title = (
                        rf"${comp.name}(z={z:.2f}\lambda_0$, t={t:.2f}$"
                        + r"\nu_0^{-1}"
                        + rf"$) {qty.name.lower()}$"
                    )
                    saving, path = title_to_savepath(
                        title, args, f.label, is_anim=False
                    )
                    vis.adj_line(
                        f,
                        z,
                        t,
                        component=comp,
                        qty=qty,
                        title=title,
                        save=saving,
                        savepath=path,
                    )


def point_time_eval(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            for r in args.rs:
                for z in args.zs:
                    title = (
                        rf"Time evolution: ${comp.name}(r={r:.2f}\lambda_0$, $z={z:.2f}$"
                        + r"\nu_0^{-1}"
                        + rf"$) {qty.name.lower()}$"
                    )
                    saving, path = title_to_savepath(
                        title, args, f.label, is_anim=False
                    )
                    vis.point_time(
                        f,
                        r,
                        z,
                        component=comp,
                        qty=qty,
                        title=title,
                        save=saving,
                        savepath=path,
                    )


def time_eval_z(f, args):
    for qty in args.qtys:
        for comp in args.comps:
            for r in args.rs:
                title = rf"Time evolution: ${comp.name}(r={r:.2f}\lambda_0$) {qty.name.lower()}"
                saving, path = title_to_savepath(title, args, f.label, is_anim=False)
                vis.time_eval_z_line(
                    f, r, component=comp, qty=qty, cmap=cmap, title=title
                )


def plot_field_phase_separate(f: FieldVector, args):
    # ~~ 2d field-phase plots in r-z and x-y plane separately
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    ebound_order = 4
    mpl_dpi = 96
    figxp, figyp = 800, 650
    ampl_cmap = "gnuplot"
    phase_cmap = "jet"

    thetanum = f.mesh.nr
    rticks = f.mesh.rticks
    zticks = f.mesh.zticks
    x, y = vis.spin_rticks(rticks, thetanum)
    r = vis.mirror_rticks(rticks)
    z = f.mesh.zticks
    for zpl in args.zs:
        iz = f.get_z_index(zpl)
        z_sl = zticks[iz]
        t = z_sl / C_LIGHT
        it = f.get_t_index(t)
        t = f.frametimes[it]
        m = f.m
        for comp in args.comps:

            frame = f.get_component_frame(comp, it)

            rz_frame = vis.mirror_frame(frame, m)
            xy_frame = vis.spin_zline(
                frame, z_sl, thetanum, m, interpolate=False, zticks=zticks
            )

            rz_ampl = physutils.get_qty(rz_frame, Norm.ABS)
            rz_ph = physutils.get_qty(rz_frame, Norm.PHASE) / np.pi
            xy_ampl = physutils.get_qty(xy_frame, Norm.ABS)
            xy_ph = physutils.get_qty(xy_frame, Norm.PHASE) / np.pi

            fieldmax = np.amax(rz_ampl)
            rz_ph = np.ma.masked_where(
                rz_ampl < fieldmax * np.e ** -ebound_order, rz_ph
            )
            xy_ph = np.ma.masked_where(
                xy_ampl < fieldmax * np.e ** -ebound_order, xy_ph
            )

            # ~~ set up the figure and the elements
            fig, ((rz_aax, rz_phax), (xy_aax, xy_phax)) = plt.subplots(
                2, 2, sharex="row", sharey="row", constrained_layout=True
            )
            phase_norm = vis.get_norm("lin", -np.pi, np.pi)
            fig.set_size_inches(figxp / mpl_dpi, figyp / mpl_dpi)
            rz_apc = rz_aax.pcolormesh(
                z, r, rz_ampl, cmap=ampl_cmap, shading="nearest"
            )  # , norm=norm)
            rz_ppc = rz_phax.pcolormesh(
                z, r, rz_ph, cmap=phase_cmap, norm=phase_norm, shading="nearest"
            )
            xy_apc = xy_aax.pcolormesh(
                x, y, xy_ampl, cmap=ampl_cmap, shading="nearest"
            )  # , norm=norm)
            xy_ppc = xy_phax.pcolormesh(
                x, y, xy_ph, cmap=phase_cmap, norm=phase_norm, shading="nearest"
            )
            rz_aax.vlines(
                z_sl,
                r[0],
                r[-1],
                colors="white",
                linestyles="dotted",
                linewidths=1,
                alpha=0.8,
            )
            fig.colorbar(rz_apc, ax=rz_aax, aspect=50)
            fig.colorbar(rz_ppc, ax=rz_phax, aspect=50)
            fig.colorbar(xy_apc, ax=xy_aax, aspect=50)
            fig.colorbar(xy_ppc, ax=xy_phax, aspect=50)

            title = (
                rf"{f.label} ${comp.name}$, $t={t:.2f}"
                + r"\nu_0^{-1}"
                + rf"$, $z={z_sl:.2f}\lambda_0$"
            )
            fig.suptitle(title)
            rz_aax.set_xlabel(r"$r (\lambda_0)$")
            rz_aax.set_ylabel(r"$z (\lambda_0)$")
            rz_phax.set_xlabel(r"$r (\lambda_0)$")
            rz_phax.set_ylabel(r"$z (\lambda_0)$")
            xy_aax.set_xlabel(r"$x (\lambda_0)$")
            xy_aax.set_ylabel(r"$y (\lambda_0)$")
            xy_phax.set_xlabel(r"$x (\lambda_0)$")
            xy_phax.set_ylabel(r"$y (\lambda_0)$")

            rz_aax.set_facecolor("black")
            rz_phax.set_facecolor("black")
            xy_aax.set_facecolor("black")
            xy_phax.set_facecolor("black")

            # abar.set_label("Rec(fieldvals)")
            # plt.tight_layout()
            # plt.show()
            # exit()

            saving, savepath = title_to_savepath(title, args, f.label + "_ampphsep")
            if saving:
                if savepath is None:
                    raise ValueError("missing savepath")
                destfolder = os.path.split(savepath)[0]
                if destfolder != "" and not os.path.isdir(destfolder):
                    os.makedirs(destfolder)
                fig.savefig(savepath)
            else:
                plt.show()
            plt.close(fig)


def plot_field_phase_shared(f: FieldVector, args):
    # ~~ 2d field-phase plots in r-z and x-y plane
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print("Creating 2d phase plots with joint ampl/phase...")

    ebound_order = 4
    mpl_dpi = 96
    figxp, figyp = 450, 800
    ampl_cmap = "gnuplot"
    phase_cmap = "jet"
    thetanum = f.mesh.nr
    rticks = f.mesh.rticks
    zticks = f.mesh.zticks

    x, y = vis.spin_rticks(rticks, thetanum)
    r = vis.mirror_rticks(rticks)
    z = f.mesh.zticks
    for zpl in args.zs:
        iz = f.get_z_index(zpl)
        z_sl = zticks[iz]
        t = z_sl / C_LIGHT
        it = f.get_t_index(t)
        t = f.frametimes[it]
        # exit()
        m = f.m
        for comp in args.comps:

            # print(f.frametimes)
            # print(zticks)

            frame = f.get_component_frame(comp, it)

            rz_frame = vis.mirror_frame(frame, m)
            xy_frame = vis.spin_zline(
                frame, z_sl, thetanum, m, interpolate=False, zticks=zticks
            )

            rz_ampl = physutils.get_qty(rz_frame, Norm.ABS)
            rz_ph = physutils.get_qty(rz_frame, Norm.PHASE) / np.pi
            xy_ampl = physutils.get_qty(xy_frame, Norm.ABS)
            xy_ph = physutils.get_qty(xy_frame, Norm.PHASE) / np.pi

            fieldmax = np.amax(rz_ampl)
            rz_ph = np.ma.masked_where(
                rz_ampl < fieldmax * np.e ** -ebound_order, rz_ph
            )
            xy_ph = np.ma.masked_where(
                xy_ampl < fieldmax * np.e ** -ebound_order, xy_ph
            )
            phase_norm = vis.get_norm("lin", -np.pi, np.pi)

            # ~~ set up the figure and the elements
            fig, (rz_ax, xy_ax) = plt.subplots(2, 1, constrained_layout=True)
            fig.set_size_inches(figxp / mpl_dpi, figyp / mpl_dpi)

            rz_apc = rz_ax.pcolormesh(
                z, r[r >= 0], rz_ampl[r >= 0, :], cmap=ampl_cmap, shading="nearest"
            )  # , norm=norm)
            rz_ppc = rz_ax.pcolormesh(
                z,
                r[r < 0],
                rz_ph[r < 0, :],
                cmap=phase_cmap,
                shading="nearest",
                norm=phase_norm,
            )

            xp = x
            xn = x
            ampp = np.ma.masked_where(x <= 0, xy_ampl)
            phn = np.ma.masked_where(x >= 0, xy_ph)

            xy_apc = xy_ax.pcolormesh(xp, y, ampp, cmap=ampl_cmap, shading="nearest")
            xy_ppc = xy_ax.pcolormesh(
                xn, y, phn, cmap=phase_cmap, shading="nearest", norm=phase_norm
            )
            rz_ax.vlines(
                z_sl,
                r[0],
                r[-1],
                colors="white",
                linestyles="dotted",
                linewidths=1,
                alpha=0.8,
            )
            fig.colorbar(rz_apc, ax=rz_ax, aspect=30)
            # fig.colorbar(rz_ppc, ax=rz_ax, aspect=50)
            # fig.colorbar(xy_apc, ax=xy_ax, aspect=50)
            fig.colorbar(xy_ppc, ax=xy_ax, aspect=30)

            title = (
                rf"{f.label} ${comp.name}$, $t={t:.2f}"
                + r"\nu_0^{-1}"
                + rf"$, $z={z_sl:.2f}\lambda_0$"
            )
            fig.suptitle(title)
            rz_ax.set_xlabel(r"$r (\lambda_0)$")
            rz_ax.set_ylabel(r"$z (\lambda_0)$")
            xy_ax.set_xlabel(r"$x (\lambda_0)$")
            xy_ax.set_ylabel(r"$y (\lambda_0)$")

            rz_ax.set_facecolor("black")
            xy_ax.set_facecolor("black")

            # abar.set_label("Rec(fieldvals)")
            # plt.tight_layout()

            saving, savepath = title_to_savepath(title, args, f.label + "_ampphjoint")
            if saving:
                if savepath is None:
                    raise ValueError("missing savepath")
                destfolder = os.path.split(savepath)[0]
                if destfolder != "" and not os.path.isdir(destfolder):
                    os.makedirs(destfolder)
                fig.savefig(savepath)
            else:
                plt.show()
            plt.close(fig)


if __name__ == "__main__":
    main()
