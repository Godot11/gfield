#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys

import numpy as np
import termcolor as tc
from GaussPulse import Comp, Norm, physutils, utils, vis
from GaussPulse.InputParse import C_LIGHT_NNORM, ConfigParser, output_path_exists_prompt

# C_LIGHT_NNORM = 1  # const.c * const.femto / const.micro
EPSILON0 = 1  # TODO


MAX_MEMORY = 16731832320 * 0.7


SAVE = True
PLOT_SPECTUM = False  # TODO BROKEN
T_ANIM = 4
MAX_MEMORY_WARN_RATIO = 0.5

# ~~ read config file(s)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def construct_parser(description: str):
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("--no-multicore", dest="no_multicore", action="store_true")
    parser.add_argument(
        "-o, --outdir",
        dest="outdir",
        action="store",
        type=str,
        default=None,
        const="./",
        nargs="?",
    )
    parser.add_argument(
        "-c, --comp",
        dest="comps",
        action="store",
        type=Comp,
        choices=list(Comp),
        default=[Comp.E_r,],
        nargs="+",
    )
    parser.add_argument(
        "-r, --rcoord", dest="rs", action="store", type=float, nargs="+", default=(0.5,)
    )
    parser.add_argument(
        "-p, --phicoord",
        dest="phi",
        action="store",
        type=float,
        nargs="+",
        default=(0,),
    )
    parser.add_argument(
        "-z, --zcoord", dest="zs", action="store", type=float, nargs="+", default=(0,)
    )
    parser.add_argument(
        "-s, --meshsize", dest="meshsize", action="store", type=int, default=None,
    )
    parser.add_argument("config_files", nargs="+")
    return parser


def title_to_savepath(title, args, prefix, is_anim=False):

    s = title.replace(r"\lambda_0", "")
    s = s.replace(r"\nu_0^{-1}", "")
    if prefix:
        s = prefix + "_" + s
    s = s.replace(r"$", "")
    s = s.replace(r"(", "")
    s = s.replace(r",", "")
    s = s.replace(r")", "")
    s = s.replace(" ", "_")
    s = s.replace("=", "_")
    s = s.replace(":", "")
    s = s.replace("__", "_")
    if is_anim:
        s = s + ".mp4"
    else:
        s = s + ".png"

    if args.outdir is not None:
        saving = True
        path = os.path.join(args.outdir, s)
    else:
        saving = False
        path = None

    return saving, path


def process_config_file(config_file, args):
    config = ConfigParser(config_file)
    f = config.field
    nmesh = args.meshsize
    if nmesh is not None:
        mesh = f.mesh.resample(nmesh, nmesh)
    else:
        mesh = f.mesh
    multicore = not args.no_multicore
    if args.meshsize is not None:
        mesh = f.mesh.resample(args.meshsize, args.meshsize)
    else:
        mesh = (f.mesh,)
    for comp in args.comps:
        for r in args.rs:
            for z in args.zs:
                title = rf"Pulse shape: {f.label} ${comp.name}$ at $r={r}, \varphi={args.phi}, z={z}$"
                saving, savepath = title_to_savepath(title, args, f.label)
                # savepath = config.savepath
                vis.pulse_shape(
                    f,
                    comp,
                    r=r,
                    phi=args.phi,
                    z=z,
                    mesh=mesh if args.meshsize is not None else f.mesh,
                    multicore=multicore,
                    title=title,
                    save=saving,
                    savepath=savepath,
                )  # , mesh=f.mesh)


def main():
    parser = construct_parser("plot the pulse shape and spectrum in a specific point")

    args = parser.parse_args()

    cfiles = args.config_files
    if not cfiles:
        print("Error: at least one config file must be supplied.")
        return

    for cfile in cfiles:
        process_config_file(cfile, args)


if __name__ == "__main__":
    main()
