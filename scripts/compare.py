"""
Compare results on shared plots .

python compare.py --compare-comp will generate a comparsion plot for 1 component;
python compare.py --compare-all-comp will do it for all components.

Usage: e.g.:
python compare.py --compare-comp -c e_r e_z -q real -z 0.0 -t 0.0 pulse1.json pulse2.json
will compare the pulse amplitude in the focal plane.

"""

import argparse
import os
from dataclasses import dataclass
from typing import Callable, List

import numpy as np
from GaussPulse import C_LIGHT, Comp, Norm, utils, vis, physutils
from GaussPulse.FieldVector import FieldFromSaved, FieldVector
from GaussPulse.InputParse import (
    C_LIGHT_NNORM,
    ConfigParser,
    load_f_from_config_savepath_with_fail_prompt,
)
from matplotlib import pyplot as plt

cmap = vis.blue_black_red_colormap


@dataclass
class Action(object):
    argument: str
    dest: str
    func: Callable


def ACTIONS():
    actions = [
        # Action("--energy", "plot_energy", plot_energy),
        # Action("--speed", "plot_speed", plot_speed),
        # Action("--animate-optax", "animate_optax", animate_optax),
        # Action("--animate-slice", "animate_slice", animate_slice),
        # Action("--frame-optax", "frame_optax", frame_optax),
        # Action("--frame-slice", "frame_slice", frame_slice),
        # Action("--animate-optax-line", "animate_optax_line", animate_optax_line),
        # Action("--frame-optax-line", "frame_optax_line", frame_optax_line),
        Action("--compare-comp", "compare_comp", compare_comp),
        Action("--compare-all-comps", "compare_all_comp", compare_all_comps),
        # Action("--point-time-eval", "point_time_eval", point_time_eval),
        # Action("--time-eval-z", "time_eval_z", time_eval_z),
    ]
    return actions


def main():
    parser = construct_parser("Compare results on shared plots.")

    args = parser.parse_args()
    # args.config_files = [
    #     "/home/nagyg/Codes/gfield/field_configs/new_config.json",
    # ]
    if not args.config_files:
        print("Error: at least one config file must be supplied.")
        return

    if args.outdir is not None and not os.path.isdir(args.outdir):
        os.makedirs(args.outdir)

    for cfile in args.config_files:
        process_config_file(cfile, args)


def process_config_file(cfile, args):
    config = ConfigParser(cfile)
    try:
        f = load_f_from_config_savepath_with_fail_prompt(cfile)
    except FileNotFoundError:
        print("Skipping " + cfile)
        return

    for action in ACTIONS():
        if getattr(args, action.dest):
            action.func(f, args)


def construct_parser(description: str):
    actions = ACTIONS()
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "-o, --outdir",
        dest="outdir",
        action="store",
        type=str,
        default=None,
        const="./",
        nargs="?",
    )
    parser.add_argument(
        "-c, --comp",
        dest="comps",
        action="store",
        type=Comp,
        choices=list(Comp),
        default=[Comp.E_r,],
        nargs="+",
    )
    parser.add_argument(
        "-q, --qty",
        dest="qtys",
        action="store",
        type=Norm.fromstr,
        choices=list(Norm),
        default=["real",],
        nargs="+",
    )
    parser.add_argument(
        "-r, --rcoord", dest="rs", action="store", type=float, nargs="+", default=0.5
    )
    parser.add_argument(
        "-p, --phicoord", dest="phi", action="store", type=float, nargs="+", default=0,
    )
    parser.add_argument(
        "-z, --zcoord", dest="zs", action="store", type=float, nargs="+", default=0
    )
    parser.add_argument(
        "-t, --time", dest="ts", action="store", type=float, nargs="+", default=0
    )

    parser.add_argument(
        "--normalize-energy", dest="normalize_energy", action="store_true"
    )

    parser.add_argument("config_files", nargs="+")
    for action in actions:
        parser.add_argument(action.argument, dest=action.dest, action="store_true")
    return parser


def title_to_savepath(title, args, prefix, is_anim=False):

    s = title.replace(r"\lambda_0", "")
    s = s.replace(r"\nu_0^{-1}", "")
    if prefix:
        s = prefix + "_" + s
    s = s.replace(r"$", "")
    s = s.replace(r"(", "")
    s = s.replace(r",", "")
    s = s.replace(r")", "")
    s = s.replace(" ", "_")
    s = s.replace("=", "_")
    s = s.replace(":", "")
    if is_anim:
        s = s + ".mp4"
    else:
        s = s + ".png"

    if args.outdir is not None:
        saving = True
        path = os.path.join(args.outdir, s)
    else:
        saving = False
        path = None

    return saving, path


def compare_comp(fs: List[FieldVector], args):
    # # ~~ Compare the fields
    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print("Creating comparsion plots for Vikartovsky fields...")

    # savefolder = os.path.join(ssavefolder, "vik_comparsion")
    figxp, figyp = 1500, 800
    plotzs = args.zs
    mpl_dpi = 96

    for qty in args.qtys:
        for zpl in plotzs:
            for comp in args.comps:
                fig, ax = plt.subplots()
                fig.set_size_inches(figxp / mpl_dpi, figyp / mpl_dpi)
                for f in fs:
                    if args.normalize_energy:
                        f = f.set_energy(1, time_avg=True, exclude_optax=True)
                    print(f)
                    iz = f.get_z_index(zpl)
                    z_sl = f.z[iz]
                    t = z_sl / C_LIGHT
                    it = f.get_t_index(t)
                    t = f.frametimes[it]
                    m = f.m
                    rticks = vis.mirror_rticks(f.r)
                    frame = f.get_component_frame(comp, it)
                    line = vis.mirror_frame(frame, m)[:, iz].transpose()
                    line = physutils.get_qty(line, qty)

                    ax.plot(rticks, line, label=f.label, alpha=0.8 if m != 0 else 1)
                    ax.set_title(comp.name)

                labels = ""
                for f in fs:
                    labels += f.label + ", "
                title = (
                    labels + rf"{comp.name}, z={z_sl:.2f}\lambda0, t={t:.2f}\nu_0^{-1}$"
                )
                fig.suptitle(title)
                ax.legend()
                # plt.tight_layout()
                saving, savepath = title_to_savepath(title, args, f.label)
                if saving:
                    destfolder = os.path.split(savepath)[0]
                    if destfolder != "" and not os.path.isdir(destfolder):
                        os.makedirs(destfolder)
                    plt.savefig(savepath)
                else:
                    plt.show()
                plt.close(fig)


def compare_all_comps(fs: List[FieldVector], args):
    # # ~~ Compare the fields
    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print("Creating comparsion plots for Vikartovsky fields...")

    # savefolder = os.path.join(ssavefolder, "vik_comparsion")
    figxp, figyp = 1500, 800
    plotzs = args.z
    mpl_dpi = 96

    for qty in args.qtys:
        for zpl in plotzs:
            for f in fs:
                fig, (eaxes, baxes) = plt.subplots(2, 3, constrained_layout=True)
                fig.set_size_inches(figxp / mpl_dpi, figyp / mpl_dpi)
                axes = np.concatenate((eaxes, baxes))
                comps = list(Comp)
                if args.normalize_energy:
                    f = f.set_energy(1, time_avg=True, exclude_optax=True)
                iz = f.get_z_index(zpl)
                z_sl = f.z[iz]
                t = z_sl / C_LIGHT
                print(t)
                it = f.get_t_index(t)
                t = f.frametimes[it]
                m = f.m
                rticks = vis.mirror_rticks(f.r)

                for ax, comp in zip(axes, comps):
                    frame = f.get_component_frame(comp, it)
                    line = vis.mirror_frame(frame, m)[:, iz].transpose()
                    line = physutils.get_qty(line, qty)

                    ax.plot(rticks, line, label=f.label, alpha=0.8 if m != 0 else 1)
                    ax.set_title(comp.value.capitalize())

            labels = ""
            for f in fs:
                labels += f.label + ", "
            title = labels + rf"z={z_sl:.2f}\lambda0, t={t:.2f}\nu_0^{-1}$"
            fig.suptitle(title)
            ax.legend()
            # plt.tight_layout()
            saving, savepath = title_to_savepath(title, args, f.label)
            if saving:
                destfolder = os.path.split(savepath)[0]
                if destfolder != "" and not os.path.isdir(destfolder):
                    os.makedirs(destfolder)
                plt.savefig(savepath)
            else:
                plt.show()
            plt.close(fig)


if __name__ == "__main__":
    main()
