#!/bin/bash
CONFIGS=(
    "test_levy"
    "test_pu"
    "test_vik"
)
SAVEPATH="./output"

comps=("e_r")
qtys=("real")
r=(1.0)
z=(0)
t=(0)

ACTIONS=(
    "compare-comp"
    "compare-all-comps"
)

SAVE=false
# SAVE=true

CONFIG_EXT=".json"
CONFIG_PATH="/home/nagyg/Codes/gfield/field_configs/"
confiles=("${CONFIGS[@]/%/$CONFIG_EXT}")
configs=("${confiles[@]/#/$CONFIG_PATH}")
acts=("${ACTIONS[@]/#/"--"}")

if [ "$SAVE" = true ]; then
    saveflag="-o $SAVEPATH"
else
    saveflag=""
fi

cmnd="scripts/compare.py $saveflag -c ${comps[@]} -q ${qtys[@]} -r ${r[@]} -z ${z[@]} -t ${t[@]} ${acts[@]} ${configs[@]}"
echo "python" $cmnd
python $cmnd

# echo ${configs[@]}
# echo ${acts[@]}
