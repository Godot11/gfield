#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import enum
import os
from copy import copy
from typing import Dict

import matplotlib as mpl
import numpy as np
from matplotlib import pyplot as plt
from scipy import constants as const
from GaussPulse.FieldVector import FieldFromSaved

# from GaussPulse.VikartovskyVPField import er_v as vp_e_r
from GaussPulse import (
    C_LIGHT,
    Comp,
    FieldVector,
    MonoField,
    Norm,
    Parameters,
    ioutils,
    meshfield,
    physutils,
    utils,
    vis,
)
from GaussPulse.InputParse import (
    C_LIGHT_NNORM,
    ConfigParser,
    load_f_from_config_savepath_with_fail_prompt,
)


def construct_parser(description: str):
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "-o, --outdir",
        dest="outdir",
        action="store",
        type=str,
        default=None,
        nargs="?",
    )
    parser.add_argument(
        "-c, --comp",
        dest="comps",
        action="store",
        type=Comp,
        choices=list(Comp),
        default=[Comp.E_r,],
        nargs="+",
    )
    parser.add_argument(
        "-q, --qty",
        dest="qtys",
        action="store",
        type=Norm.fromstr,
        choices=list(Norm),
        default=["real",],
        nargs="+",
    )
    parser.add_argument(
        "-t, --times", dest="ts", action="store", type=float, nargs="+", default=0
    )
    parser.add_argument(
        "-l, --zlims",
        dest="zlims",
        action="store",
        type=float,
        nargs="+",
        const=None,
        default=None,
    )
    parser.add_argument(
        "-p, --phicoord", dest="phi", action="store", type=float, nargs="+", default=0,
    )
    parser.add_argument("config_files", nargs="+")
    return parser


def process_config_file(cfile, args):
    config = ConfigParser(cfile)
    try:
        f = load_f_from_config_savepath_with_fail_prompt(cfile)
    except FileNotFoundError:
        print("Skipping " + cfile)
        return

    for norm in args.qtys:
        plot_snapshots(f, args, norm)


def main():
    parser = construct_parser("Plot various aspects of the supplied field(s).")

    args = parser.parse_args()
    # args.config_files = [
    #     "/home/nagyg/Codes/gfield/field_configs/new_config.json",
    # ]
    if not args.config_files:
        print("Error: at least one config file must be supplied.")
        return

    if args.outdir is not None and not os.path.isdir(args.outdir):
        os.makedirs(args.outdir)

    for cfile in args.config_files:
        process_config_file(cfile, args)


def title_to_savepath(title, args, prefix, is_anim=False):

    s = title.replace(r"\lambda_0", "")
    s = s.replace(r"\nu_0^{-1}", "")
    if prefix:
        s = prefix + "_" + s
    s = s.replace(r"$", "")
    s = s.replace(r"(", "")
    s = s.replace(r",", "")
    s = s.replace(r")", "")
    s = s.replace(" ", "_")
    s = s.replace("=", "_")
    s = s.replace(":", "")
    if is_anim:
        s = s + ".mp4"
    else:
        s = s + ".png"

    if args.outdir is not None:
        saving = True
        path = os.path.join(args.outdir, s)
    else:
        saving = False
        path = None

    return saving, path


def plot_snapshots(f: FieldFromSaved, args, qty):

    np.seterr(divide="raise")

    field_cmap = vis.blue_black_red_colormap
    ampl_cmap = vis.cyanogen_cmap
    phase_cmap = "jet"
    mpl.style.use("seaborn")

    mpl_dpi = 96
    figxpixsize = 1500
    figypixsize = 800

    basefolder = args.outdir

    plotts = args.ts
    plotzs = np.array(plotts) * C_LIGHT

    mesh = f.mesh
    rticks = vis.mirror_rticks(mesh.rticks)
    zticks = mesh.zticks
    dr = mesh.dr
    dz = mesh.dz

    # ~~ Snapshots
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    """
    Plot snapshots of the pulse next to each other, on the same scale.
    """
    comps = args.comps

    plt.style.use("dark_background")
    zlims = args.zlims
    if not zlims:
        zlims = np.array([[mesh.zmin, mesh.zmax] for _ in plotzs])
    else:
        if len(zlims) % 2:
            print("Error: number of zlims must be even. Aborting.")
            exit()
        else:
            zlims = np.reshape(zlims, (len(zlims) // 2, 2))
        if len(zlims) != len(plotzs):
            print(
                "Error: number of zlims does not correspond to the number of time points. Aborting."
            )
            exit()

    figxp, figyp = 900, 500
    axdist = 0.05
    m = f.m

    plot_rpoints = vis.mirror_rticks(mesh.rticks)
    plot_zpoints = mesh.zticks

    # set ratios of the subplots so that the x axis stays the same size even
    #   for different z ranges
    absolute_zmin, absolute_zmax = (zlims[0][0], zlims[-1][1])
    ratios = []
    for zlim in zlims:
        ratios.append(abs(zlim[1] - zlim[0]) / (absolute_zmax - absolute_zmin))

    # create the plots, next to each other, ampl in the upper and relative phase
    #   in the lower row
    fig, amaxes = plt.subplots(
        nrows=len(comps),
        ncols=len(plotzs),
        sharex="col",
        sharey="all",  # "row",
        gridspec_kw={"width_ratios": ratios},
    )
    #!!
    # fig, (amaxes, phaxes) = plt.subplots(nrows=2, ncols=len(frametimes),
    #    sharey="row", gridspec_kw = {'width_ratios': ratios})

    # for i, (amax, phax, frametime, zlim) in enumerate(zip(amaxes, phaxes, frametimes, zlims)):
    for icomp, comp in enumerate(comps):
        for i, (amax, zpl, zlim) in enumerate(zip(amaxes[icomp], plotzs, zlims)):

            iz = f.get_z_index(zpl)
            z_sl = zticks[iz]
            t = z_sl / C_LIGHT
            it = f.get_t_index(t)
            frametime = f.frametimes[it]
            m = f.m
            it0 = f.get_t_index(0)

            frame = f.get_component_frame(comp, it)
            line = vis.mirror_frame(frame, m)[:, iz].transpose()
            line = physutils.get_qty(line, qty)

            # get the frame and it's min and max value
            frindex = f.get_t_index(frametime)
            frame = f.get_component_frame(comp, frindex)
            # frame = elmfield.get_intensity_frame(frindex, "energy")
            # comoving = meshfield.get_comoving_frame(frame, frametime, fieldparams, mesh)
            # zoms, fft_frame = meshfield.get_z_fourier_frame(comoving, fieldparams, mesh)
            frame = physutils.get_qty(vis.mirror_frame(frame, m), qty)
            # fft_frame = mirror_frame(fft_frame)
            fieldmin = np.amin(frame)
            fieldmax = np.amax(frame)
            frameampl = np.abs(frame)
            # if qty in {"re", "im"}:
            #     if abs(fieldmin) < abs(fieldmax):
            #         fieldmin = -fieldmax
            #     else:
            #         fieldmax = -fieldmin

            # get the relative phase, mask where the field amplitude is smaller
            #   than fieldmax/maskrange
            # mask = frameampl > fieldmax * maskrange
            # relphase = np.ma.array(np.abs(fft_frame), mask=mask)

            # Choose the normalization type according to `scale` (phase is always
            #   plotted on linear scale)
            # phasenorm = get_norm('lin', 0, 1)
            mn, mx = fieldmin, fieldmax
            if qty is Norm.REAL or qty is Norm.IMAG:
                mx = mx if abs(mx) > abs(mn) else abs(mn)
                mn = mn if abs(mn) > abs(mx) else -abs(mx)
            norm = vis.get_norm("lin", mn, mx)

            # Create the amplitude plot
            amax.pcolormesh(
                plot_zpoints,
                plot_rpoints,
                frame,
                cmap=field_cmap,
                norm=norm,
                shading="nearest",
            )
            # amax.vlines(
            #     z_sl,
            #     plot_rpoints[0],
            #     plot_rpoints[-1],
            #     colors="white",
            #     linestyles="dotted",
            #     linewidths=1,
            #     alpha=0.3,
            # )
            amax.set_xlim(zlim[0], zlim[1])
            if icomp == 0:
                amax.set_title("t = {:.1f}fs".format(frametime))
            # amax.spines["bottom"].set_color("#dddddd")
            # amax.spines["left"].set_color("#dddddd")
            amax.spines["bottom"].set_visible(True)
            amax.spines["top"].set_visible(False)

            if i < len(plotzs) - 1:
                amax.spines["right"].set_visible(False)
            if i != 0:
                amax.spines["left"].set_visible(False)
                amax.tick_params(left=False)
            # only label the rightmost plot's y ax
            if i == 0:
                amax.set_ylabel(r"$r (\lambda_0)$")
            # else:
            #     ax.set_yticks([])
            # label the x ax in the center
            if i == len(amaxes) // 2 and icomp == len(comps) - 1:
                amax.set_xlabel(r"$z (\lambda0)$")

    # zrecbar = fig.colorbar(zre_plot, ax=zreax, extend='max')
    # zrecbar.set_label("Re(" + compstr[component] +") (arb. units)")

    # decorate

    # compstr = {'e_r': '$E_r$', 'e_phi': '$E_{phi}$', 'e_z': '$E_z$',
    #             'b_r': '$B_r$', 'b_phi': '$B_{phi}$', 'b_z': '$B_z$'}
    title = f.label + " " + rf"${comp.name}$" + f" {qty.name.lower()}"
    fig.suptitle(title)
    fig.set_size_inches(figxp / mpl_dpi, figyp / mpl_dpi)
    fig.subplots_adjust(wspace=axdist)
    plt.tight_layout()

    if args.outdir is not None:
        savepath = title_to_savepath(title, args, f.label)
        destfolder = os.path.split(savepath)[0]
        if destfolder != "" and not os.path.isdir(destfolder):
            os.makedirs(destfolder)
        plt.savefig(savepath)
    else:
        plt.show()

    plt.close(fig)


if __name__ == "__main__":
    main()
