#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys

import numpy as np
import termcolor as tc
from GaussPulse import Comp, Norm, physutils, utils, vis
from GaussPulse.InputParse import C_LIGHT_NNORM, ConfigParser, output_path_exists_prompt

# C_LIGHT_NNORM = 1  # const.c * const.femto / const.micro
EPSILON0 = 1  # TODO


MAX_MEMORY = 16731832320 * 0.7


SAVE = True
PLOT_SPECTUM = False  # TODO BROKEN
T_ANIM = 4

comp = Comp.E_r
r = 1.0
z = 0.0
t = 0.0


def check_available_memory():
    if PSUTIL_AVAILABLE:
        mem = psutil.virtual_memory()
        return mem.available
    else:
        return MAX_MEMORY


try:
    import psutil
except ImportError:
    PSUTIL_AVAILABLE = False
else:
    PSUTIL_AVAILABLE = True


MAX_MEMORY_WARN_RATIO = 0.5

# ~~ read config file(s)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


def process_config_file(config_file, args):
    config = ConfigParser(config_file)
    f = config.field
    multicore = not args.no_multicore
    savepath = config.savepath

    tc.cprint(f"Processing {f.label} (from {config_file})", "yellow")

    membytes = 2 * np.empty((f.nr, f.nz, f.nframes), dtype="complex64").nbytes
    mb_size = membytes / 1048576
    print(
        tc.colored("Estimated memory usage: ", "blue") + f"{membytes / 1048576:.1f} MiB"
    )
    if membytes > check_available_memory() * MAX_MEMORY_WARN_RATIO:
        q = (
            f"Estimated memory usage is over {MAX_MEMORY_WARN_RATIO*100:.0f}% "
            "of available memory; continue?"
        )
        if not utils.query_yes_no(q, default="no"):
            print("Skipping " + config_file)
            return

    if not args.show_only:
        try:
            output_path_exists_prompt(savepath)
        except FileExistsError:
            print("Aborting.")
            exit()

    f.calculate_field(show_progress=True, show_stats=True, multicore=multicore)
    ifr = f.get_t_index(0)

    if config.energy is not None:
        if not config.normalized:
            energy = physutils.normalize(
                config.lambda0,
                config.lightspeed,
                unit_type="energy",
                vals=config.energy,
                epsilon0=EPSILON0,
            )
        else:
            energy = config.energy
        f = f.set_energy(
            config.energy, ref_frame_i=ifr, time_avg=True, exclude_optax=False
        )

    if not args.show_only:
        f.save(savepath)
        print("Field saved to " + savepath)

    else:

        if PLOT_SPECTUM:
            vis.pulse_shape(f, comp, r=r, phi=0, z=z)  # , mesh=f.mesh)
        # ifr = f.get_t_index(0)
        # en1 = f.get_frame_energy(ifr, time_avg=True, exclude_optax=False)
        # f = f.set_energy(
        #     config.energy, ref_frame_i=ifr, time_avg=True, exclude_optax=False
        # )
        # en2 = f.get_frame_energy(ifr, time_avg=True, exclude_optax=False)

        # print(en1, en2, config.energy)
        # vis.total_energy(f)
        # vis.total_energy(f2)
        # exit()

        # if not config.normalized:
        #     f = f.denormalize(config.lambda0, C_LIGHT_NNORM)
        # for comp in f.nonzero_comps:
        vis.optax_line(f, t, r, comp, Norm.REAL, title=f"z line (r={r:.2f}, t={t:.2f})")
        vis.point_time(f, r, z, comp, Norm.REAL, title=f"t line (r={r:.2f}, z={z:.2f})")
        # vis.optax_frame(
        #     f,
        #     0,
        #     comp,
        #     Norm.REAL,
        #     scale="lin",  # "symlog",
        #     logrange=2,
        #     cmap=vis.blue_black_red_colormap,
        #     title=comp,
        #     # fps=fps,
        # )
        vis.optax_anim(
            f,
            comp,
            Norm.REAL,
            scale="lin",
            logrange=2,
            cmap=vis.blue_black_red_colormap,
            title=comp,
            fps=f.nframes / T_ANIM,
        )


def main():
    parser = argparse.ArgumentParser(description="Process some integers.")
    parser.add_argument("config_files", nargs="*")
    parser.add_argument("--no-multicore", dest="no_multicore", action="store_true")
    parser.add_argument("--show-only", dest="show_only", action="store_true")
    args = parser.parse_args()

    cfiles = args.config_files
    if not cfiles:
        print("Error: at least one config file must be supplied.")
        return

    for cfile in cfiles:
        process_config_file(cfile, args)


print(__name__)
if __name__ == "__main__":
    main()
