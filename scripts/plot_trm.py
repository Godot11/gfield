import sys
import numpy as np
from matplotlib import pyplot as plt
from scipy import special as sp
from GaussPulse.Fields.LevyRadpolField import e_rdl_r0, e_rdl_r2, e_rdl_r
from GaussPulse.Parameters import LevyParameters, RwParameters
from GaussPulse import OMEGA_0
from GaussPulse.Fields import RWFields


def apod_pu(theta, alpha, beta):
    p, l = 0, 1
    b = beta
    a = alpha
    btpera = beta * np.sin(theta) / np.sin(a)
    return (
        btpera
        / np.sin(a)
        * np.exp(-(btpera ** 2))
        * sp.eval_genlaguerre(p, l, 2 * btpera ** 2)
    )


def apod_pu_fix(theta, alpha, beta):
    p, l = 0, 1
    btpera = beta * np.tan(theta) / np.tan(alpha)
    return (
        btpera
        / np.tan(alpha)
        * np.exp(-(btpera ** 2))
        * sp.eval_genlaguerre(p, l, 2 * btpera ** 2)
    )


def apod_genlg(theta, alpha, beta, p, m):
    m = abs(m)
    btpera = beta * np.tan(theta) / np.tan(alpha)
    normfact = np.sqrt(
        2 ** (m + 1) * np.math.factorial(p) / (np.pi * np.math.factorial(p + m))
    )
    return (
        normfact
        * (beta / np.tan(alpha)) ** (m + 1)
        * (np.tan(theta)) ** m
        * np.exp(-(btpera ** 2))
        * sp.eval_genlaguerre(p, m, 2 * btpera ** 2)
    )


def apod_eljiah(theta, alpha, beta):
    # btpera = beta * np.sin(theta) / np.sin(alpha)
    btpera = beta * np.tan(theta) / np.tan(alpha)
    return sp.jv(1, 2 * btpera) * np.exp(-(btpera ** 2))


def levyint(theta, thetadiv, z):

    params = LevyParameters(thetadiv)
    omega = OMEGA_0

    z = 100
    r = z * np.tan(theta)
    er = e_rdl_r0(r, 0, z, omega, params)
    return np.abs(er)


# def levyint2(theta, thetadiv):

#     omega = OMEGA_0
#     thetad = np.tan(np.deg2rad(thetadiv))
#     return np.abs(np.tan(theta) * (thetad ** 2 / 2 * np.tan(theta) ** 2 - 1))
def levyint2(theta, thetadiv):
    return (
        np.tan(theta)
        * np.abs(-2 + np.tan(theta) ** 2 * thetadiv ** 2)
        * np.exp(-np.tan(theta) ** 2)
    )  # / (k * z))


def apod_rw(theta, alpha, beta, p, m, rfunc):
    na = np.sin(alpha)
    pms = RwParameters(beta, na, p, m, rfunc)
    omega = OMEGA_0
    return np.array([*map(lambda theta: RWFields.R_func(theta, omega, pms), theta)])
    # return RWFields.transmission_R(theta, omega, pms)


def norm(a):
    return a / np.amax(np.abs(a))


def plot_apod():
    theta = 30
    xnum = 400
    xmax = 4
    f = 2
    r_aperture = 2
    r_beam = f * np.tan(np.deg2rad(theta))
    beta = r_aperture / r_beam
    alpha = np.arctan(r_aperture / f)
    thetadiv = np.rad2deg(np.arctan(r_beam / f))
    print(f"beta={beta}, alpha={alpha}, theta={thetadiv}")
    x = np.linspace(-xmax, xmax, xnum)
    theta = np.abs(np.arctan(x / f))

    # print(apod_rw(np.array([theta[10]]), alpha, beta, 0, 0, rfunc="levy"))
    # return

    plots = {
        # "Pu": apod_pu(theta, alpha, beta),
        # "Pu fix": apod_pu_fix(theta, alpha, beta),
        # "gen lag l=0, m=1": apod_genlg(theta, alpha, beta, 0, 1),
        # "Levy": levyint(theta, thetadiv, 1000),
        # "Levy zlim": levyint2(theta, thetadiv),
        # "Eljiah": apod_eljiah(theta, alpha, beta),
        # "RW flat": apod_rw(theta, alpha, beta, 0, 0, rfunc="flat"),
        "RW Levy": apod_rw(theta, alpha, beta, 0, 0, rfunc="levy"),
        # "RW p=0, m=0": apod_rw(theta, alpha, beta, p=0, m=0, rfunc="GenLG"),
        "RW p=0, m=1": apod_rw(theta, alpha, beta, p=0, m=1, rfunc="GenLG"),
        "RW p=0, m=2": apod_rw(theta, alpha, beta, p=0, m=2, rfunc="GenLG"),
        "RW p=0, m=3": apod_rw(theta, alpha, beta, p=0, m=3, rfunc="GenLG"),
        # "RW p=0, m=4": apod_rw(theta, alpha, beta, p=0, m=4, rfunc="GenLG"),
    }

    for label, y in plots.items():
        plt.plot(x, norm(y), label=label)
        # plt.plot(x, (y), label=label)

    plt.legend()

    plt.axvline(
        -r_aperture, linestyle="--", linewidth=0.5, color="red",
    )
    plt.axvline(
        r_aperture, linestyle="--", linewidth=0.5, color="red",
    )
    plt.axvline(
        -r_beam, linestyle="--", linewidth=0.5, color="grey",
    )
    plt.axvline(
        r_beam, linestyle="--", linewidth=0.5, color="grey",
    )
    # plt.axhline(y_1e, linestyle="--", linewidth=0.5, color="grey")
    plt.show()


def plot_integrand():
    n = 100
    f = 4
    r_aperture = 4
    r_beam = 2
    beta = r_aperture / r_beam
    alpha = np.arctan(r_aperture / f)
    na = np.sin(alpha)
    thetadiv = np.rad2deg(np.arctan(r_beam / f))
    print(f"beta={beta}, alpha={alpha}, theta={thetadiv}")

    pms = RwParameters(beta, na)
    theta = np.linspace(0, alpha, n)
    intg = np.empty_like(theta, dtype="complex")
    for i in range(n):
        intg[i] = RWFields.integrand(
            theta[i],
            r=0.5,
            phi=1,
            z=10,
            omega=OMEGA_0,
            pms=pms,
            component=RWFields.COMP_R,
        )
    plt.plot(theta, intg.real, label="real")
    plt.plot(theta, intg.imag, label="imag")
    plt.legend()
    plt.show()


def plot_integral():
    n = 100
    f = 4
    r_aperture = 4
    r_beam = 2
    beta = r_aperture / r_beam
    alpha = np.arctan(r_aperture / f)
    na = np.sin(alpha)
    thetadiv = np.rad2deg(np.arctan(r_beam / f))
    print(f"beta={beta}, alpha={alpha}, theta={thetadiv}")

    pms = RwParameters(beta, na)
    rs = np.linspace(0, 10, n)
    intg = np.empty_like(rs, dtype="complex")
    for i in range(n):
        intg[i] = RWFields.rw_integral(
            RWFields.COMP_R,
            r=rs[i],
            phi=1,
            z=10,
            omega=OMEGA_0,
            pms=pms,
            warn_subdvs=True,
            warn_roundoff=True,
        )
    plt.plot(rs, intg.real, label="real")
    plt.plot(rs, intg.imag, label="imag")
    plt.legend()
    plt.show()


plot_apod()
